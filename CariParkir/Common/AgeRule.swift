//
//  AgeRule.swift
//  CariParkir
//
//  Created by Indovti on 9/8/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation

public class AgeRule : Rule {
    private var AGE_LIMIT:Int = 15
    private var message : String
    
    public init(limit: Int, message : String = "Anda harus berusia minimal %ld tahun"){
        self.message = String(format: message, self.AGE_LIMIT)
    }
    
    open func validate(_ value: String) -> Bool {
        let birthMonth = value.components(separatedBy: "/")
        let currentYear = Calendar.current.component(.year, from: Date())
        if let year = Int(birthMonth[1]) {
            if (currentYear - year < AGE_LIMIT) {
                return false
            }
        } else {
            return false
        }
        
        return true
    }
    
    open func errorMessage() -> String {
        return message
    }
}
