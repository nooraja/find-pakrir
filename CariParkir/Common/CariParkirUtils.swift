//
//  CariParkirUtils.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 7/17/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
class CariParkirUtils {
    static func getDayString(_ today:Int) -> String{
        var todayString: String = ""
        if (today == 1)
        {
            todayString = "SUNDAY"
        }
        else if (today == 2)
        {
            todayString = "MONDAY"
        }
        else if (today == 3)
        {
            todayString = "TUESDAY"
        }
        else if (today == 4)
        {
            todayString = "WEDNESDAY"
        }
        else if (today == 5)
        {
            todayString = "THURSDAY"
        }
        else if (today == 6)
        {
            todayString = "FRIDAY"
        }
        else if (today == 7)
        {
            todayString = "SATURDAY"
        }
        
        return todayString
    }
}
