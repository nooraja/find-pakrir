//
//  CustomDatePicker.swift
//  CariParkir
//
//  Created by vti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class CustomDatePicker: UIDatePicker {

    override func addSubview(_ view: UIView) {
        self.setValue(UIColor.edit_text_gray, forKey: "textColor")
        super.addSubview(view)
    }

}
