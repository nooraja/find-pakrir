//
//  CustomModal.swift
//  CariParkir
//
//  Created by vti on 8/30/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit


class CustomModal {

    var containerView = UIView()
    var frameModal = UIView()
    var parentView = UIView()
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont(name: "Titillium-Bold", size: 18)
        return label
    }()
    
    let stackViewConteiner: UIStackView = {
        let stackView   = UIStackView()
        stackView.axis  = UILayoutConstraintAxis.horizontal
        stackView.distribution  = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.fill
        stackView.spacing   = 10.0
        return stackView
    }()
    
    
    
    func modal(_ view: UIView, title: String, height: CGFloat) {
        parentView = view
        containerView.frame = parentView.frame
        containerView.frame.origin.x = 0
        containerView.frame.origin.y = 0
        let screenSize = UIScreen.main.bounds
        containerView.frame.size.width = screenSize.width
        containerView.frame.size.height = screenSize.height
        containerView.backgroundColor = UIColor(hex: 0x000000, alpha: 0.8)
        
        frameModal.frame = CGRect(x: 25, y: (containerView.frame.size.height - height) / 2, width: (containerView.frame.width) - 50, height: height)
        frameModal.backgroundColor = UIColor(hex: 0xffffff, alpha: 1.0)
        frameModal.clipsToBounds = true
        frameModal.layer.cornerRadius = 5
        
        titleLabel.text = title
        frameModal.addTopBorderWithColor(color: UIColor.yellow_button, width: 50)
        frameModal.addSubview(titleLabel)
        
        _ = titleLabel.anchor(frameModal.topAnchor, left: frameModal.leftAnchor, bottom: nil, right: frameModal.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 21)
        
        containerView.isHidden = true
        containerView.addSubview(frameModal)
        
        if let navVC = parentView.parentViewController?.navigationController {
            navVC.view.addSubview(containerView)
        } else {
            parentView.addSubview(containerView)
            
        }
    }
    
    func showModal() {
        containerView.isHidden = false
    }
    
    func hideModal() {
        containerView.isHidden = true
    }
    
    func addContent(view: UIView, space: CGFloat, width: CGFloat, height: CGFloat, referenceBottom: UIView) {
        
        frameModal.addSubview(view)
        _ = view.anchor(referenceBottom.bottomAnchor, left: frameModal.leftAnchor, bottom: nil, right: frameModal.rightAnchor, topConstant: space, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: width, heightConstant: height)
        view.centerXAnchor.constraint(equalTo: frameModal.centerXAnchor).isActive = true
    }
    
    func addContent(view: UIView, space: CGFloat, width: CGFloat, height: CGFloat) {
        
        frameModal.addSubview(view)
        _ = view.anchor(frameModal.centerYAnchor, left: nil, bottom: nil, right: nil, topConstant: space, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: height)
        view.centerXAnchor.constraint(equalTo: frameModal.centerXAnchor).isActive = true
    }
    
    func addBtnBottom(btn1: UIView, btn2: UIView?, height: CGFloat) {
        
        var parent: UIView? = nil
        
        if (btn2 != nil) {
            stackViewConteiner.addArrangedSubview(btn1)
            stackViewConteiner.addArrangedSubview(btn2!)
            stackViewConteiner.translatesAutoresizingMaskIntoConstraints = false
            parent = stackViewConteiner
        }else {
            parent = btn1
        }
        
        frameModal.addSubview(parent!)
        _ = parent?.anchor(nil, left: frameModal.leftAnchor, bottom: frameModal.bottomAnchor, right: frameModal.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: height)
        parent?.centerXAnchor.constraint(equalTo: frameModal.centerXAnchor).isActive = true
    }
    
    
    func moveY(height: CGFloat) {
        frameModal.frame.origin.y = frameModal.frame.origin.y - height
    }
}

