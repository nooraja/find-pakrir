//
//  Config.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/19/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import UserNotifications

class Config {
    
    static let instance = Config()
    
    let devMode = false
    let prodMode = true
    
    let devServer = "http://cariparkir.nostratech.com"
    let testServer = "http://sit.cariparkir.co.id"
    let prodServer = "https://api.cariparkir.co.id"
    let newsCms = "http://52.163.126.217"
    
    var api: String {
        if devMode {
            return "http://13.76.214.116"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var apiAuth: String {
        if devMode {
            return "\(devServer):20001"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var apiUser: String {
        if devMode {
            return "\(devServer):20003"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var apiLocation: String {
        if devMode {
            return "\(devServer):20005"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var apiSearch: String {
        if devMode {
            return "\(devServer):20007"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var apiTransaction: String {
        if devMode {
            return "\(devServer):20009"
        } else {
            if prodMode {
                return "\(prodServer)"
            } else {
                return "\(testServer)"
            }
        }
    }
    
    var addressWeb: String {
//        return "\(devServer):10002"
        return "\(devServer):10005/#/"
    }
    
    var addressNewsDetail: String {
        if devMode {
            return "\(devServer):10005/#/view-news"
        } else {
            if prodMode {
                return "\(newsCms)/#/view-news"
            } else {
                return "\(testServer)/#/view-news"
            }
        }
    }
    
    var addressPromoDetail: String {
        if devMode {
            return "\(devServer):10005/#/view-promo"
        } else {
            if prodMode {
                return "\(newsCms)/#/view-promo"
            } else {
                return "\(testServer)/#/view-promo"
            }
        }
    }
    
    let googleUserInfo = "https://www.googleapis.com/oauth2/v3/userinfo"
    let globalAPIErrorMessage = "Something went wrong when connecting to server. Please try again later"
    func getErrorMessage(error: Error) -> String {
        if (devMode) {
            return error.localizedDescription
        } else {
            print("[ERROR] " + error.localizedDescription)
            return globalAPIErrorMessage
        }
    }
    var loggedInUser: Consumer? = nil
    var selectedVehicle: ConsumerVehicle? = nil
    
    func showNotification(title: String, message: String) {
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey:
            title, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey:
            message, arguments: nil)
        
        // Deliver the notification in five seconds.
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        
        // Schedule the notification.
        let request = UNNotificationRequest(identifier: "com.nostratech.cariparkir.consumer.development", content: content, trigger: trigger)
//        let request = UNNotificationRequest(identifier: "com.nostratech.cariparkir.consumer.dev", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: nil)
    }
}

public enum Gender: String {
    case male 		= "Male"
    case female     = "Female"
}

public enum VehicleType: String {
    case motor 		= "Motor"
    case mobil     = "Mobil"
}

public enum PromoType: String {
    case all = "Semua Jenis Kendaraan"
    case motor = "Hanya Motor"
    case mobil = "Hanya Mobil"
}

