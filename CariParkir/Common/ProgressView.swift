//
//  ProgressView.swift
//  CariParkir
//
//  Created by vti on 8/28/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class ProgressView {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    open class var shared: ProgressView {
        struct Static {
            static let instance: ProgressView = ProgressView()
        }
        return Static.instance
    }
    
    open func showProgressView(_ view: UIView) {
        containerView.frame = view.frame
        containerView.frame.origin.x = 0
        containerView.frame.origin.y = 0
        let screenSize = UIScreen.main.bounds
        containerView.frame.size.width = screenSize.width
        containerView.frame.size.height = screenSize.height
        containerView.backgroundColor = UIColor.clear
        
        progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        progressView.center = containerView.center
        progressView.backgroundColor = UIColor(hex: 0x444444, alpha: 0.8)
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        if let navVC = view.parentViewController?.navigationController {
            navVC.view.addSubview(containerView)
        } else {
            view.addSubview(containerView)
        }
        
        activityIndicator.startAnimating()
    }
    
    open func hideProgressView() {
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
    
    open func isProgressViewAnimating() -> Bool {
        return activityIndicator.isAnimating
    }
}
