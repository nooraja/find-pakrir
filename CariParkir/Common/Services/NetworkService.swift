//
//  NetworkService.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/19/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SwiftyJSON

protocol INetworkService {
    func fetchParkingNearby(latLong: String, success: @escaping (APIResponseArray<Park>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchLocationBy(name: String, page: Int, size: Int, success: @escaping (APIResponseArray<Location>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchLocationWithDistanceBy(name: String, latlong: String, page: Int, size: Int, success: @escaping (APIResponseList<Location>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchParkDetail(id: String, success: @escaping (APIResponse<Park>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchValidateLoginOTP(phone: String, otp_key: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchValidateRegisterOTP(consumer: Consumer, success: @escaping (RegisterResponse?)  -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchRequestLoginOTP(phone: String, success: @escaping (APIResponse<GetOTP>?) -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchRequestRegisterOTP(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchValidateRegisterOTPAnother(phone: String, otp_key: String, success:  @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchParkToken(username: String, password: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchConsumerDetail(success: @escaping (APIResponse<Consumer>?) -> Void, failure: @escaping (Error?) -> Void)
    
    func fetchVehicles(type: String, success: @escaping (VehiclesResponse?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchBrands(type: String, brand: String, success: @escaping (BrandsResponse<Brand>?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func sendRequestSignUp(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func sendSignUpOTP(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func fetchforgotPassword(email: String, success: @escaping (APIStringResponse?) -> Void, failure: @escaping (_ error: Error?) -> Void)
    
    func signupSocmed(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void)
    
    func signinSocmed(username: String, password: String, source: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void)
}

class NetworkService: NSObject, INetworkService {
    
    static let sharedInstance = NetworkService()
    
    let home: String
    let serverAuth: String
    let serverUser: String
    let serverLocation: String
    let serverSearch: String
    let serverTransaction: String
    let serverWeb: String
    let serverView: String
    
    override init() {
        home = Config.instance.api
        serverAuth = Config.instance.apiAuth
        serverUser = Config.instance.apiUser
        serverLocation = Config.instance.apiLocation
        serverSearch = Config.instance.apiSearch
        serverTransaction = Config.instance.apiTransaction
        serverView = Config.instance.addressPromoDetail
        serverWeb = Config.instance.addressWeb
        super.init()
    }
    
    func fetchParkingNearby(latLong: String, success: @escaping (APIResponseArray<Park>?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        let parameters: [String : Any] = [
            "location": latLong
        ]
        
        startRequest(method: .get, path: "\(serverSearch)/parking/nearby", parameters: parameters, encoding: URLEncoding.default).responseObject { (response: DataResponse<APIResponseArray<Park>>) in
            
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func fetchLocationBy(name: String, page: Int, size: Int, success: @escaping (APIResponseArray<Location>?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        let parameters: [String : Any] = [
            "name": name,
            "page": page,
            "size": size
        ]
        
        startRequest(method: .get, path: "\(serverSearch)/poi/search", parameters: parameters, encoding: URLEncoding.default).responseObject { (response: DataResponse<APIResponseArray<Location>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func fetchLocationWithDistanceBy(name: String, latlong: String, page: Int, size: Int, success: @escaping (APIResponseList<Location>?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        print("fetchLocationWithDistanceBy called")
        print("name: \(name)")
        print("latlong: \(latlong)")
        print("page: \(page)")
        print("size: \(size)")
        
        let parameters: [String : Any] = [
            "name": name,
            "latlong": latlong,
            "page": page,
            "size": size
        ]

        startRequest(method: .get, path: "\(serverLocation)/location-service/api/v1/public/parking/location/search-poi", parameters: parameters, encoding: URLEncoding.default).responseObject { (response: DataResponse<APIResponseList<Location>>) in
            
            print("response: \(response.result)")
            print("response: \(response.result)")
            
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func fetchParkDetail(id: String, success: @escaping (APIResponse<Park>?) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        startRequest(method: .get, path: "\(serverLocation)/location-service/api/v1/public/parking/location/\(id)", parameters: nil, encoding: URLEncoding.default).responseObject { (response: DataResponse<APIResponse<Park>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }

    }
    
    /// Validate OTP
    /// - Parameter :
    ///   - phone   : phone_number consumer
    ///   - otp_key : otp_key consumer
    func fetchValidateLoginOTP(phone: String, otp_key: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "phone_number": phone,
            "otp_key"     : otp_key
        ]
        startRequest(method: .post, path: "\(serverAuth)/auth-service/api/v1/login-validate-otp/", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIResponse<GetToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchValidateRegisterOTP(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": consumer.username,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "password": consumer.password,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!),
            "status_login": consumer.source,
            "consumer_vehicle_list": [
                [
                    "vehicle_type": consumer.vehicleList[0].id,
                    "reg_code": consumer.vehicleList[0].regCode
//                    "vehicle_year": consumer.vehicleList[0].vehicleYear
                ]
            ]
        ]
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/validate-otp-register/", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<RegisterResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    //// Request OTP with Phone Number
    //// - Paramaters :
    ////   - phone    : phone_number consumer
    func fetchRequestLoginOTP(phone: String, success: @escaping (APIResponse<GetOTP>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "phone_number": phone
        ]
        startRequest(method: .post, path: "\(serverAuth)/auth-service/api/v1/login-request-otp/", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIResponse<GetOTP>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchRequestRegisterOTP(consumer: Consumer,  success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": consumer.username,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "password": consumer.password,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!),
            "status_login": consumer.source,
            "consumer_vehicle_list": [
                [
                    "vehicle_type": consumer.vehicleList[0].id,
                    "reg_code": consumer.vehicleList[0].regCode,
                    "vehicle_year": consumer.vehicleList[0].vehicleYear
                ]
            ]
        ]
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/request-otp-register/", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<RegisterResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchValidateRegisterOTPAnother(phone: String, otp_key: String,  success:  @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "phone_number": phone,
            "otp_key": otp_key
        ]
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/validate-otp/", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIResponse<GetToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    /// Login Auth Token
    ///
    /// - Parameters:
    ///   - username: username consumer
    ///   - password: password consumer
    func fetchParkToken(username: String, password: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": username,
            "password": password
        ]
        
        startRequest(method: .post, path: "\(serverAuth)/auth-service/api/v1/login?lang=in_ID", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIResponse<GetToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func logout(success: @escaping (APIStringResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": Config.instance.loggedInUser!.username
        ]
        
        startRequest(method: .post, path: "\(serverAuth)/auth-service/api/v1/logout?lang=in_ID", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIStringResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func refreshToken(success: @escaping (APIResponse<ConsumerToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "refresh_token": UserDefaults.standard.getRefreshToken()!
        ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        startRequestWithHeader(method: .post, path: "\(serverAuth)/auth-service/api/v1/refresh-token?lang=in_ID", parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<ConsumerToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func changePassword(newPass: String, oldPass: String, success: @escaping (APIStringResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": Config.instance.loggedInUser!.username,
            "old_password": oldPass,
            "new_password": newPass
        ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        startRequestWithHeader(method: .put, path: "\(serverUser)/user-service/api/v1/update-password/consumer", parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIStringResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    /// Get Data Vehicle
    ///
    /// - Parameters:
    ///   - type: type motor or mobil
    func fetchVehicles(type: String, success: @escaping (VehiclesResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "type": type
        ]
        
        startRequest(method: .get, path: "\(serverUser)/user-service/api/v1/public/usermanagement/list-all-type", parameters: parameters, encoding: URLEncoding.default).responseObject { (response: DataResponse<VehiclesResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    
    /// Get Data Brand
    ///
    /// - Parameters:
    ///   - type: type motor or mobil
    ///   - brand: brand motor or mobil
    func fetchBrands(type: String, brand: String, success: @escaping (BrandsResponse<Brand>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "type": type,
            "brand": brand
        ]
        
        startRequest(method: .get, path: "\(serverUser)/user-service/api/v1/public/usermanagement/list-all-brand", parameters: parameters, encoding: URLEncoding.default).responseObject { (response: DataResponse<BrandsResponse<Brand>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    
    
    /// Sign Up/Register
    ///
    /// - Parameters:
    ///   - email: email consumer
    ///   - password: password consumer
    ///   - ttl: ttl consumer
    ///   - gender: gender consumer
    ///   - tlp: tlp consumer
    ///   - vehicleType: vehicleType consumer
    ///   - noPol: noPol consumer
    func sendRequestSignUp(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": consumer.username,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "password": consumer.password,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!),
            "consumer_vehicle_list": [
                [
                    "vehicle_type": consumer.vehicleList[0].id,
                    "reg_code": consumer.vehicleList[0].regCode,
                    "vehicle_year": consumer.vehicleList[0].vehicleYear
                ]
            ]
        ]
        
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/register?lang=in_ID", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<RegisterResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func sendSignUpOTP(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": consumer.username,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "password": consumer.password,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!),
            "status_login": consumer.source,
            "consumer_vehicle_list": [
                [
                    "vehicle_type": consumer.vehicleList[0].id,
                    "reg_code": consumer.vehicleList[0].regCode,
                    "vehicle_year": consumer.vehicleList[0].vehicleYear
                ]
            ]
        ]
        
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/register-otp", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<RegisterResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func signupSocmed(consumer: Consumer, success: @escaping (RegisterResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": consumer.username,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "password": consumer.password,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!),
            "status_login": consumer.source,
            "consumer_vehicle_list": [
                [
                    "vehicle_type": consumer.vehicleList[0].id,
                    "reg_code": consumer.vehicleList[0].regCode,
                    "vehicle_year": consumer.vehicleList[0].vehicleYear
                ]
            ]
        ]
        
        startRequest(method: .post, path: "\(serverUser)/user-service/api/v1/public/usermanagement/register-social-media", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<RegisterResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func signinSocmed(username: String, password: String, source: String, success: @escaping (APIResponse<GetToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let parameters: Parameters = [
            "username": username,
            "password": password
//            "status_login": source
        ]
        
        startRequest(method: .post, path: "\(serverAuth)/auth-service/api/v1/login", parameters: parameters, encoding: JSONEncoding.default).responseObject { (response: DataResponse<APIResponse<GetToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            
            failure(response.error)
        }
    }
    
    func fetchConsumerDetail(success: @escaping (APIResponse<Consumer>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverUser)/user-service/api/v1/consumer/get-detail-profile", parameters: nil, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<Consumer>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func updateConsumerDetail(consumer: Consumer, success: @escaping (APIResponse<Consumer>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        var params: Parameters = [
            "version": consumer.version,
            "first_name": consumer.firstName,
            "last_name": consumer.lastName,
            "phone": consumer.phone,
            "birth_date": consumer.birthDate,
            "gender": String(consumer.gender.rawValue.first!)
        ]
        
        var vehicleList:[Parameters] = []
        for consumerVehicle in consumer.vehicleList {
            vehicleList.append([
                "vehicle_type": consumerVehicle.vehicleId,
                "reg_code": consumerVehicle.regCode,
                "vehicle_year": "\(consumerVehicle.vehicleYear)"
                ])
        }
        params["consumer_vehicle_list"] = vehicleList
        
        startRequestWithHeader(method: .put, path: "\(serverUser)/user-service/api/v1/consumer/\(consumer.id)", parameters: params, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<Consumer>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func registerDeviceToken(token: String, success: @escaping (APIResponse<DeviceToken>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "deviceId": "\(token)"
        ]
        
        startRequestWithHeader(method: .put, path: "\(serverUser)/user-service/api/v1/consumer/set-device-id", parameters: params, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<DeviceToken>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchNews(page: Int, success: @escaping (APIResponseArray<NewsPromo>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "page": page
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/info/info-active?type=N", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<NewsPromo>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchPromo(page: Int, success: @escaping (APIResponseArray<NewsPromo>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "page": page
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/info/promo-active", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<NewsPromo>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchRedeemPoint(success: @escaping (APIIntResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "qrcode": Config.instance.loggedInUser!.qrCode
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/transaction/user-point", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIIntResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchUsedRedeem(page: Int, success: @escaping (APIResponseArray<Redeem>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "qr_code_consumer": Config.instance.loggedInUser!.qrCode,
            "page": page
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/transaction/used-redeem", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<Redeem>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
//
//        startRequestWithHeader(method: .get, path: "\(serverView)/transaction-service/api/v1/transaction/view-promo", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<Redeem>>) in
//            if response.result.isSuccess {
//                success(response.result.value)
//                return
//            }
//            failure(response.error)
//        }
    }
    
    func redeem(promo: NewsPromo, success: @escaping (APIResponse<Redeem>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "qr_code_consumer": Config.instance.loggedInUser!.qrCode,
            "point": promo.point,
            "promo_code": promo.code,
            "type": promo.type == .mobil ? "C" : (promo.type == .motor ? "M" : "A")
        ]
        
        startRequestWithHeader(method: .post, path: "\(serverTransaction)/transaction-service/api/v1/transaction/redeem-point", parameters: params, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<Redeem>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func claimPromo(redeem: String, ticketCode: String, success: @escaping (APIStringResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "code_promo": redeem,
            "code_transaction": ticketCode
        ]
        
        startRequestWithHeader(method: .put, path: "\(serverTransaction)/transaction-service/api/v1/transaction/code-promo", parameters: params, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIStringResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func checkIn(parkingQrCode: String, success: @escaping (APIResponse<CheckIn>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let vehicle: ConsumerVehicle = Config.instance.selectedVehicle!
        let parameters: Parameters = [
            "qr_code_consumer": Config.instance.loggedInUser!.qrCode,
            "parking_location": parkingQrCode,
            "check_in": Date().millisecondsSince1970,
            "vehicle_type": vehicle.vehicleType == .motor ? "M" : "C",
            "reg_code": vehicle.regCode
        ]
        
        startRequestWithHeader(method: .post, path: "\(serverTransaction)/transaction-service/api/v1/transaction", parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponse<CheckIn>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchParkingHistory(page: Int, success: @escaping (APIResponseArray<ParkingHistory>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "page": page
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/transaction/history-user", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<ParkingHistory>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchActiveTicket(success: @escaping (APIResponseArray<ParkingHistory>?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer " + UserDefaults.standard.getToken()!
        ]
        
        let params: Parameters = [
            "qrcode": Config.instance.loggedInUser!.qrCode
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverTransaction)/transaction-service/api/v1/transaction/ticket", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIResponseArray<ParkingHistory>>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func fetchforgotPassword(email: String, success: @escaping (APIStringResponse?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let params: Parameters = [
            "email": email
        ]
        
        startRequestWithHeader(method: .get, path: "\(serverUser)/user-service/api/v1/public/usermanagement/forgot-password", parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<APIStringResponse>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    func getGoogleUserInfo(token: String, success: @escaping (GoogleUser?) -> Void, failure: @escaping (Error?) -> Void) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let params: Parameters = [
            "access_token": token
        ]
        
        startRequestWithHeader(method: .get, path: Config.instance.googleUserInfo, parameters: params, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GoogleUser>) in
            if response.result.isSuccess {
                success(response.result.value)
                return
            }
            failure(response.error)
        }
    }
    
    private func startRequest(method: HTTPMethod, path: String, parameters: Parameters?, encoding: ParameterEncoding) -> DataRequest {
        print("[\(method.rawValue)] \(path)")
        parameters?.forEach { print("\($0.key) = \($0.value)") }
        return Alamofire.request(path, method: method, parameters: parameters, encoding: encoding)
    }
    
    private func startRequestWithHeader(method: HTTPMethod, path: String, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> DataRequest {
        print("[\(method.rawValue)] \(path)")
        parameters?.forEach { print("\($0.key) = \($0.value)") }
        return Alamofire.request(path, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
}
