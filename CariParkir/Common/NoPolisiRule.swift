//
//  NoPolisiRule.swift
//  CariParkir
//
//  Created by vti on 9/7/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation


public class NoPolisiRule : RegexRule {
    
//    static let regex = "(([A-Z]){1,2})\\s((\\d){1,4})\\s([A-Z]{1,3})$"
    
    static let regex = "^(([A-Z]){1,2})\\s((\\d){1,4})(|(\\s[A-Z]{0,3}))$"
    
    public convenience init(message : String = "Nomor Polisi Harus Terdiri Maksimal Dua Huruf, 4 Angka dan huruf Besar.\nContoh: B 1234 ABC") {
        self.init(regex: NoPolisiRule.regex, message : message)
    }
}
