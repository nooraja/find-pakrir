//
//  CustomSegmentedControl.swift
//  CariParkir
//
//  Created by Indovti on 8/21/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UISegmentedControl {
    let selectedBackgroundColor = UIColor(red: 19/255, green: 59/255, blue: 85/255, alpha: 0.5)
    var sortedViews: [UIView]!
    var currentIndex: Int = 0
    let bottomBorderLayer = CALayer()
    var screenWidth: CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        screenWidth = UIScreen.main.bounds.width
        configure()
    }
    
    private func configure() {
        sortedViews = self.subviews.sorted(by:{$0.frame.origin.x < $1.frame.origin.x})
        changeSelectedIndex(to: selectedSegmentIndex)
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 0
        self.clipsToBounds = true
        bottomBorderLayer.backgroundColor = UIColor.white.cgColor
        print("bottom configure : \(bottomBorderLayer.frame)")
        bottomBorderLayer.frame = CGRect(x: 0, y: self.frame.height - 4, width: self.screenWidth / CGFloat(subviews.count), height: 4)
        var unselectedAttributes = [NSForegroundColorAttributeName: UIColor.search_title_gray,
                                    NSFontAttributeName:  UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)]
        var selectedAttributes = [NSForegroundColorAttributeName: UIColor.white,
                                    NSFontAttributeName:  UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)]
        if let font = UIFont(name: "Titillium-RegularUpright", size: 14) {
            unselectedAttributes.updateValue(font, forKey: NSFontAttributeName)
            selectedAttributes.updateValue(font, forKey: NSFontAttributeName)
        }
        self.setTitleTextAttributes(unselectedAttributes, for: .normal)
        self.setTitleTextAttributes(selectedAttributes, for: .selected)
        
    }
    
    func changeSelectedIndex(to newIndex: Int) {
        bottomBorderLayer.removeFromSuperlayer()
        bottomBorderLayer.backgroundColor = UIColor.white.cgColor
        bottomBorderLayer.frame = CGRect(x: 0, y: sortedViews[selectedSegmentIndex].frame.height - 4, width: self.screenWidth / CGFloat(subviews.count), height: 4)
        print("bottom : \(bottomBorderLayer.frame)")
        
        self.sortedViews[selectedSegmentIndex].layer.addSublayer(bottomBorderLayer)
    }
}
