//
//  CustomPopup.swift
//  CariParkir
//
//  Created by vti on 8/26/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class CustomPopup: UIView {
    @IBOutlet var customPopup: UIView!
    @IBOutlet weak var bgTrans: UIView!
    @IBOutlet weak var framePopup: UIView!
    @IBOutlet weak var btnOK: UIButton!
    
    var newView: CustomPopup?
    var myController: ScanQRCodeController?
    
    override func layoutSubviews() {
        framePopup.layer.cornerRadius = 5
        framePopup.layer.masksToBounds = true
        btnOK.layer.cornerRadius = 3
        
        customPopup.isHidden = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        customPopup = Bundle.main.loadNibNamed("CustomPopup", owner: self, options: nil)?[0] as! UIView
        customPopup.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        customPopup.frame = bounds
        addSubview(customPopup)
    }
    
    @IBAction func actionClickOK(_ sender: UIButton) {
        customPopup.isHidden = true
        newView?.alpha = 0
        myController?.dismiss(animated: true, completion: nil)
    }
    
    func showPopup() {
        customPopup.isHidden = false
    }
    
    func otherView(_ view: CustomPopup, _ controller: ScanQRCodeController) {
        newView = view;
        myController = controller
    }
    
//    class func instanceFromNib() -> CustomPopup {
//        return UINib(nibName: "CustomPopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomPopup
//    }
}
