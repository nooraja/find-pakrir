//
//  TahunKendaraanRule.swift
//  CariParkir
//
//  Created by Indovti on 9/8/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation

public class TahunKendaraanRule : Rule {
    
    private var message : String
    
    public init(message : String = "Tidak boleh melebihi tahun saat ini"){
        self.message = message
    }
    
    open func validate(_ value: String) -> Bool {
        let currentYear = Calendar.current.component(.year, from: Date())
        if let year = Int(value) {
            if (year > currentYear) {
                return false
            }
        } else {
            return false
        }
        
        return true
    }
    
    open func errorMessage() -> String {
        return message
    }
}
