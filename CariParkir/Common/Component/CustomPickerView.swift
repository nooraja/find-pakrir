//
//  CustomPickerView.swift
//  CariParkir
//
//  Created by vti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class CustomPickerView: UIPickerView {

    func getFontAttribute(_ text: String) -> NSAttributedString {
        return NSAttributedString(string: text, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.edit_text_gray])
    }

}
