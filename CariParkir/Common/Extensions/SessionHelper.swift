//
//  SessionHelper.swift
//  CariParkir
//
//  Created by vti on 8/23/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import SwiftyJSON

extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case isLoggedIn
        case userName
        case fullName
        case token
        case refreshToken
        case defaultVehicle
        case isWizard
        case isTermAndCondition
        case isSosmed
        case isPhoneNumber
        case isPassword
        case isEmailVerified
    }
    
    func setIsLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        synchronize()
    }
    
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func setIsWizard(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isWizard.rawValue)
        synchronize()
    }
    
    func isWizard() -> Bool {
        return bool(forKey: UserDefaultsKeys.isWizard.rawValue)
    }
    
    func setIsTermAndCondition(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isTermAndCondition.rawValue)
        synchronize()
    }
    
    func isTermAndCondition() -> Bool {
        return bool(forKey: UserDefaultsKeys.isTermAndCondition.rawValue)
    }
    
    func setUserName(value: String) {
        set(value, forKey: UserDefaultsKeys.userName.rawValue)
        synchronize()
    }
    
    func getUserName() -> String? {
        return string(forKey: UserDefaultsKeys.userName.rawValue)
    }
    
    func setFullName(value: String) {
        set(value, forKey: UserDefaultsKeys.fullName.rawValue)
        synchronize()
    }
    
    func getFullName() -> String? {
        return string(forKey: UserDefaultsKeys.fullName.rawValue)
    }
    
    func setToken(value: String) {
        set(value, forKey: UserDefaultsKeys.token.rawValue)
        synchronize()
    }
    
    func getToken() -> String? {
        return string(forKey: UserDefaultsKeys.token.rawValue)
    }
    
    func setRefreshToken(value: String) {
        set(value, forKey: UserDefaultsKeys.refreshToken.rawValue)
        synchronize()
    }
    
    func getRefreshToken() -> String? {
        return string(forKey: UserDefaultsKeys.refreshToken.rawValue)
    }
    
    func setIsSosmed(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isSosmed.rawValue)
        synchronize()
    }
    
    func isSosmed() -> Bool {
        return bool(forKey: UserDefaultsKeys.isSosmed.rawValue)
    }
    
    func setIsEmailVerified(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isEmailVerified.rawValue)
        synchronize()
    }
    
    func isEmailVerified() -> Bool {
        return bool(forKey: UserDefaultsKeys.isEmailVerified.rawValue)
    }
    
    func setPhoneNumber(value: String)  {
        set(value, forKey: UserDefaultsKeys.isPhoneNumber.rawValue)
        synchronize()
    }
    
    func getPhoneNumber() -> String {
        return string(forKey: UserDefaultsKeys.isPhoneNumber.rawValue)!
    }
    
    func setPassword(value: String)  {
        set(value, forKey: UserDefaultsKeys.isPassword.rawValue)
        synchronize()
    }
    
    func getPassword() -> String {
        return string(forKey: UserDefaultsKeys.isPassword.rawValue)!
    }
    
    private func getDefaultVehicleList() -> [String:ConsumerVehicle] {
        let vehicleString:String? = string(forKey: UserDefaultsKeys.defaultVehicle.rawValue)
        if let vehicles = vehicleString {
            if (vehicles != "") {
                var vehicleList:[String:ConsumerVehicle] = [:]
                for (key, object):(String, JSON) in JSON(parseJSON: vehicles) {
                    vehicleList[key] = ConsumerVehicle(JSONString: object.rawString([.castNilToNSNull: true])!)
                }
                return vehicleList
            } else {
                return [:]
            }
        } else {
            return [:]
        }
    }
    
    func setDefaultVehicle(consumerId: String, vehicle: ConsumerVehicle) {
        let vehicleList = self.getDefaultVehicleList()
        var vehicles: JSON = [:]
        for (key, value) in vehicleList {
            let vehicleJson = JSON(parseJSON: value.toJSONString()!)
            vehicles[key] = vehicleJson
        }
        vehicles[consumerId] = JSON(parseJSON: vehicle.toJSONString()!)
        set(vehicles.rawString([.castNilToNSNull: true])!, forKey: UserDefaultsKeys.defaultVehicle.rawValue)
    }
    
    func getDefaultVehicle(consumerId: String) -> ConsumerVehicle? {
        let vehicleList:[String:ConsumerVehicle] = self.getDefaultVehicleList()
        return vehicleList[consumerId] ?? nil
    }
}
