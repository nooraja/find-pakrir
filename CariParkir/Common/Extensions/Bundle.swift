//
//  Bundle.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 7/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import UIKit

public extension Bundle{
    
    class func loadStoryboard(forClass aClass: AnyClass, bundleName: String, storyboardName: String) -> UIStoryboard{
        let podBundle = Bundle(for: aClass)
        return UIStoryboard(name: storyboardName, bundle: podBundle)
    }
    
    class func getPlistUrl(forClass aClass: AnyClass, bundleName: String, plistName: String) -> URL?{
        let podBundle = Bundle(for: aClass)
        return podBundle.url(forResource: plistName, withExtension: "plist")!
    }
    
    class func getBundle(forClass aClass: AnyClass) -> Bundle {
        return Bundle(for: aClass)
    }
    
    class func loadNibNamed(_ nibName: String, forClass nibClass: AnyClass) -> UINib {
        let podBundle = Bundle(for: nibClass)
        return UINib(nibName: nibName, bundle: podBundle)
    }
}
