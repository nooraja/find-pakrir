//
//  String.swift
//  CariParkir
//
//  Created by Indovti on 9/14/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            if let d = data(using: String.Encoding.utf8, allowLossyConversion: true) {
                return try NSAttributedString(data: d, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            }
            return nil
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    var htmlAttributedString: NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlAttributedString.string
    }
}
