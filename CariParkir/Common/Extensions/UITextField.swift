//
//  UITextField.swift
//  CariParkir
//
//  Created by vti on 8/18/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func underlined(){
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: screenWidth, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func underlinedRed(){
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = UIColor.red.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  screenWidth, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func underlinedWhite(){
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: screenWidth, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension UILabel {
    
    func underlined(){
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  screenWidth, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func underlinedRed(){
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = UIColor.red.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  screenWidth, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
}
