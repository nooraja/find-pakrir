//
//  Date.swift
//  CariParkir
//
//  Created by Indovti on 8/30/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
