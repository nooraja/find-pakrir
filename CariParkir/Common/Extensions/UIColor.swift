//
//  UIColor.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    public convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red) / 255
        let newGreen = CGFloat(green) / 255
        let newBlue = CGFloat(blue) / 255
        
        self.init(red: newRed,green: newGreen, blue: newBlue, alpha: 1.0)
    }
    
    public class func fromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static let gray_placeholder: UIColor = UIColor(hex: 0xb4b6b8, alpha: 1)
    static let color_primary: UIColor = UIColor(hex: 0x58595b, alpha: 1)
    static let color_primary_dark: UIColor = UIColor(hex: 0x414042, alpha: 1)
    static let color_accent: UIColor = UIColor(hex: 0xFF4081, alpha: 1)
    static let soft_grey: UIColor = UIColor(hex: 0xe1dde3, alpha: 1)
    static let toolbar_grey: UIColor = UIColor(hex: 0x58595b, alpha: 1)
    static let login_background: UIColor = UIColor(hex: 0xffc822, alpha: 1)
    static let yellow_button: UIColor = UIColor(hex: 0xffd400, alpha: 1)
    static let signup_button: UIColor = UIColor(hex: 0x605f62, alpha: 1)
    static let white: UIColor = UIColor(hex: 0xffffff, alpha: 1)
    static let label_gray: UIColor = UIColor(hex: 0xaea27b, alpha: 1)
    static let black: UIColor = UIColor(hex: 0x000000, alpha: 1)
    static let edit_text_gray: UIColor = UIColor(hex: 0x605f62, alpha: 1)
    static let search_title_gray: UIColor = UIColor(hex: 0x919295, alpha: 1)
    static let parking_detail_box: UIColor = UIColor(hex: 0x414042, alpha: 1)
    static let grey_background: UIColor = UIColor(hex: 0xf1f2f2, alpha: 1)
    static let orange: UIColor = UIColor(hex: 0xf7941e, alpha: 1)
    static let orange_underline: UIColor = UIColor(hex: 0xffde17, alpha: 1)
    static let gray_overlay: UIColor = UIColor(hex: 0x990000, alpha: 0)
    static let gplus_color_button: UIColor = UIColor(hex: 0xDF4A32, alpha: 1)
    static let fb_color_button: UIColor = UIColor(hex: 0x49639F, alpha: 1)
    static let color_yellow_vehicle: UIColor = UIColor(hex: 0xF9ED32, alpha: 1)
    static let light_orange: UIColor = UIColor(hex: 0xffc822, alpha: 1)
    static let grey: UIColor = UIColor(hex: 0x919295, alpha: 1)
    static let green: UIColor = UIColor(hex: 0x8DC63F, alpha: 1)
    static let transparent: UIColor = UIColor(hex: 0x000000, alpha: 0)
    static let accent: UIColor = UIColor(hex: 0x40C4FF, alpha: 1)
    static let grey_icon: UIColor = UIColor(hex: 0x969696, alpha: 1)
}
