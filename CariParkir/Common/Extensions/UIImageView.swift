//
//  UIImageView.swift
//  CariParkir
//
//  Created by Indovti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()
class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    func downloadedFrom(_ link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        
        imageUrlString = link
        
        guard let url =  URL(string: link.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) else { return }
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: link as NSString) {
            self.image = imageFromCache
            self.contentMode = mode
            return
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse , httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType , mimeType.hasPrefix("image"),
                let data = data , error == nil,
                let image = UIImage(data: data)
            else {
                return
            }
            DispatchQueue.main.async(execute: {
                let imageToCache = image
                
                if self.imageUrlString == link {
                    self.image = imageToCache
                    self.contentMode = mode
                }
                
                imageCache.setObject(imageToCache, forKey: link as NSString)
            })
            
        }) .resume()
    }
    
    func downloadedFromImageUrl(_ link: String, completed: @escaping (_ img : UIImage) -> ()) {
        guard let url =  URL(string: link) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse , httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType , mimeType.hasPrefix("image"),
                let data = data , error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async(execute: {
                self.image = image
                completed(image)
            })
            
        }) .resume()
    }
}
