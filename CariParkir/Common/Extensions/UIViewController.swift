//
//  UIViewController.swift
//  CariParkir
//
//  Created by Indovti on 9/3/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

extension
UIViewController {

    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 75, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        if let font = UIFont(name: "Titillium-RegularUpright", size: 12) {
            toastLabel.font = font
        } else {
            toastLabel.font = UIFont.systemFont(ofSize: 12)
        }
        
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func generateTableBackground(tableView: UITableView, title: String, text: String, image: UIImage, proporsion: CGFloat, tint: UIColor) -> UIView {
        let bgWrapper = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        let centerPos: CGFloat = tableView.bounds.size.height / 2
        
        let titleLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: centerPos - 18, width: tableView.bounds.size.width, height: 36))
        titleLabel.text          = title
        titleLabel.textColor     = tint
        titleLabel.textAlignment = .center
        let fontBold = UIFont(name: "Titillium-bold", size: 36)
        if (fontBold != nil) {
            titleLabel.font = fontBold
        }
        
        let iconWidth: CGFloat = proporsion * tableView.bounds.size.width
        let icon = UIImageView(frame: CGRect(x: (tableView.bounds.size.width - iconWidth) / 2, y: centerPos - 50 - iconWidth, width: iconWidth, height: iconWidth))
        let templateImage = image.withRenderingMode(.alwaysTemplate)
        icon.tintColor = tint
        icon.image = templateImage
        icon.contentMode = .scaleToFill
        
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: centerPos + 26, width: tableView.bounds.size.width, height: 16))
        noDataLabel.text          = text
        noDataLabel.textColor     = tint
        noDataLabel.textAlignment = .center
        let font = UIFont(name: "Titillium-Semibold", size: 16)
        if (font != nil) {
            noDataLabel.font = font
        }
        
        bgWrapper.addSubview(titleLabel)
        bgWrapper.addSubview(icon)
        bgWrapper.addSubview(noDataLabel)
        
        return bgWrapper
    }

    func setNavigationBarFont() {
        if let font = UIFont(name: "Titillium-Bold", size: 20) {
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: font]
        }
    }
}
