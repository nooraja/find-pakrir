//
//  Park.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/19/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class APIResponseArray<T: Mappable>: Mappable {
    
    var data: [T] = [T]()
    var result: [T] = [T]()
    var page: Int = 0
    var pageSize: Int = 0
    var total: Int = 0
    var errorMessage: String = ""
    var message: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        page <- map["page"]
        pageSize <- map["page_size"]
        total <- map["total"]
        data <- map["data"]
        result <- map["result"]
        message <- map["message"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}

class APIResponseList<T: Mappable>: Mappable {
    
    var data: [T] = [T]()
    var page: Int = 0
    var pageSize: Int = 0
    var total: Int = 0
    var errorMessage: String = ""
    var message: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        page <- map["result.page"]
        pageSize <- map["result.page_size"]
        total <- map["result.total"]
        data <- map["result.data"]
        message <- map["message"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}

class APIStringResponse: Mappable {
    var message: String = ""
    var result: String = ""
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        result <- map["result"]
    }
}

class APIIntResponse: Mappable {
    var message: String = ""
    var result: Int = 0
    var errorMessage: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        message <- map["message"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}

class APIResponse<T: Mappable>: Mappable {
    
    var result: T?
    var message: String = ""
    var errorMessage: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        result <- map["result"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}


class Park: NSObject, Mappable {
    
    var id: String = ""
    var name: String = ""
    var counter: Int = 0
    var contactPersonName: String = ""
    var contactPersonPhone: String = ""
    var operationalDay: [String] = []
    var imageUrlList: [String] = []
    var address: String = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var facilities: [String] = []
    var facilitiesCode: [String] = []
    var open: String = ""
    var close: String = ""
    var location: String = ""
    var parkingType: [ParkingType] = []
    var status: String = "false"
    
    override init() {
        super.init()
    }
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        counter <- map["counter"]
        contactPersonName <- map["contact_person_name"]
        contactPersonPhone <- map["contact_person_phone"]
        operationalDay <- map["operational_day_name"]
        address <- map["address"]
        imageUrlList <- map["imageList"]
        latitude <- map["location.lat"]
        longitude <- map["location.lon"]
        facilities <- map["facilities_name"]
        facilitiesCode <- map["facilities"]
        open <- map["open"]
        close <- map["close"]
        parkingType <- map["parking_type_list"]
        location <- map["lat_lot"]
        status <- map["status"]
    }
    
}

class ParkingType: NSObject, Mappable {
    var type: String = ""
//    var capacity: Int = 0
    var price: Int = 0
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        type <- map["vehicle_type_name"]
//        capacity <- map["capacity"]
        price <- map["price"]
    }
}

class Location: Mappable {
    var name: String = ""
    var longitude: Double = 0
    var latitude: Double = 0
    var type: String = ""
    var distance: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        longitude <- map["location.lon"]
        latitude <- map["location.lat"]
        type <- map["type"]
        distance <- map["distance"]
    }
}

class GetToken: Mappable {
    var token: String = ""
    var refreshToken: String = ""
    var fullName: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        refreshToken <- map["refresh_token"]
        fullName <- map["full_name"]
    }
}

class GetOTP: Mappable {
    var phone_number: String = ""
    var otp_key: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        phone_number <- map["phone_number"]
        otp_key <- map["otp_key"]
    }
    
}


var selectedLocation: Location? = nil
