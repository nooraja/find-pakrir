//
//  ConsumerVehicle.swift
//  CariParkir
//
//  Created by Indovti on 8/24/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class ConsumerVehicle: NSObject, Mappable {
    var id: String = ""
    var version: Int = 0
    var vehicleType: VehicleType = .motor
    var regCode: String = ""
    var vehicleYear: Int = 0
    var vehicleId: String = ""
    var brandName: String = ""
    var brandType: String = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        version <- map["version"]
        vehicleType <- map["vehicle_type"]
        regCode <- map["reg_code"]
        vehicleYear <- map["vehicle_year"]
        vehicleId <- map["vehicle_id"]
        brandName <- map["brand_name"]
        brandType <- map["brand_type"]
    }
}
