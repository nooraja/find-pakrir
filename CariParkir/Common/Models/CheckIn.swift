//
//  CheckIn.swift
//  CariParkir
//
//  Created by Indovti on 8/29/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class CheckIn: NSObject, Mappable {
    var id: String = ""
    var version: Int = 0
    var qrCode: String = ""
    var address: String = ""
    var name: String = ""
    var checkInTime: Int = 0
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.version <- map["version"]
        self.qrCode <- map["code"]
        self.address <- map["address"]
        self.name <- map["name"]
        self.checkInTime <- map["check_in"]
    }
}
