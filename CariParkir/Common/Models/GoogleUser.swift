//
//  GoogleUser.swift
//  CariParkir
//
//  Created by Indovti on 10/3/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import ObjectMapper

class GoogleUser: NSObject, Mappable {
    var name: String = ""
    var picture: String = ""
    var email: String = ""
    var gender: String = ""
    var locale: String = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.name <- map["name"]
        self.picture <- map["picture"]
        self.email <- map["email"]
        self.gender <- map["gender"]
        self.locale <- map["locale"]
    }
}
