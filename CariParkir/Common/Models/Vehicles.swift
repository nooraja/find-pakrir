//
//  Vehicles.swift
//  CariParkir
//
//  Created by vti on 8/24/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class VehiclesResponse: Mappable {
    
    var result: [Any]?
    var message: String?
    var errorMessage: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        result <- map["result"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}
