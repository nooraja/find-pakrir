//
//  NewsPromo.swift
//  CariParkir
//
//  Created by Indovti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsPromo: NSObject, Mappable {
    var id: String = ""
    var version: Int = 0
    var code: String = ""
    var title: String = ""
    var desc: String = ""
    var image: String = ""
    var type: PromoType = .all
    var point: Int = 0
    var promo: String = ""
    var startTime: Int = 0 {
        didSet {
            startDate = Date(timeIntervalSince1970: TimeInterval(startTime / 1000))
        }
    }
    var startDate: Date = Date()
    var endTime: Int = 0 {
        didSet {
            endDate = Date(timeIntervalSince1970: TimeInterval(endTime / 1000))
        }
    }
    var endDate: Date = Date()
    var discParking: Int = 0
    var freeParking: Int = 0
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.version <- map["version"]
        self.code <- map["code"]
        self.title <- map["title"]
        self.desc <- map["description"]
        self.image <- map["image"]
        self.type <- map["type"]
        self.point <- map["point"]
        self.promo <- map["promo"]
        self.startTime <- map["start_date"]
        self.endTime <- map["end_date"]
        self.discParking <- map["discount_parking"]
        self.freeParking <- map["free_parking"]
    }
}
