//
//  DeviceToken.swift
//  CariParkir
//
//  Created by Indovti on 8/29/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class DeviceToken: NSObject, Mappable {
    var id: String = ""
    var version: Int = 0
    var deviceId: String = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.version <- map["version"]
        self.deviceId <- map["deviceId"]
    }
}
