//
//  Redeem.swift
//  CariParkir
//
//  Created by Indovti on 9/6/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class Redeem: NSObject, Mappable {
    var id: String = ""
    var version: Int = 0
    var point: Int = 0
    var code: String = ""
    var promoCode: String = ""
    var promoDesc: String = ""
    var date: Int = 0
    var usedDate: Int? = nil
    var type: String = ""
    var title: String = ""
    var promo_image: String = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.version <- map["version"]
        self.point <- map["point"]
        self.code <- map["redeem_code"]
        self.promoCode <- map["promo_code"]
        self.promoDesc <- map["promo_desc"]
        self.date <- map["redeem_date"]
        self.usedDate <- map["used_date"]
        self.type <- map["type"]
        self.title <- map["title"]
        self.promo_image <- map ["promo_image"]
    }
}
