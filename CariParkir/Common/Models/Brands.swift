//
//  Brands.swift
//  CariParkir
//
//  Created by vti on 8/24/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class BrandsResponse<T: Mappable>: Mappable {
    var result: [T]?
    var message: String?
    var errorMessage: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        result <- map["result"]
        if (message != "OK") {
            errorMessage <- map["result"]
        }
    }
}

class Brand: Mappable {
    var id: String?
    var version: Int?
    var brandName: String?
    var brandType: String?
    var vehicleType: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        version <- map["version"]
        brandName <- map["brand_name"]
        brandType <- map["brand_type"]
        vehicleType <- map["vehicle_type"]
        
    }
}
