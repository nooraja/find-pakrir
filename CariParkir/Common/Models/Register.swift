//
//  Register.swift
//  CariParkir
//
//  Created by vti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class RegisterResponse: Mappable{
    var result: String?
    var message: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        result <- map["result"]
    }
}
