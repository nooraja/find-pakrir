//
//  Consumer.swift
//  CariParkir
//
//  Created by Indovti on 8/24/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class Consumer: NSObject, Mappable, NSCopying {
    var id: String = ""
    var version: Int = 0
    var username: String = ""
    var password: String = ""
    var phone: String = ""
    var role: String = ""
    var enable: Bool = false
    var gender: Gender = .male
    var deviceId: String = ""
    var qrCode: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var birthDate: String = ""
    var vehicleList: [ConsumerVehicle] = []
    var source: String = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        version <- map["version"]
        username <- map["username"]
        phone <- map["phone"]
        role <- map["role"]
        enable <- map["enable"]
        gender <- map["gender"]
        deviceId <- map["deviceId"]
        qrCode <- map["qrCode"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        birthDate <- map["birth_date"]
        vehicleList <- map["consumer_vehicle_list"]
        source <- map["status_login"]
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Consumer()
        copy.id = id
        copy.version = version
        copy.username = username
        copy.phone = phone
        copy.role = role
        copy.enable = enable
        copy.gender = gender
        copy.deviceId = deviceId
        copy.qrCode = qrCode
        copy.firstName = firstName
        copy.lastName = lastName
        copy.birthDate = birthDate
        copy.vehicleList = vehicleList
        return copy
    }
}
