//
//  TermCondition.swift
//  CariParkir
//
//  Created by Wisnu Riyan Pratama on 2/6/18.
//  Copyright © 2018 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class TermCondition: NSObject, Mappable {
    
    var id: String = ""
    var version: Int = 0
    var desc: String = ""
    var time_save: Int16 = 0
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        version <- map["version"]
        desc <- map["description"]
        time_save <- map["time_save"]
    }
    
    
}
