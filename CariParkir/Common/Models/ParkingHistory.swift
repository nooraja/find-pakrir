//
//  ParkingHistory.swift
//  CariParkir
//
//  Created by Indovti on 8/30/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import Foundation
import ObjectMapper

class ParkingHistory: NSObject, Mappable {
    var code: String = ""
    var username: String = ""
    var qrCodeConsumer: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var parkingName: String = ""
    var address: String = ""
    var facilities: String = ""
    var promoCode: String = ""
    var promoDesc: String = ""
    var price: Int = 0
    var total: Int = 0
    var checkIn: Int = 0
    var checkOut: Int = 0
    var totalPoint: Int = 0
    var regCode: String = ""
    var priceHour: Int = 0
    var brandName: String = ""
    var brandType: String = ""
    var vehicleType: String = ""
    var point: Int = 0
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.code <- map["code"]
        self.username <- map["username"]
        self.qrCodeConsumer <- map["qr_code_consumer"]
        self.firstName <- map["first_name"]
        self.lastName <- map["last_name"]
        self.parkingName <- map["parking_name"]
        self.address <- map["address"]
        self.facilities <- map["facilities"]
        self.promoCode <- map["promo_code"]
        self.promoDesc <- map["promo_desc"]
        self.price <- map["price"]
        self.total <- map["total"]
        self.checkIn <- map["check_in"]
        self.checkOut <- map["check_out"]
        self.totalPoint <- map["total_point"]
        self.regCode <- map["reg_code"]
        self.priceHour <- map["price_hour"]
        self.brandName <- map["brand_name"]
        self.brandType <- map["brand_type"]
        self.vehicleType <- map["vehicle_type"]
        self.point <- map["point"]
    }
}
