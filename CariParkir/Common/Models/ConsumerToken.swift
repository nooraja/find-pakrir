//
//  ConsumerToken.swift
//  CariParkir
//
//  Created by Indovti on 9/11/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsumerToken: NSObject, Mappable {
    var token: String = ""
    var refreshToken: String = ""
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.token <- map["token"]
        self.refreshToken <- map["refresh_token"]
    }
}
