//
//  TermAndConditionController.swift
//  CariParkir
//
//  Created by Indovti on 9/14/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class TermAndConditionController: UIViewController, NSLayoutManagerDelegate, UIWebViewDelegate {
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnReload: UIButton!
    
    @IBOutlet weak var termText: UITextView!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webviewHeight: NSLayoutConstraint!
    @IBOutlet weak var parentWebView: UIView!
    
    var networkService: INetworkService!
    let transitionManager = TransitionManager()
    var consumer: Consumer!
    var flagWizard: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupWebView() {
        self.webView.isOpaque = false;
        self.webView.backgroundColor = UIColor.clear
        webView.delegate = self
        let url = URL(string: "https://file.cariparkir.co.id/cari-parkir/terms_and_condition.html")!
        let urlreq = URLRequest(url: url)
        webView.loadRequest(urlreq)
    }
    
    @IBAction func agree(_ sender: UIButton) {
        UserDefaults.standard.setIsTermAndCondition(value: true)
        self.performSegue(withIdentifier: "toSignup", sender: self)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.color =\"White\"")
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"Titillium-Light\"")
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontSize =\"14\"")
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.textAlign =\"justify\"")
        
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.scrollView.isScrollEnabled=false;

        webviewHeight.constant = webView.scrollView.contentSize.height + 63
        webView.scalesPageToFit = true
        
        btnAgree.backgroundColor = UIColor.yellow_button
        btnAgree.isHidden = false
        parentWebView.layoutIfNeeded()
    }

    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 10
    }
    
    @IBAction func actionClickBack(_ sender: UIButton) {
        if (self.flagWizard) {
            let module = LoginModule()
            let controller = module.instantiateWithView()
            controller.modalPresentationStyle = .custom
            controller.modalTransitionStyle = .crossDissolve
            UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                UIApplication.shared.delegate!.window!!.rootViewController = controller
            }, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionReloadTNC(_ sender: UIButton) {
        btnReload.isHidden = true
        setupWebView()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toSignup"?:
            let dest = segue.destination as! SignupController
            dest.transitioningDelegate = self.transitionManager
            networkService = NetworkService.sharedInstance
            dest.networkService = networkService
            dest.flagWizard = self.flagWizard
        default:
            break
        }
    }
}
