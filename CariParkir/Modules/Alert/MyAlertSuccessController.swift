//
//  MyAlertSuccessController.swift
//  CariParkir
//
//  Created by Aditya Sumardi on 3/4/18.
//  Copyright © 2018 PT NOSTRA. All rights reserved.
//

import UIKit

protocol MyAlertSuccessDelegate {
    func finishButtonTapped()
}

class MyAlertSuccessController: UIViewController {
    var content: MyAlertSuccess?
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var bodyView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var yestButton: UIButton!
    
    var delegate: MyAlertSuccessDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let cont = content {
            setupAlert(title: cont.title, message: cont.message)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupAlert(title: String, message: String) {
        bodyView.layer.cornerRadius = 10
        titleView.round(corners: [.topLeft, .topRight], radius: 10)
        yestButton.round(corners: [.allCorners], radius: 4, borderColor: UIColor.gray, borderWidth: 1)
        yestButton.backgroundColor = UIColor.yellow_button
        
        titleLabel.text = title
        descLabel.text = message
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    func animateView() {
        bodyView.alpha = 0;
        self.bodyView.frame.origin.y = self.bodyView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.bodyView.alpha = 1.0;
            self.bodyView.frame.origin.y = self.bodyView.frame.origin.y - 50
        })
    }
    
    @IBAction func yesDidTapped(_ sender: UIButton) {
        delegate?.finishButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
}

struct MyAlertSuccess {
    var title: String = ""
    var message: String = ""
}

