//
//  MyAlertCustomViewController.swift
//  CariParkir
//
//
//  Copyright © 2018 PT NOSTRA. All rights reserved.
//

import UIKit

protocol MyAlertCustomControllerDelegate {
    func okButtonTapped()
    func cancelButtonTapped()
}

class MyAlertCustomViewController: UIViewController {
    var content: MyAlertCustom?
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var bodyView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var yestButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    var delegate: MyAlertCustomControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let cont = content {
            setupAlert(title: cont.title, message: cont.message, labelPositive: cont.yes, labelNegative: cont.no)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupAlert(title: String, message: String, labelPositive: String, labelNegative: String) {
        bodyView.layer.cornerRadius = 10
        titleView.round(corners: [.topLeft, .topRight], radius: 10)
        yestButton.round(corners: [.allCorners], radius: 4, borderColor: UIColor.gray, borderWidth: 1)
        yestButton.backgroundColor = UIColor.yellow_button
        noButton.round(corners: [.allCorners], radius: 4, borderColor: UIColor.gray, borderWidth: 1)
        
        titleLabel.text = title
        descLabel.text = message
        
        yestButton.setTitle(labelPositive, for: .normal)
        noButton.setTitle(labelNegative, for: .normal)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    func animateView() {
        bodyView.alpha = 0;
        self.bodyView.frame.origin.y = self.bodyView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.bodyView.alpha = 1.0;
            self.bodyView.frame.origin.y = self.bodyView.frame.origin.y - 50
        })
    }
    
    @IBAction func yesDidTapped(_ sender: UIButton) {
        delegate?.okButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noDidTapped(_ sender: UIButton) {
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
}

struct MyAlertCustom {
    var title: String = ""
    var message: String = ""
    var yes: String = ""
    var no: String = ""
}

