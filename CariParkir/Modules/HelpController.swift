//
//  HelpController.swift
//  CariParkir
//
//  Created by vti on 9/15/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class HelpController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        webView.delegate = self
        webView.scrollView.bounces = false
        
        let url = URL(string: NetworkService.sharedInstance.serverWeb + "/faq.html")
        if let unwrappedURL = url {
            let request = URLRequest(url: unwrappedURL)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if error == nil {
                    self.webView.loadRequest(request)
                }else {
                    print("Error: \(error.debugDescription)")
                }
                
            }
            
            task.resume()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ProgressView.shared.showProgressView(self.webView)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ProgressView.shared.hideProgressView()
    }

    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
//        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}

