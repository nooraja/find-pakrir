//
//  Page3Controller.swift
//  CariParkir
//
//  Created by vti on 9/6/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class Page3Controller: UIViewController {
    
    @IBOutlet weak var btnCariParkir: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    var networkService: INetworkService!
    let transitionManager = TransitionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnCustom()
    }
    
    func btnCustom() {
        btnLogin.backgroundColor = .clear
        btnLogin.layer.cornerRadius = 3
        btnLogin.layer.borderWidth = 1
        btnLogin.setTitleColor(.white, for: .normal)
        btnLogin.layer.borderColor = UIColor.white.cgColor
        
        btnCariParkir.layer.cornerRadius = 3
        btnSignUp.layer.cornerRadius = 3
    }
    
    @IBAction func clickBtnCariParkir(_ sender: UIButton) {
        self.goToHome()
    }
    
    @IBAction func clickBtnLogin(_ sender: UIButton) {
        self.goToLogin()
    }
    
    @IBAction func clickBtnSignUp(_ sender: UIButton) {
        if UserDefaults.standard.isTermAndCondition() {
            self.goToSignUp()
        } else {
            self.goToTermAndCondition()
        }
    }
    
    
    func goToHome() {
        let module = HomeModule()
        let controller = module.instantiateWithNavigation()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func goToTermAndCondition() {
        let storyboard = UIStoryboard(name: "TermAndCondition", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()! as! TermAndConditionController
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        controller.flagWizard = true
        controller.networkService = NetworkService.sharedInstance
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func goToSignUp() {
        let module = SignupModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }

}
