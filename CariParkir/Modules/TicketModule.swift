//
//  TicketModule.swift
//  CariParkir
//
//  Created by vti on 8/31/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class TicketModule: NSObject {
    
    func instantiateWithView() -> UIViewController {
        let storyboard = UIStoryboard(name: "Ticket", bundle: nil)
        let viewTiket = storyboard.instantiateInitialViewController()!
        
        return viewTiket
    }
}
