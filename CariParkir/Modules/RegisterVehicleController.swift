//
//  RegisterVehicleController.swift
//  CariParkir
//
//  Created by vti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import DLRadioButton

class RegisterVehicleController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    
    @IBOutlet weak var rbMotor: DLRadioButton!
    @IBOutlet weak var rbMobil: DLRadioButton!
    @IBOutlet weak var txtMerk: CustomTextField!
    @IBOutlet weak var txtType: CustomTextField!
    @IBOutlet weak var txtNopol: UITextField!
    @IBOutlet weak var txtTahun: CustomTextField!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var spinMerk: UIActivityIndicatorView!
    @IBOutlet weak var spinType: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblNoPolErr: UILabel!
    @IBOutlet weak var lblTahunErr: UILabel!
    @IBOutlet weak var lblMerkErr: UILabel!
    @IBOutlet weak var lblTypeErr: UILabel!
    
    let transitionManager = TransitionManager()
    weak var activeField: UITextField?
    var merk = [String]()
    var pickerViewMerk: CustomPickerView!
    var pickerViewType: CustomPickerView!
    var pickerViewYear: CustomPickerView!
    var selectedRow = 0;
    var brands: [Brand]?
    var formsList =  [String:String]()
    var type : [(key: String, value: String)]{
        return formsList.sorted{$0.key<$1.key}
    }
    var imageViewMerk: UIImageView!
    var imageViewType: UIImageView!
    var imageViewYear: UIImageView!
    var valueRb: String = ""
    var valueRbFull: VehicleType!
    var txtTypeId: String = ""
    
    var networkService: INetworkService!
    var consumer: Consumer!
    
    var rowDefaultMerk: Int = 0
    var rowDefaultType: Int = 0
    let limitLength = 4
    let validator = Validator()
    
    var currentYear: Int!
    var years = [Int]()
    var defaultYear: Int = 0
    var fixTemp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector:  #selector(deviceDidRotate),
            name: .UIDeviceOrientationDidChange,
            object: nil
        )
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        pickerViewMerk = CustomPickerView()
        pickerViewType = CustomPickerView()
        pickerViewYear = CustomPickerView()
        
        pickerViewMerk.delegate = self
        pickerViewMerk.dataSource = self
        
        pickerViewType.delegate = self
        pickerViewType.dataSource = self

        pickerViewYear.delegate = self
        pickerViewYear.dataSource = self
        
        doneButton(picker: pickerViewMerk, txtPickerTextField: txtMerk)
        doneButton(picker: pickerViewType, txtPickerTextField: txtType)
        doneButton(picker: pickerViewYear, txtPickerTextField: txtTahun)
        
        txtCustom()
        btnCustom()
        txtLoadUnderline()
        radioButtonAction(radioButton: rbMotor)
        radioButtonAction(radioButton: rbMobil)
        
        if consumer.vehicleList[0].vehicleType == .mobil {
            valueRb = "C"
            valueRbFull = .mobil
        }else {
            valueRb = "M"
            valueRbFull = .motor
        }
        
        consumer.vehicleList[0].vehicleType = valueRbFull
        getDataVehicles(value: valueRb, firstLoad: true)
        
        print("consumer register: \(consumer.phone)")
        
        self.hideKeyboard()
    }
    
    func txtLoadUnderline() {
        txtMerk.underlined()
        txtType.underlined()
        txtNopol.underlined()
        txtTahun.underlined()
    }
    
    @IBAction func vehicleNoChanged(_ sender: UITextField) {
        sender.text = sender.text?.uppercased()
    }
    
    func txtCustom() {
        imageViewMerk = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        let imageMerk = UIImage(named: "ic_dropdown")
        imageViewMerk.image = imageMerk
        txtMerk.rightView = imageViewMerk
        txtMerk.rightViewMode = .always
        
        imageViewType = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        let imageType = UIImage(named: "ic_dropdown")
        imageViewType.image = imageType
        txtType.rightView = imageViewType
        txtType.rightViewMode = .always
        
        imageViewYear = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        let imageYear = UIImage(named: "ic_dropdown")
        imageViewYear.image = imageYear
        txtTahun.rightView = imageViewYear
        txtTahun.rightViewMode = .always
        
        imageViewMerk.isHidden = true
        imageViewType.isHidden = true
        
        txtNopol.delegate = self
        txtMerk.delegate  = self
        txtType.delegate  = self
//        txtTahun.delegate = self
        
        
        
        validator.registerField(txtNopol, errorLabel: lblNoPolErr, rules: [RequiredRule(), NoPolisiRule()])
        txtNopol.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        if !consumer.vehicleList[0].regCode.isEmpty {
            txtNopol.text = consumer.vehicleList[0].regCode
        }
        
        validator.registerField(txtMerk, errorLabel: lblMerkErr, rules: [RequiredRule()])
        txtMerk.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
        
        validator.registerField(txtType, errorLabel: lblTypeErr, rules: [RequiredRule()])
        txtType.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
        
        validator.registerField(txtTahun, errorLabel: lblTahunErr, rules: [RequiredRule(), ExactLengthRule(length: 4, message: "Wajib 4 angka"), TahunKendaraanRule()])
        txtTahun.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        if consumer.vehicleList[0].vehicleYear != 0 {
            txtTahun.text = String(consumer.vehicleList[0].vehicleYear)
        }
    }
    
    func btnCustom() {
        btnSignup.backgroundColor = .clear
        btnSignup.layer.cornerRadius = 3
        btnSignup.layer.borderWidth = 1
        btnSignup.layer.borderColor = UIColor.white.cgColor
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == txtTahun {
//            guard let text = textField.text else { return true }
//            let newLength = text.characters.count + string.characters.count - range.length
//            
//            return newLength <= limitLength
//        }
        
        
        
        if textField == txtNopol {
            let char        = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            var lastChar    = ""

            if (textField.text?.isEmpty)! {
                flagNumber  = false
                flagText    = false
            }else{
                lastChar    = String((textField.text?.last)!)
                flagNumber  = !isNumber(s: lastChar)
                flagText    = !flagNumber
            }

            if (isBackSpace != -92) {
                
                if (isNumber(s: string)) {
                    if (flagNumber) {
                        if (lastChar != " "){
                            textField.text?.insert(" ", at: (textField.text?.endIndex)!)
                        }
                    }
                }else {
                    if (flagText) {
                        if (lastChar != " " && string != " "){
                            textField.text?.insert(" ", at: (textField.text?.endIndex)!)
                        }
                    }
                }

            }else {
                print((textField.text?.last)!)
            }
            
          return true
        }
        
        return true
    }
    
    func radioButtonAction(radioButton : DLRadioButton){
        radioButton.addTarget(self, action: #selector(self.logSelectedButton), for: UIControlEvents.touchUpInside);
        
        if (radioButton.titleLabel!.text! == consumer.vehicleList[0].vehicleType.rawValue) {
            radioButton.isSelected = true
        } else {
            radioButton.isSelected = false
        }
        
    }
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        if radioButton.titleLabel!.text! == "Motor" {
            valueRb = "M"
            valueRbFull = .motor
            consumer.vehicleList[0].brandName = "Honda"
        }else{
            valueRb = "C"
            valueRbFull = .mobil
            consumer.vehicleList[0].brandName = "BMW"
        }
        
        consumer.vehicleList[0].brandName = ""
        consumer.vehicleList[0].brandType = ""
        consumer.vehicleList[0].vehicleType = valueRbFull
        getDataVehicles(value: valueRb, firstLoad: true)
    }
    
    func isNumber(s : String) -> Bool {
        let numberCharacters = CharacterSet.decimalDigits.inverted
        return !s.isEmpty && s.rangeOfCharacter(from:numberCharacters) == nil
    }
    
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = CharacterSet(charactersIn: matchCharacters)
        return matchCharacters.rangeOfCharacter(from: characterSet) == nil
    }

    
    var flagNumber = false
    var flagText = false
    func textFieldDidChange(_ textField: UITextField) {
        
//        if textField == txtTahun {
//            if (!((textField.text?.isEmpty)!)) {
//                let char = textField.text?.characters.last!
//                if isNumber(s: String(char!)) {
//                    consumer.vehicleList[0].vehicleYear = Int(textField.text!)!
//                }
//            }else{
//                consumer.vehicleList[0].vehicleYear = 0
//            }
//        }
        
        if textField == txtNopol {
            if (textField.text?.isEmpty)! {
                consumer.vehicleList[0].regCode = ""
            }else {
                consumer.vehicleList[0].regCode = textField.text!
            }
        }
        
        validator.validateField(textField){ error in
            if error != nil {
                error?.errorLabel?.isHidden = false
                error?.errorLabel?.text = error?.errorMessage
            }
        }
        
    }
    
    func validationSuccessful(_ validator: Validator) {
        let temp = String(describing: consumer.phone)
        if consumer.phone != "" {
            let tempEndIndex = temp.endIndex
            let tmpStartIndex = temp.index(tempEndIndex, offsetBy: -11)
            
            let range = Range(uncheckedBounds: (lower: tmpStartIndex, upper: tempEndIndex))
            fixTemp = "62\(temp[range])"
        }
        
        print("index: \(fixTemp)")
//        consumer.phone = fixTemp
        
        print("username : \(consumer.username)")
        print("first_name : \(consumer.firstName)")
        print("last_name : \(consumer.lastName)")
        print("phone : \(consumer.phone)")
        print("password : \(consumer.password)")
        print("birth_date : \(consumer.birthDate)")
        print("gender : \(consumer.gender)")
        print("status_login : \(consumer.source)")
        print("consumer : \(consumer.vehicleList)")
        
        
        ProgressView.shared.showProgressView(self.view)
        networkService.fetchRequestRegisterOTP(consumer: consumer, success: { (data) in
            ProgressView.shared.hideProgressView()
            if(data?.message == "OK") {
                print("data \(String(describing: data?.result))")
                print("data \(String(describing: data?.message))")
                UserDefaults.standard.setPhoneNumber(value: self.fixTemp)
                if self.consumer.phone != "" {
                    self.performSegue(withIdentifier: "toVerificationRegister", sender: self)
                    print("Berhasil OTP")
                }
            }
            else {
                self.alertError(title: "Test"+(data?.message)!, msg: (data?.result)!)
                ProgressView.shared.hideProgressView()
            }
        }) { (error) in
            ProgressView.shared.hideProgressView()
            self.alertError(title: "Error view", msg: Config.instance.getErrorMessage(error: error!))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toVerificationRegister":
            let desti = segue.destination as! VerificationRegisterViewController
            desti.transitioningDelegate = self.transitionManager
            networkService = NetworkService.sharedInstance
            desti.networkService = networkService
            desti.consumer = consumer
            desti.consumer.username = consumer.username
            desti.consumer.firstName = consumer.firstName
            desti.consumer.lastName = consumer.lastName
            desti.consumer.phone = consumer.phone
            desti.consumer.password = consumer.password
            desti.consumer.birthDate = consumer.birthDate
            desti.consumer.gender = consumer.gender
            desti.consumer.source = consumer.source
            desti.consumer.vehicleList = consumer.vehicleList
        default:
            break
        }
    }
    
    
    func alertSuccess(title: String ,msg: String){
        let storyBoard = UIStoryboard(name: "AlertSuccess", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "alertsuccess") as! MyAlertSuccessController
        customAlert.content = MyAlertSuccess(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertSuccessDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func alertError(title: String ,msg: String){
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
        
    }
    
    func validationFailed(_ validator: Validator, _ errors:[(Validatable ,ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.underlinedRed()
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
    
    func deviceDidRotate() {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            txtLoadUnderline()
        default:
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        }
    }
    
    func doneButton(picker: UIPickerView, txtPickerTextField: CustomTextField){
        
        let pickerView = picker
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true
        
        var years: [Int] = []
        currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
        
        if years.count == 0 {
            var oldyear: Int = 1900
            for _ in oldyear...currentYear {
                years.append(oldyear)
                oldyear += 1
            }
        }
        self.years = years
        
        let toolBar = UIToolbar()
        toolBar.barTintColor = UIColor.yellow_button
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Pilih", style: .done, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Batal", style: .plain, target: self, action: #selector(self.canclePicker))
        
        switch pickerView {
        case pickerViewMerk:
            doneButton.tag = 1
            cancelButton.tag = 1
        case pickerViewType:
            doneButton.tag = 2
            cancelButton.tag = 2
        case pickerViewYear:
            doneButton.tag = 3
            cancelButton.tag = 3
            defaultYear = years.index(of: currentYear)!
            picker.selectRow(defaultYear, inComponent: 0, animated: true)
            txtTahun.text = "\(years[defaultYear])"
        default:
            return
        }
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtPickerTextField.inputView = pickerView
        txtPickerTextField.inputAccessoryView = toolBar
    }
    
    func donePicker(_ sender: UIBarButtonItem) {
        
        switch sender.tag {
        case 1:
            rowDefaultMerk = pickerViewMerk.selectedRow(inComponent: 0)
            txtMerk.text = merk[rowDefaultMerk]
            getDataBrand(param1: valueRb, param2: txtMerk.text!)
            consumer.vehicleList[0].brandName = txtMerk.text!
            
        case 2:
            rowDefaultType = pickerViewType.selectedRow(inComponent: 0)
            txtType.text = type[rowDefaultType].value
            txtTypeId = type[rowDefaultType].key
            consumer.vehicleList[0].brandType = txtType.text!
            consumer.vehicleList[0].id = txtTypeId
    
        case 3:
            defaultYear = pickerViewYear.selectedRow(inComponent: 0)
            txtTahun.text = "\(years[defaultYear])"
            consumer.vehicleList[0].vehicleYear = Int(txtTahun.text!)!
        default:
            return
        }
        
        self.view.endEditing(true)
    }
    
    func canclePicker(_ sender: UIBarButtonItem) {
        
        switch sender.tag {
        case 1:
            pickerViewMerk.selectRow(rowDefaultMerk, inComponent: 0, animated: true)
        case 2:
            txtTypeId = type[rowDefaultType].key
            pickerViewType.selectRow(rowDefaultType, inComponent: 0, animated: true)
        case 3:
            txtTahun.text = "\(years[defaultYear])"
            pickerViewYear.selectRow(defaultYear, inComponent: 0, animated: true)
        default:
            return
        }
        
        self.view.endEditing(true)
    }
    
    func getDataVehicles(value: String, firstLoad: Bool) {
        imageViewMerk.isHidden = true
        spinMerk.isHidden = false
        spinType.isHidden = true
        txtMerk.text = ""
        txtType.text = ""
        txtMerk.isUserInteractionEnabled = false
        networkService.fetchVehicles(type: value, success: { (data) in
            self.merk.removeAll()
            if(data?.message == "OK"){
                self.merk += (data?.result)! as! [String]
                
                if(self.merk.count > 0 ){
                    if (firstLoad) {
                        for (idx,brand) in self.merk.enumerated() {
                            if(!self.consumer.vehicleList[0].brandName.isEmpty){
                                if (brand == self.consumer.vehicleList[0].brandName) {
                                    self.rowDefaultMerk = idx
                                    
                                    self.txtMerk.text = self.merk[self.rowDefaultMerk]
                                    self.pickerViewMerk.selectRow(self.rowDefaultMerk, inComponent: 0, animated: true)
                                    self.getDataBrand(param1: self.valueRb, param2: self.txtMerk.text!)
                                }
                            } else {
                                if (brand == "Honda" && self.valueRb == "M") {
                                    self.rowDefaultMerk = idx
                                }else if (brand == "BMW" && self.valueRb == "C"){
                                    self.rowDefaultMerk = idx
                                }
                            }
                        }
                    }
                    
                    self.imageViewMerk.isHidden = false
                    self.spinMerk.isHidden = true
                    self.txtMerk.isUserInteractionEnabled = true

                }
                
            }else{
                print((data?.errorMessage)!)
            }
            
        }) { (error) in
//            self.alert(title: "Error Connection", msg: error?.localizedDescription ?? "")
            print(error?.localizedDescription ?? "")
        }
    }
    
    func getDataBrand(param1: String, param2: String) {
        imageViewType.isHidden = true
        spinType.isHidden = false
        txtType.text = ""
        txtType.isUserInteractionEnabled = false
        networkService.fetchBrands(type: param1, brand: param2, success: { (data) in
            self.formsList.removeAll()
            if(data?.message == "OK"){
                self.brands = data?.result
                for node in self.brands! {
                    if(node.vehicleType == self.valueRbFull.rawValue && node.brandName == param2){
                        self.formsList.updateValue(node.brandType!, forKey: node.id!)
                    }
                }
                
                if(self.type.count > 0){
                    if(!self.consumer.vehicleList[0].brandType.isEmpty){
                        var idx = 0
                        for (_, brand) in self.type {
                            if (brand == self.consumer.vehicleList[0].brandType) {
                                self.rowDefaultType = idx
                                self.txtType.text   = self.type[self.rowDefaultType].value
                                self.txtTypeId      = self.type[self.rowDefaultType].key
                            }
                            idx += 1
                        }
                    }else {
                        self.rowDefaultType = 0
                    }
                    self.pickerViewType.selectRow(self.rowDefaultType, inComponent: 0, animated: true)
                    self.imageViewType.isHidden = false
                    self.spinType.isHidden = true
                    self.txtType.isUserInteractionEnabled = true
                }
            }else{
                print((data?.errorMessage)!)
            }
            
        }) { (error) in
//            self.alert(title: "Error Connection", msg: error?.localizedDescription ?? "")
            print(error?.localizedDescription ?? "")
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signUpClick(_ sender: UIButton) {
        validator.validateAll(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField) {
        self.activeField = nil
    }
    
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        self.activeField = sender
    }
    
    
}

extension RegisterVehicleController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView {
        case pickerViewType:
            return type.count
        case pickerViewMerk:
            return merk.count
        case pickerViewYear:
            return years.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        switch pickerView {
        case pickerViewType:
            return pickerViewType.getFontAttribute(type[row].value)
        case pickerViewMerk:
            return pickerViewMerk.getFontAttribute(merk[row])
        case pickerViewYear:
            return pickerViewYear.getFontAttribute("\(years[row])")
        default:
            return nil
        }
    }
    
}

extension RegisterVehicleController : MyAlertSuccessDelegate{
    func finishButtonTapped() {
          self.goToLogin()
    }
}
