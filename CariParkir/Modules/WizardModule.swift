//
//  WizardModule.swift
//  CariParkir
//
//  Created by vti on 9/6/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class WizardModule: NSObject {
    
    func instantiateWithView() -> UIPageViewController {
        let storyboard = UIStoryboard(name: "Wizard", bundle: nil)
        let viewWizard = storyboard.instantiateInitialViewController() as! UIPageViewController
        return viewWizard
    }
    
}
