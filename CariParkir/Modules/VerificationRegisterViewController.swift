//
//  VerificationRegisterViewController.swift
//  CariParkir
//
//  Created by Muhammad Noor on 03/04/2018.
//  Copyright © 2018 PT NOSTRA. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class VerificationRegisterViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var lblKirimUlang: UILabel!
    @IBOutlet weak var btnLanjut: UIButton!
    @IBOutlet weak var lblKonfirmasiText: UILabel!
    @IBOutlet weak var txtInput1: UITextField!
    @IBOutlet weak var txtInput2: UITextField!
    @IBOutlet weak var txtInput3: UITextField!
    @IBOutlet weak var txtInput4: UITextField!
    @IBOutlet weak var txtInput5: UITextField!
    @IBOutlet weak var txtInput6: UITextField!
    
    let transitionManager = TransitionManager()
    var dataToken: GetToken?
    var dataOTP: GetOTP?
    var button: UIButton!
    var iconClick : Bool!
    var networkService: INetworkService!
    let validator = Validator()
    var consumer = Consumer()
    private var fixTemp = ""
    var tempOTP: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelUnderline(label: lblKirimUlang, filter: nil)
        lblKonfirmasiText.text = "Masukan kode verifikasi yang Anda\n terima di nomor"
        
        txtCustom()
        btnCustom()
    }
    
    func txtCustom()  {
        button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_edit"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        button.frame = CGRect(x: 0, y: 5, width: CGFloat(20), height: CGFloat(20))
        button.addTarget(self, action: #selector(self.showHide), for: .touchUpInside)
        txtPhoneNumber.rightView = button
        txtPhoneNumber.rightViewMode = .always
        
        txtInput1.delegate = self
        txtInput2.delegate = self
        txtInput3.delegate = self
        txtInput4.delegate = self
        txtInput5.delegate = self
        txtInput6.delegate = self
        
        let gestureResendToken = UITapGestureRecognizer(target: self, action: #selector(resendOTP))
        lblKirimUlang.isUserInteractionEnabled = true
        lblKirimUlang.addGestureRecognizer(gestureResendToken)
        
        txtPhoneNumber.text = consumer.phone
        txtPhoneNumber.setLeftPaddingPoints(10)
        addBottomBorderTo(textField: txtPhoneNumber, color: UIColor.grey)
        addBottomBorderTo(textField: txtInput1, color: UIColor.grey)
        addBottomBorderTo(textField: txtInput2,color: UIColor.grey)
        addBottomBorderTo(textField: txtInput3,color: UIColor.grey)
        addBottomBorderTo(textField: txtInput4,color: UIColor.grey)
        addBottomBorderTo(textField: txtInput5, color: UIColor.grey)
        addBottomBorderTo(textField: txtInput6, color: UIColor.grey)
    }
    
    func addBottomBorderTo(textField:UITextField, color: UIColor) {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        layer.frame = CGRect(x: 0.0, y: textField.frame.size.height - 2.0, width: textField.frame.size.width, height: 2.0)
        textField.layer.addSublayer(layer)
    }
    
    func closeLogin() {
        let module = HomeModule()
        let controller = module.instantiateWithNavigation()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    @IBAction func whenButtonNextTapped(_ sender: Any) {
        let a = txtInput1.text
        let b = txtInput2.text
        let c = txtInput3.text
        let d = txtInput4.text
        let e = txtInput5.text
        let f = txtInput6.text
        
        
        if let temp = a, let temp2 = b, let temp3 = c,let temp4 = d, let temp5 = e,let temp6 = f {
            print("otp_key \(temp)\(temp2)\(temp3)\(temp4)\(temp5)\(temp6)")
            tempOTP = "\(temp)\(temp2)\(temp3)\(temp4)\(temp5)\(temp6)"
        }
        
        print(tempOTP)
        print("Password : \(consumer.password)")
        if consumer.source == "" {
            consumer.password = tempOTP
            
        }
        print("Password : \(consumer.password)")
        
        consumer.phone = txtPhoneNumber.text!
        print("username : \(consumer.username)")
        print("first_name : \(consumer.firstName)")
        print("last_name : \(consumer.lastName)")
        print("phone : \(consumer.phone)")
        print("password : \(consumer.password)")
        print("birth_date : \(consumer.birthDate)")
        print("gender : \(consumer.gender)")
        print("status_login : \(consumer.source)")
        print("consumer : \(consumer.vehicleList[0].vehicleYear)")
        
        if (consumer.source == "GOOGLE") || (consumer.source == "FACEBOOK") {
            networkService.fetchValidateRegisterOTPAnother(phone: txtPhoneNumber.text!, otp_key: tempOTP, success: { (data) in
                if (data?.message == "OK") {
                    self.goToHome()
                } else {
                    self.alertError(title: String(describing: (data?.message)! + " unknown"), msg: String(describing: data?.result))
                }
            }) { (error) in
                ProgressView.shared.hideProgressView()
                self.alertError(title: "ERROR", msg: Config.instance.getErrorMessage(error: error!))
            }
        }
        else if (consumer.source == "") {
            ProgressView.shared.showProgressView(self.view)
            networkService.fetchValidateRegisterOTP(consumer: consumer, success: { (data) in
                if (data?.message == "OK") {
                    print("data: \(String(describing: data?.result))")
                    self.nextStep()
                } else {
                    self.alertError(title: String(describing: (data?.message)! + " unknown"), msg: String(describing: data?.result))
                }
            }) { (error) in
                ProgressView.shared.hideProgressView()
                self.alertError(title: "ERROR", msg: Config.instance.getErrorMessage(error: error!))
            }
        }
        
    
    }
    
    @objc func resendOTP() {
        let temp = String(describing: consumer.phone)
        if consumer.phone != "" {
            let tempEndIndex = temp.endIndex
            let tmpStartIndex = temp.index(tempEndIndex, offsetBy: -11)
    
            let range = Range(uncheckedBounds: (lower: tmpStartIndex, upper: tempEndIndex))
            fixTemp = "62\(temp[range])"
        }
        
        print("index: \(fixTemp)")
        
        ProgressView.shared.showProgressView(self.view)
        
        networkService.fetchRequestRegisterOTP(consumer: consumer, success: { (data) in
            ProgressView.shared.hideProgressView()
            if(data?.message == "OK") {
                print("data \(String(describing: data?.result))")
                print("data \(String(describing: data?.message))")
                UserDefaults.standard.setPhoneNumber(value: self.txtPhoneNumber.text!)
            }
            else {
                ProgressView.shared.hideProgressView()
            }
        }) { (error) in
            ProgressView.shared.hideProgressView()
            self.alertError(title: "Error", msg: Config.instance.getErrorMessage(error: error!))
        }
    }
    
    func goToHome()  {
        if consumer != nil {
            ProgressView.shared.showProgressView(self.view)
            if consumer.source != "" {
                networkService.sendSignUpOTP(consumer: consumer, success: { (data) in
                    ProgressView.shared.hideProgressView()
                    if(data?.message == "OK"){
                        print("data: \(String(describing: data?.message))")
                        self.nextStep()
                    }else{
                        self.alertError(title: (data?.message)!, msg: (data?.result)!)
                    }
                }) { (error) in
                    ProgressView.shared.hideProgressView()
                    self.alertError(title: "Gagal", msg: "Cek Koneksi Anda dan Coba lagi.")
                }
            }
        }

    }
    
    func nextStep()  {
        ProgressView.shared.showProgressView(self.view)
        if (consumer.source == "GOOGLE") || (consumer.source == "FACEBOOK") {
            networkService.fetchParkToken(username: consumer.username, password: consumer.password, success: { (data) in
                if(data?.message == "OK"){
                    self.dataToken = data?.result
                    
                    print("data fetch token : \(String(describing: data?.result))")
                    UserDefaults.standard.setUserName(value: self.txtPhoneNumber.text!)
                    UserDefaults.standard.setFullName(value: self.dataToken!.fullName)
                    UserDefaults.standard.setToken(value: self.dataToken!.token)
                    UserDefaults.standard.setRefreshToken(value: self.dataToken!.refreshToken)
                    self.saveDeviceToken()
                }else{
                    ProgressView.shared.hideProgressView()
                    self.alertError(title: "Error login", msg: data!.errorMessage)
                }
            }) { (error) in
                ProgressView.shared.hideProgressView()
                self.alertError(title: "ERROR", msg: Config.instance.getErrorMessage(error: error!))
            }
        } else {
            networkService.fetchValidateLoginOTP(phone: self.txtPhoneNumber.text!, otp_key: tempOTP, success: { (data) in
                if (data?.message == "OK") {
                    self.dataToken = data?.result
                    print("data: \(String(describing: data?.result))")
                    
                    UserDefaults.standard.setPhoneNumber(value: self.txtPhoneNumber.text!)
                    UserDefaults.standard.setFullName(value: (self.dataToken?.fullName)!)
                    UserDefaults.standard.setToken(value: (self.dataToken?.token)!)
                    UserDefaults.standard.setRefreshToken(value: (self.dataToken?.refreshToken)!)
                    
                    self.saveDeviceToken()
                }
            }) { (error) in
                ProgressView.shared.hideProgressView()
                self.alertError(title: "ERROR", msg: "Test "+Config.instance.getErrorMessage(error: error!))
            }
        }
    }
    
    func saveDeviceToken() {
        if let token = InstanceID.instanceID().token() {
            NetworkService.sharedInstance.registerDeviceToken(token: token, success: {data in
                if (data?.message == "OK") {
                    UserDefaults.standard.setIsLoggedIn(value: true)
                    self.loadUserProfile()
                } else {
                    self.alertError(title: "Error", msg: data?.errorMessage ?? "")
                }
            }, failure: {error in
                self.alertError(title: "ERROR", msg: Config.instance.getErrorMessage(error: error!))
            })
            ProgressView.shared.hideProgressView()
        } else {
            ProgressView.shared.hideProgressView()
            self.alertError(title: "ERROR", msg: "Retrieving Token failed. Please restart your application")
        }
    }
    
    func loadUserProfile() {
        NetworkService.sharedInstance.fetchConsumerDetail(success: {data in
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                let defaultVehicle: ConsumerVehicle? = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id)
                if (defaultVehicle == nil) {
                    let vehicle:ConsumerVehicle = Config.instance.loggedInUser!.vehicleList[0]
                    UserDefaults.standard.setDefaultVehicle(consumerId: Config.instance.loggedInUser!.id, vehicle: vehicle)
                }
                if(Config.instance.loggedInUser!.birthDate.isEmpty || Config.instance.loggedInUser!.phone.isEmpty){
                    UserDefaults.standard.setIsSosmed(value: true)
                }
                self.closeLogin()
            } else {
                ProgressView.shared.hideProgressView()
                self.alertError(title: "Error", msg: data?.errorMessage ?? "")
            }
        }, failure: {error in
            ProgressView.shared.hideProgressView()
            self.alertError(title: "ERROR", msg: Config.instance.getErrorMessage(error: error!))
        })
    }
    
    @IBAction func showHide(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
        
    }
    
    func btnCustom()  {
        btnLanjut.backgroundColor = .clear
        btnLanjut.layer.cornerRadius = 3
        btnLanjut.layer.borderWidth = 1
        btnLanjut.layer.borderColor = UIColor.white.cgColor
    }
    
    func goToSignUp() {
        let module = SignupModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1) && (string.count > 0) {
            if textField == txtInput1 {
                txtInput2.becomeFirstResponder()
                addBottomBorderTo(textField: txtInput1, color: UIColor.yellow)
            }
            if textField == txtInput2 {
                txtInput3.becomeFirstResponder()
                addBottomBorderTo(textField: txtInput2, color: UIColor.yellow)
            }
            if textField == txtInput3 {
                txtInput4.becomeFirstResponder()
                addBottomBorderTo(textField: txtInput3, color: UIColor.yellow)
            }
            if textField == txtInput4 {
                txtInput5.becomeFirstResponder()
                addBottomBorderTo(textField: txtInput4, color: UIColor.yellow)
            }
            if textField == txtInput5 {
                txtInput6.becomeFirstResponder()
                addBottomBorderTo(textField: txtInput5, color: UIColor.yellow)
            }
            if textField == txtInput6 {
                txtInput6.resignFirstResponder()
                addBottomBorderTo(textField: txtInput6, color: UIColor.yellow)
            }
            textField.text = string
            return false
        } else if ((textField.text?.count)! >= 1) && (string.count == 0) {
            if textField == txtInput2 {
                addBottomBorderTo(textField: txtInput2, color: UIColor.grey)
                txtInput1.becomeFirstResponder()
            }
            if textField == txtInput3 {
                addBottomBorderTo(textField: txtInput3, color: UIColor.grey)
                txtInput2.becomeFirstResponder()
            }
            if textField == txtInput4 {
                addBottomBorderTo(textField: txtInput4, color: UIColor.grey)
                txtInput3.becomeFirstResponder()
            }
            if textField == txtInput5 {
                addBottomBorderTo(textField: txtInput5, color: UIColor.grey)
                txtInput4.becomeFirstResponder()
            }
            if textField == txtInput6 {
                addBottomBorderTo(textField: txtInput6, color: UIColor.grey)
                txtInput5.becomeFirstResponder()
            }
            if textField == txtInput1 {
                addBottomBorderTo(textField: txtInput1, color: UIColor.grey)
                txtInput1.resignFirstResponder()
            }
            textField.text = ""
            return false
        } else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        return true
    }
    
    func alertSuccess(title: String ,msg: String){
        let storyBoard = UIStoryboard(name: "AlertSuccess", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "alertsuccess") as! MyAlertSuccessController
        customAlert.content = MyAlertSuccess(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertSuccessDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func alertError(title: String ,msg: String){
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func labelUnderline(label: UILabel, filter: String?) {
        let textRange: NSRange?
        let text = label.text
        
        if (filter != nil) {
            textRange = (text! as NSString).range(of: filter!)
        }else {
            textRange = NSMakeRange(0, (text?.count)!)
        }
        let attributedText = NSMutableAttributedString(string: text!)
        attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange!)
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: textRange!)
        label.attributedText = attributedText
    }

}

extension VerificationRegisterViewController : MyAlertSuccessDelegate{
    func finishButtonTapped() {
        self.goToLogin()
    }
}
