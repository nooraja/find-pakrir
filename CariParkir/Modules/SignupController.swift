//
//  SignupController.swift
//  CariParkir
//
//  Created by vti on 8/21/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import DLRadioButton
import Firebase
import FirebaseAuth
import FacebookCore
import FacebookLogin
import GoogleSignIn

class SignupController: UIViewController, ValidationDelegate {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNamaDepan: UITextField!
    @IBOutlet weak var txtNamaBelakang: UITextField!
    @IBOutlet weak var txtTtl: CustomTextField!
    @IBOutlet weak var txtTlp: UITextField!
    @IBOutlet weak var rbPria: DLRadioButton!
    @IBOutlet weak var rbWanita: DLRadioButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblNamaDepan: UILabel!
    @IBOutlet weak var lblRightLine: UIView!
    @IBOutlet weak var lblLeftLine: UIView!
    @IBOutlet weak var lblOrConnect: UILabel!
    weak var activeField: UITextField?
    @IBOutlet weak var lblEmailErr: UILabel!
    @IBOutlet weak var lblNamaBelakangErr: UILabel!
    @IBOutlet weak var lblNamaDepanErr: UILabel!
    @IBOutlet weak var lblTtlErr: UILabel!
    @IBOutlet weak var lblTlpErr: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblFAQ: UILabel!
    
    
    var networkService: INetworkService!
    var button1: UIButton!
    var button2: UIButton!
    var gender: Gender = .male
    let transitionManager = TransitionManager()
    let validator = Validator()
    var tempPassword = ""
    
    var consumer = Consumer()
    var listConsumerVehicles = [ConsumerVehicle()]
    var whenHidden: Bool!
    var flagWizard: Bool!
    var pickerViewDate: CustomPickerView!
    var selectedRow = 0;
    var currentYear: Int!
    var months = [String]()
    var years = [Int]()
    var defaultMonth: Int = 0
    var defaultYear: Int = 0
    var month: Int = 0 {
        didSet {
            pickerViewDate.selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    var year: Int = 0 {
        didSet {
            pickerViewDate.selectRow(years.index(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    let loginFbManager: LoginManager = LoginManager()
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        validator.validateAll(self)
    }
    
    func validationSuccessful(_ validator: Validator) {
        self.performSegue(withIdentifier: "toVehicle", sender: self)
    }
    
    func validationFailed(_ validator: Validator, _ errors:[(Validatable ,ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.underlinedRed()
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toVehicle"?:
            let dest = segue.destination as! RegisterVehicleController
            dest.transitioningDelegate = self.transitionManager
            networkService = NetworkService.sharedInstance
            dest.networkService = networkService
            
            if consumer.source != "" {
                dest.consumer = consumer
                dest.consumer.vehicleList = listConsumerVehicles
                dest.consumer.username = txtEmail.text!
                dest.consumer.firstName = txtNamaDepan.text!
                dest.consumer.lastName = txtNamaBelakang.text!
                dest.consumer.birthDate = consumer.source != "" && txtTtl.text == "" ? "" : "\(String(format: "%02d", defaultMonth))-\(defaultYear)"
                dest.consumer.password = tempPassword
                dest.consumer.gender = gender
                dest.consumer.phone = txtTlp.text!
            } else {
                dest.consumer = consumer
                dest.consumer.vehicleList = listConsumerVehicles
                dest.consumer.username = txtEmail.text!
                dest.consumer.firstName = txtNamaDepan.text!
                dest.consumer.lastName = txtNamaBelakang.text!
                dest.consumer.birthDate = consumer.source != "" && txtTtl.text == "" ? "" : "\(String(format: "%02d", defaultMonth))-\(defaultYear)"
                dest.consumer.password = "123456"
                dest.consumer.gender = gender
                dest.consumer.phone = txtTlp.text!
            }
        default:
            break
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.commonSetup()
        NotificationCenter.default.addObserver(
            self,
            selector:  #selector(deviceDidRotate),
            name: .UIDeviceOrientationDidChange,
            object: nil
        )
        
        btnCustom()
        txtCustom()
        txtLoadUnderline()
        radioButtonAction(radioButton: rbPria)
        radioButtonAction(radioButton: rbWanita)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.hideKeyboard()
    }
    
    func resetForm() {
        txtEmail.text = ""
        txtNamaDepan.text = ""
        txtTtl.text = ""
        txtTlp.text = ""
        rbPria.sendActions(for: .touchUpInside)
        self.consumer = Consumer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if consumer.source != "" {
//            resetForm()
        }
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField) {
        self.activeField = nil
    }
    
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        self.activeField = sender
    }
    
    func radioButtonAction(radioButton : DLRadioButton){
        radioButton.addTarget(self, action: #selector(self.logSelectedButton), for: UIControlEvents.touchUpInside);
    }
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        switch radioButton.titleLabel!.text! {
        case "Pria":
            gender = .male
        case "Wanita":
            gender = .female
        default:
            break
        }
    }
    
    func btnCustom() {
        btnNext.backgroundColor = .clear
        btnNext.layer.cornerRadius = 3
        btnNext.layer.borderWidth = 1
        btnNext.layer.borderColor = UIColor.white.cgColor
        
        btnFb.layer.cornerRadius = 3
        btnGoogle.layer.cornerRadius = 3

    }
    
    func txtCustom() {
        whenHidden = true
        whenHidden = true
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let image = UIImage(named: "ico_calendar")
        imageView.image = image
        txtTtl.rightView = imageView
        txtTtl.rightViewMode = .always
        
        let gestureRecognizerFAQ = UITapGestureRecognizer(target: self, action: #selector(SignupController.goFAQ(sender:)))
        lblFAQ.isUserInteractionEnabled = true
        lblFAQ.addGestureRecognizer(gestureRecognizerFAQ)
        
        let gestureRecognizerLogin = UITapGestureRecognizer(target: self, action: #selector(SignupController.goToLogin(_:)))
        lblLogin.isUserInteractionEnabled = true
        lblLogin.addGestureRecognizer(gestureRecognizerLogin)

//        validator.registerField(txtEmail, errorLabel: lblEmailErr, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
        txtEmail.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        validator.registerField(txtNamaDepan, errorLabel: lblNamaDepanErr, rules: [RequiredRule()])
        txtNamaDepan.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        validator.registerField(txtNamaBelakang, errorLabel: lblNamaBelakangErr, rules: [RequiredRule()])
        txtNamaBelakang.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        validator.registerField(txtTtl, errorLabel: lblTtlErr, rules: [RequiredRule()])
        txtTtl.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        validator.registerField(txtTlp, errorLabel: lblTlpErr, rules: [RequiredRule(), PhoneNumberRule(message: "Invalid Phone Number")])
        txtTlp.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    
    func txtLoadUnderline() {
        txtEmail.underlined()
        txtNamaDepan.underlined()
        txtNamaBelakang.underlined()
        txtTtl.underlined()
        txtTlp.underlined()
        
        labelUnderline(label: lblLogin, filter: "LOGIN")
        labelUnderline(label: lblFAQ, filter: nil)
    }
    
    func labelUnderline(label: UILabel, filter: String?) {
        let textRange: NSRange?
        let text = label.text
        let attributedText = NSMutableAttributedString(string: text!)
        if (filter != nil) {
            textRange = (text! as NSString).range(of: filter!)
            attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: textRange!)
        }else {
            textRange = NSMakeRange(0, (text?.count)!)
            attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange!)
        }
        
        label.attributedText = attributedText
    }
    
    func goFAQ(sender: UITapGestureRecognizer) {
        if (UserDefaults.standard.isLoggedIn() == false){
            self.performSegue(withIdentifier: "showHelp", sender: self)
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        validator.validateField(textField){ error in
            if error != nil {
                error?.errorLabel?.isHidden = false
                error?.errorLabel?.text = error?.errorMessage
            }
        }
    }
    
    func closeLogin()  {
        let module = HomeModule()
        let controller = module.instantiateWithNavigation()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    @IBAction func goToLogin(_ sender: UIButton) {
        if (self.flagWizard) {
            let module = LoginModule()
            let controller = module.instantiateWithView()
            controller.modalPresentationStyle = .custom
            controller.modalTransitionStyle = .crossDissolve
            UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                UIApplication.shared.delegate!.window!!.rootViewController = controller
            }, completion: nil)
        }
        performSegue(withIdentifier: "unwindSegueToLogin", sender: self)
    }
    
    func commonSetup() {
        pickerViewDate = CustomPickerView()
        
        var years: [Int] = []
        currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
        
        if years.count == 0 {
            var oldyear: Int = 1900
            for _ in oldyear...currentYear {
                years.append(oldyear)
                oldyear += 1
            }
        }
        self.years = years
        
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(DateFormatter().monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months
        
        pickerViewDate.delegate = self
        pickerViewDate.dataSource = self
        
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        
        pickerViewDate.selectRow(currentMonth - 1, inComponent: 0, animated: false)
        pickerViewDate.selectRow(years.index(of: currentYear)!, inComponent: 1, animated: true)
        
        pickerViewDate.backgroundColor = .white
        pickerViewDate.showsSelectionIndicator = true
        
        let toolBar = UIToolbar()
        toolBar.barTintColor = UIColor.yellow_button
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Pilih", style: .done, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Batal", style: .plain, target: self, action: #selector(self.canclePicker))
        
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtTtl.inputView = pickerViewDate
        txtTtl.inputAccessoryView = toolBar
        
        self.month = currentMonth
        self.year = currentYear
    }
    
    func donePicker(_ sender: UIBarButtonItem) {
        defaultMonth = self.month
        defaultYear = self.year
        self.setTtlResult(month: defaultMonth, year: defaultYear)
        self.view.endEditing(true)
    }
    
    func canclePicker(_ sender: UIBarButtonItem) {
        if (defaultMonth != 0 && defaultYear != 0){
            pickerViewDate.selectRow(defaultMonth - 1, inComponent: 0, animated: false)
            pickerViewDate.selectRow(years.index(of: defaultYear)!, inComponent: 1, animated: true)
            self.setTtlResult(month: defaultMonth, year: defaultYear)
        }
        self.view.endEditing(true)
    }
    
    func deviceDidRotate() {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            txtLoadUnderline()
        default:
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        }
    }
    
    func setTtlResult(month: Int, year: Int) {
        let monthNumber = month
        let fmt = DateFormatter()
        let monthString = fmt.shortMonthSymbols[monthNumber - 1]
        
        let resultYear = currentYear - year
        if (resultYear < 15) {
            lblTtlErr.isHidden = false
            lblTtlErr.text? = "Anda harus berusia minimal 15 tahun untuk melakukan registrasi"
            txtTtl.underlinedRed()
        }else{
            txtTtl.underlined()
            lblTtlErr.isHidden = true
        }
        
        txtTtl.text = "\(monthString.uppercased())/\(year)"
    }
    
    func splitName(_ fullName: String) -> (String, String) {
        let splitUserName = fullName.components(separatedBy: " ")
        let firstName = splitUserName[0]
        var lastName = ""
        if splitUserName.count > 1 {
            lastName = (fullName as NSString).substring(from: firstName.count + 1)
        }
        return (firstName, lastName)
    }
    
    @IBAction func fbBtnClicked(_ sender: UIButton) {
        facebookAuth()
    }
    
    @IBAction func gPlusBtnClicked(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func alertError(title: String = "Oops!", msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func facebookAuth() {
        loginFbManager.logIn([.publicProfile, .email, .custom("user_birthday")], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                self.firebaseLoginHandler(nil, error.localizedDescription)
                print(error.localizedDescription)
            case .cancelled:
                self.firebaseLoginHandler(nil, "Login cancelled")
                print("Login cancelled")
            case .success( _, _, let accessToken):
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                ProgressView.shared.showProgressView(self.view)
                Auth.auth().signIn(with: credential, completion: {
                    (user, error) in
                    self.firebaseLoginHandler(user, error?.localizedDescription)
                    if (error?.localizedDescription != nil) { } else {
                        let graph: GraphRequest = GraphRequest(graphPath: "/me?fields=id,name,gender,birthday", accessToken: accessToken)
                        graph.start({(response, result) in
                            ProgressView.shared.hideProgressView()
                            switch result {
                            case .success(let resp):
                                if let me = resp.dictionaryValue {
                                    let birthString = me["birthday"] as! String?
                                    if (birthString != nil) {
                                        let birthday = birthString!.components(separatedBy: "/")
                                        self.month = Int(birthday[0])!
                                        self.year = Int(birthday[2])!
                                        self.donePicker(UIBarButtonItem(title: "", style: .done, target: nil, action: nil))
                                        self.txtTtl.isUserInteractionEnabled = false
                                    }
                                    
                                    if let gender = me["gender"] as! String? {
                                        if (gender == "male") {
                                            self.rbPria.sendActions(for: .touchUpInside)
                                        } else if (gender == "female") {
                                            self.rbWanita.sendActions(for: .touchUpInside)
                                        }
                                    }
                                    self.consumer.source = "FACEBOOK"
                                    if self.whenHidden == true {
                                        self.btnFb.isHidden = self.whenHidden
                                        self.btnGoogle.isHidden = self.whenHidden
                                        self.lblOrConnect.isHidden = self.whenHidden
                                        self.lblLeftLine.isHidden = self.whenHidden
                                        self.lblRightLine.isHidden = self.whenHidden
                                        self.lblFAQ.topAnchor.constraint(equalTo: self.btnNext.bottomAnchor, constant: 20).isActive = true
                                        self.mainView.heightAnchor.constraint(equalToConstant: 590).isActive = true
                                        self.lblNamaDepan.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 120).isActive = true
                                        self.whenHidden = false
                                    }
                                }
                                
                                break
                            case .failed(let error):
                                self.alertError(msg: error.localizedDescription)
                                break
                            }
                        })
                    }
                })
            }
        }
    }
    
    func firebaseLoginHandler(_ user: User?, _ message: String?) {
        if message != nil {
            ProgressView.shared.hideProgressView()
            self.alertError(msg: message!)
        } else {
            let splitUserName = user?.displayName!.components(separatedBy: " ")
            let firstName = splitUserName![0]
            var lastName = ""
            if splitUserName!.count > 1 {
                lastName = ((user?.displayName)! as NSString).substring(from: firstName.count + 1)
            }
            self.txtNamaDepan.text = firstName
            self.txtNamaBelakang.text = lastName
            self.txtEmail.text = user!.email
            self.tempPassword = user!.uid
        }
    }
}

extension SignupController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch component {
        case 0:
            return pickerViewDate.getFontAttribute(months[row])
        case 1:
            return pickerViewDate.getFontAttribute("\(years[row])")
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = pickerView.selectedRow(inComponent: 0)+1
        let year = years[pickerView.selectedRow(inComponent: 1)]
        
        self.month = month
        self.year = year
    }
}

extension SignupController: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let idToken = user.authentication.idToken!
            let accessToken = user.authentication.accessToken!
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
            ProgressView.shared.showProgressView(self.view)
            Auth.auth().signIn(with: credential, completion: {
                (user, error) in
                self.firebaseLoginHandler(user, error?.localizedDescription)
                if (error?.localizedDescription != nil) { } else {
                    NetworkService.sharedInstance.getGoogleUserInfo(token: accessToken, success: {googleUser in
                        ProgressView.shared.hideProgressView()
//                        let birthString = googleUser.
//                        if (birthString != nil) {
//                            let birthday = birthString!.components(separatedBy: "/")
//                            self.month = Int(birthday[0])!
//                            self.year = Int(birthday[2])!
//                            self.donePicker(UIBarButtonItem(title: "", style: .done, target: nil, action: nil))
//                            self.txtTtl.isUserInteractionEnabled = false
//                        }
                        if googleUser!.gender != "" {
                            if (googleUser!.gender == "male") {
                                self.rbPria.sendActions(for: .touchUpInside)
                                self.rbPria.isUserInteractionEnabled = false
                                self.rbWanita.isUserInteractionEnabled = false
                            } else if (googleUser!.gender == "female") {
                                self.rbWanita.sendActions(for: .touchUpInside)
                                self.rbPria.isUserInteractionEnabled = false
                                self.rbWanita.isUserInteractionEnabled = false
                            }
                        }
                        self.consumer.source = "GOOGLE"
                        if self.whenHidden == true {
                            self.btnFb.isHidden = self.whenHidden
                            self.btnGoogle.isHidden = self.whenHidden
                            self.lblOrConnect.isHidden = self.whenHidden
                            self.lblLeftLine.isHidden = self.whenHidden
                            self.lblRightLine.isHidden = self.whenHidden
                            self.lblFAQ.topAnchor.constraint(equalTo: self.btnNext.bottomAnchor, constant: 20).isActive = true
                            self.mainView.heightAnchor.constraint(equalToConstant: 590).isActive = true
                            self.lblNamaDepan.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 120).isActive = true
                            self.whenHidden = false
                        }
                    }, failure: {error in
                        self.alertError(msg: Config.instance.getErrorMessage(error: error!))
                    })
                }
            })
        } else {
            self.firebaseLoginHandler(nil, error?.localizedDescription)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        
    }
}
