//
//  ScanQRCodeController.swift
//  CariParkir
//
//  Created by vti on 8/26/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import AVFoundation
import UserNotifications
import QRCode

class ScanQRCodeController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var btnFlight: UIButton!
    @IBOutlet weak var btnMyQRCode: UIButton!
    @IBOutlet weak var btnInputKodeLoc: UIButton!
    @IBOutlet weak var stackPerentBtn: UIView!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var resultQrcode: String = "Show My QR Code"
    var customModal: CustomModal?
    var modalQRCode: CustomModal?
    var modalInputKodeLoc: CustomModal?
    var parentModal: UIView?
    var dataCheckIn: CheckIn?
    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]
    
    
    
    let logoImageView: UIImageView = {
        let image = UIImage(named: "ic_success")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.font = label.font.withSize(12)
        label.text = "Sukses! Tickets Detail sedang di proses."
        return label
    }()
    
    let titleLabel2: UILabel = {
        var myString: String = "Masukan Kode QR Code Parkir di sini!"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSFontAttributeName:UIFont(name: "Titillium-RegularUpright", size: 16.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: NSRange(location:8, length:19))
        
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = myMutableString
        return label
    }()
    
    let textFieldKode: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.layer.backgroundColor = UIColor.white.cgColor
        
        textField.layer.masksToBounds = false
        textField.layer.shadowColor = UIColor.edit_text_gray.cgColor
        textField.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        textField.layer.shadowOpacity = 0.0
        textField.layer.shadowRadius = 0.0
        textField.layoutMargins.bottom = CGFloat(40)
        textField.layoutMargins.top = CGFloat(10)
        return textField
    }()
    
    lazy var btnOK: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.green
        button.setTitle("OK", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnOK), for: .touchUpInside)
        return button
    }()
    
    lazy var btnClose: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("TUTUP", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnClose), for: .touchUpInside)
        return button
    }()
   
    lazy var btnYa: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Ya", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnYa), for: .touchUpInside)
        return button
    }()
    
    lazy var btnCancel: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Tidak", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnCancel), for: .touchUpInside)
        return button
    }()
    
    lazy var btnTutup: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Tutup", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.titleLabel?.font = UIFont(name: "Titillium-Bold", size: 12)
        button.addTarget(self, action: #selector(handleBtnCancel), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            
            captureSession?.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            captureSession?.startRunning()
            
            view.bringSubview(toFront: btnFlight)
            view.bringSubview(toFront: stackPerentBtn)
            view.bringSubview(toFront: btnMyQRCode)
            view.bringSubview(toFront: btnInputKodeLoc)
            
            qrCodeFrameView = UIView()
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.red.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            print(error)
            return
        }
        
        self.initializationModalQRCode()
        self.initializationModal()
        self.initializationModalInputKodeLoc()
        let logo = UIImage(named: "ico_nav_bar")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(ScanQRCodeController.keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ScanQRCodeController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = keyboardHeight - textFieldHeight
            modalInputKodeLoc?.moveY(height: (resultHeight - 150))
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = textFieldHeight - keyboardHeight
            modalInputKodeLoc?.moveY(height: (resultHeight + 150))
        }
    }
    
    @IBAction func clickBtnFlight(_ sender: UIButton) {
        self.toggleFlash()
    }
    
    @IBAction func clickBtnMyQRCode(_ sender: UIButton) {
        if(modalQRCode != nil){
            modalQRCode?.showModal()
        }
    }
    
    @IBAction func clickBtnInputKodeLoc(_ sender: UIButton) {
        if (modalInputKodeLoc != nil){
            modalInputKodeLoc?.showModal()
        }
    }
    
    func showPopup()  {
        if (customModal != nil){
            customModal?.showModal()
        }
    }
    
    func initializationModalQRCode() {
        let vehicle = Config.instance.selectedVehicle!
        let qrCodeString = Config.instance.loggedInUser!.qrCode + "/" + (vehicle.vehicleType == .motor ? "M" : "C") + "/" + vehicle.regCode
        let myQRCode: UIImageView = {
            var qrCode = QRCode(qrCodeString)
            qrCode?.size = CGSize(width: 170, height: 170)
            qrCode?.errorCorrection = .High
            let imageView = UIImageView(qrCode: qrCode!)
            return imageView
        }()
        
        modalQRCode = CustomModal()
        modalQRCode?.modal(self.view, title: "Scan My QR Code!", height: 300)
        modalQRCode?.addContent(view: myQRCode, space: -80, width: 170, height: 170)
        modalQRCode?.addBtnBottom(btn1: btnTutup, btn2: nil, height: 33)
    }
    
    func initializationModal() {
        customModal = CustomModal()
        customModal?.modal(self.view, title: "Konfirmasi Scan QR Code", height: 250)
        customModal?.addContent(view: logoImageView, space: -70, width: 100, height: 100)
        customModal?.addContent(view: titleLabel, space: 20, width: 0, height: 14, referenceBottom: logoImageView)
        customModal?.addBtnBottom(btn1: btnYa, btn2: nil, height: 35)
//        customModal?.addBtnBottom(btn1: btnCancel, btn2: btnOK, height: 35)
    }
    
    func initializationModalInputKodeLoc() {
        modalInputKodeLoc = CustomModal()
        modalInputKodeLoc?.modal(self.view, title: "Input Kode Lokasi", height: 204)
        modalInputKodeLoc?.addContent(view: titleLabel2, space: -30, width: 250, height: 20)
        modalInputKodeLoc?.addContent(view: textFieldKode, space: 0, width: 200, height: 38)
        modalInputKodeLoc?.addBtnBottom(btn1: btnClose, btn2: btnOK, height: 37)
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR/barcode is detected")
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
//            checkCode(metadataObj.stringValue)
            
            if metadataObj.stringValue != nil {
                resultQrcode = metadataObj.stringValue
                self.captureSession?.stopRunning()
                let qrCodeDataArray = resultQrcode.components(separatedBy: "/")
                if (qrCodeDataArray.count <= 1 || qrCodeDataArray.count > 3) {
                    self.alertError(msg: "Invalid QR Code")
                } else {
                    let vehicle = Config.instance.selectedVehicle!
                    let vehicleType = vehicle.vehicleType == .motor ? "M" : "C"
                    var vehicleValid = false
                    if (qrCodeDataArray.count == 2 && qrCodeDataArray[1] == vehicleType) {
                        vehicleValid = true
                    } else if (qrCodeDataArray.count == 3 && (qrCodeDataArray[1] == vehicleType || qrCodeDataArray[2] == vehicleType)) {
                        vehicleValid = true
                    }
                    if (vehicleValid) {
                        self.sendCheckIn(qrCode: qrCodeDataArray[0])
                    } else {
                        self.alertError(msg: "Kendaraan Anda tidak sesuai dengan tempat parkir anda check-in")
                    }
                }
            }
        }
    }
    
    func checkCode(_ code: String?) {
        if code != nil {
            resultQrcode = code!
            self.captureSession?.stopRunning()
            let qrCodeDataArray = resultQrcode.components(separatedBy: "/")
            if (qrCodeDataArray.count <= 1 || qrCodeDataArray.count > 3) {
                self.alertError(msg: "Invalid QR Code")
            } else {
                let vehicle = Config.instance.selectedVehicle!
                let vehicleType = vehicle.vehicleType == .motor ? "M" : "C"
                var vehicleValid = false
                if (qrCodeDataArray.count == 2 && qrCodeDataArray[1] == vehicleType) {
                    vehicleValid = true
                } else if (qrCodeDataArray.count == 3 && (qrCodeDataArray[1] == vehicleType || qrCodeDataArray[2] == vehicleType)) {
                    vehicleValid = true
                }
                if (vehicleValid) {
                    self.sendCheckIn(qrCode: qrCodeDataArray[0])
                } else {
                    self.alertError(msg: "Kendaraan Anda tidak sesuai dengan tempat parkir anda check-in")
                }
            }
        }
    }
    
    func handleBtnOK() {
        if(modalInputKodeLoc != nil){
            modalInputKodeLoc?.hideModal()
            checkCode(textFieldKode.text)
        }
    }
    func handleBtnClose() {
        if(modalInputKodeLoc != nil){
            modalInputKodeLoc?.hideModal()
        }
    }
    
    func handleBtnYa() {
        if (customModal != nil){
            customModal?.hideModal()
        }
        
        //        let ticketController = UIStoryboard(name: "Ticket", bundle: nil).instantiateViewController(withIdentifier: "TicketController") as! TicketController
        //        let ticketDetailController = UIStoryboard(name: "TicketDetail", bundle: nil).instantiateViewController(withIdentifier: "TicketDetailController") as! TicketDetailController
        
        //        let parkingHistory = ParkingHistory()
        //        parkingHistory.address = (dataCheckIn?.address)!
        //        parkingHistory.code = (dataCheckIn?.qrCode)!
        //        parkingHistory.parkingName = (dataCheckIn?.name)!
        //        parkingHistory.checkIn = (dataCheckIn?.checkInTime)!
        //        ticketDetailController.selectedTicket = parkingHistory
        
        //        _ = vcList.popLast()
        //        vcList.append(ticketController)
        //        vcList.append(ticketDetailController)
        //        vcList.append(homeController)
        
        //        self.navigationController!.setViewControllers(vcList, animated: true)
        
        var vcList:[UIViewController] = self.navigationController!.viewControllers
        let homeController = vcList[vcList.count - 2] as! HomeController
        homeController.networkService = NetworkService.sharedInstance
        homeController.flagShow = true
        homeController.tiketCode = (dataCheckIn?.qrCode)!
        //        self.navigationController?.popToViewController(homeController, animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func handleBtnCancel() {
        if(modalQRCode != nil){
            modalQRCode?.hideModal()
        }
    }
    
    
    func sendCheckIn(qrCode: String) {
        NetworkService.sharedInstance.checkIn(parkingQrCode: qrCode, success: { data in
            if (data?.message == "OK") {
                self.dataCheckIn = data?.result
                Config.instance.showNotification(title: "Check In", message: "Anda berhasil melakukan check in")
                self.showPopup()
            } else {
                ProgressView.shared.hideProgressView()
                Config.instance.showNotification(title: data!.message, message: data?.errorMessage ?? "")
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {error in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
        })
    }
    
    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default) {action in
            self.captureSession?.startRunning()
        })
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func toggleFlash() {
        if let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOnWithLevel(1.0)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    func checkingTrasKendaraan() {
        NetworkService.sharedInstance.fetchActiveTicket(success: {data in
            if (data?.message == "OK") {
                let vehicle = Config.instance.selectedVehicle!
                for ticket in data!.result {
                    if (vehicle.regCode == ticket.regCode) {
                        self.alertError(msg: "Kendaraan Anda masih dalam proses transaksi")
                        return
                    }
                }
            }
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
        
    }

    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
