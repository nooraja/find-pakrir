//
//  ForgotPasswordController.swift
//  CariParkir
//
//  Created by vti on 9/13/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var lblCallCenter: UILabel!
    @IBOutlet weak var lblEmailCenter: UILabel!
    @IBOutlet weak var btnKirim: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblInfo: UILabel!
    
    var networkService: INetworkService!
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtLoadUnderline()
        btnCustom()
        txtCustom()
    }
    
    func txtLoadUnderline() {
    }
    
    func btnCustom() {
        btnKirim.backgroundColor = .clear
        btnKirim.layer.cornerRadius = 3
        btnKirim.layer.borderWidth = 1
        btnKirim.layer.borderColor = UIColor.white.cgColor
    }
    
    func txtCustom() {
        lblInfo.text = "Jika nomor telepon hilang/bermasalah, \nAnda dapat mengganti nomor telepon\n dengan menghubungi kami di bawah ini:"
        lblCallCenter.text = "Call Center: (021) 1-500607"
        lblEmailCenter.text = "Email: support@cariparkir.co.id"
        labelUnderline(label: lblCallCenter, filter: "(021) 1-500607")
        labelUnderline(label: lblEmailCenter, filter: "support@cariparkir.co.id")
    }
    
    
    func labelUnderline(label: UILabel, filter: String?) {
        let textRange: NSRange?
        let text = label.text
        
        if (filter != nil) {
            textRange = (text! as NSString).range(of: filter!)
        } else {
            textRange = NSMakeRange(0, (text?.count)!)
        }
        
        let attributedText = NSMutableAttributedString(string: text!)
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange_underline, range: textRange!)
        label.attributedText = attributedText
    }
    
    @IBAction func actionClickKirim(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
        
    }
    
}

extension ForgotPasswordController: MyAlertSuccessDelegate{
    func finishButtonTapped() {
         self.goToLogin()
    }
}
