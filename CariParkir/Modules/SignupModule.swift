//
//  SignupModule.swift
//  CariParkir
//
//  Created by vti on 8/21/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class SignupModule: NSObject {
    
    func instantiateWithView() -> UIViewController {
        let storyboard = UIStoryboard(name: "Signup", bundle: nil)
        let viewSignup = storyboard.instantiateInitialViewController()!
        let signupController = viewSignup as! SignupController
        let networkService = NetworkService.sharedInstance
        signupController.networkService = networkService
        signupController.flagWizard = true
        
        return viewSignup
    }

}
