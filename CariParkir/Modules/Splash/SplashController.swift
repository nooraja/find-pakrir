//
//  SplashController.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/5/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import Firebase

class SplashController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        UserDefaults.standard.setIsWizard(value: false)
//        UserDefaults.standard.setIsTermAndCondition(value: false)
        if UserDefaults.standard.isWizard(){
            if UserDefaults.standard.isLoggedIn() {
                self.saveDeviceToken()
            } else {
                self.goToHome()
            }
        }else{
            UserDefaults.standard.setIsSosmed(value: false)
            UserDefaults.standard.setIsEmailVerified(value: false)
            UserDefaults.standard.setIsLoggedIn(value: false)
            self.goToWizard()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func goToWizard() {
        let module = WizardModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func goToHome() {
        let module = HomeModule()
        let controller = module.instantiateWithNavigation()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func refreshToken() {
        NetworkService.sharedInstance.refreshToken(success: {data in
            print(data!.toJSONString(prettyPrint: true)!)
            if (data?.message == "OK") {
                UserDefaults.standard.setToken(value: data!.result!.token)
                UserDefaults.standard.setRefreshToken(value: data!.result!.refreshToken)
                self.loadUserProfile()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    func loadUserProfile() {
        NetworkService.sharedInstance.fetchConsumerDetail(success: {data in
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                print("load data user : \(String(describing: data?.result))")
                let defaultVehicle: ConsumerVehicle? = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id)
                if (defaultVehicle == nil) {
                    let vehicle:ConsumerVehicle = Config.instance.loggedInUser!.vehicleList[0]
                    UserDefaults.standard.setDefaultVehicle(consumerId: Config.instance.loggedInUser!.id, vehicle: vehicle)
                }
                self.goToHome()
            } else {
                print(data?.errorMessage ?? "")
                UserDefaults.standard.setIsSosmed(value: false)
                UserDefaults.standard.setIsEmailVerified(value: false)
                UserDefaults.standard.setIsLoggedIn(value: false)
                self.goToLogin()
            }
        }, failure: {error in
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
        })
    }
    
    func saveDeviceToken() {
        if let token = InstanceID.instanceID().token() {
            NetworkService.sharedInstance.registerDeviceToken(token: token, success: {data in
                if (data?.message == "OK") {
                    print("data device: \(String(describing: data?.result))")
                    self.loadUserProfile()
                } else {
                    print(data?.errorMessage ?? "")
                    UserDefaults.standard.setIsSosmed(value: false)
                    UserDefaults.standard.setIsEmailVerified(value: false)
                    UserDefaults.standard.setIsLoggedIn(value: false)
                    self.goToLogin()
                }
            }, failure: {error in
                self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            })
        } else {
            self.alertError(msg: "Retrieving Token failed. Please restart your application")
        }
    }
    
    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
}
