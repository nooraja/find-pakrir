//
//  SearchController.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import SVPullToRefresh
import CoreLocation


class SearchController: UIViewController {

    @IBOutlet weak var searchField: UITextField?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var loadingBar: UIActivityIndicatorView?
    
    var keywords: String = ""
    var page: Int = 0
    var size: Int = 10
    var places: [Location] = []
    var networkService: INetworkService!
    var prevSearch: String = ""
    var isLastPage: Bool = false
    var lat: Double = 0.0
    var long: Double = 0.0
    let locationManager = CLLocationManager()
    let location =
        CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.register(UINib(nibName: "SearchPlaceViewCell", bundle: nil), forCellReuseIdentifier: "SearchPlaceViewCell")
        
        tableView?.delegate = self
        tableView?.dataSource = self
        searchField?.delegate = self
        
        searchField?.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchField?.becomeFirstResponder()
        loadingBar?.isHidden = true
        
        print("[viewDidLoad] lat: \(lat), long: \(long)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func searchLocations(text: String) {
        print("*** NEW SEARCH ***")
        loadingBar?.startAnimating()
        loadingBar?.isHidden = false
        
//        networkService.fetchLocationBy(name: text, page: page, size: size, success: { (result) in
//            if let result = result {
//                self.page = result.page
//                self.size = result.pageSize
//                self.places.append(contentsOf: result.data)
//
//                for i in 0..<result.data.count {
//                    print("name: \(result.data[i].name)")
//                    print("type: \(result.data[i].type)")
//                    result.data[i].distance="13 km"
//                    print("distance: \(result.data[i].distance)")
//                    print("")
//                }
//
//                if (result.data.count == 0) {
//                    self.isLastPage = true
//                }
//
//                self.tableView?.reloadData()
//            }
//            if (self.page == 0 && self.places.count > 0) {
//                self.tableView?.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
//            }
//            self.loadingBar?.stopAnimating()
//            self.loadingBar?.isHidden = true
//        }) { (error) in
//            self.loadingBar?.stopAnimating()
//            self.loadingBar?.isHidden = true
//        }

 //       networkService.fetchLocationWithDistanceBy(name: text, latlong: "-6.238031, 106.984723",

   //     let latlong = location.coordinate.latitude
 //       let longitude = location.coordinate.longitude
        
//        let latlong = locationManager.location?.coordinate.latitude
//        let longitude = locationManager.location?.coordinate.longitude
//        print("[viewDidLoad] Location Coordinate: \(locationManager.location?.coordinate)")

        var newLat: Double = 0.0
        var newLong: Double = 0.0
//
        if let latlong = locationManager.location?.coordinate.latitude{
            newLat = latlong
            print(newLat)
        }
        if let longitude = locationManager.location?.coordinate.longitude{
            newLong = longitude
            print(newLong)
        }
        
       networkService.fetchLocationWithDistanceBy(name: text, latlong: "\(newLat), \(newLong)",
            page: page, size: size, success: { (result) in
            if let result = result {
                self.page = result.page
                self.size = result.pageSize
                self.places.append(contentsOf: result.data)
                
                for i in 0..<result.data.count {
                    print("name: \(result.data[i].name)")
                    print("type: \(result.data[i].type)")
                    print("distance: \(result.data[i].distance)")
                    print("")
                }
                
                if (result.data.count == 0) {
                    self.isLastPage = true
                }
                
                self.tableView?.reloadData()
            }
            if (self.page == 0 && self.places.count > 0) {
                self.tableView?.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
            self.loadingBar?.stopAnimating()
            self.loadingBar?.isHidden = true
        }) { (error) in
            self.loadingBar?.stopAnimating()
            self.loadingBar?.isHidden = true
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SearchController: UITextFieldDelegate {
    
    func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        
        self.keywords = text
        if text.count < 3 {
            return
        }
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.keywords != text {
                return
            }
            self.places.removeAll()
            self.page = 0
            self.isLastPage = false
            self.searchLocations(text: self.keywords)
        }
    }
}

extension SearchController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlaceViewCell") as! SearchPlaceViewCell
        let place = places[indexPath.row]
        
        cell.updateViews(name: place.name, address: place.type, distance: place.distance)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = places[indexPath.row]
        
        selectedLocation = location
        dismiss(animated: false, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (places.count > 0) {
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No result"
            noDataLabel.textColor     = UIColor.color_primary
            noDataLabel.textAlignment = .center
            let font = UIFont(name: "Titillium-Semibold", size: 15)
            if (font != nil) {
                noDataLabel.font = font
            }
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (loadingBar!.isAnimating) {
            return
        }
        let lastElement = places.count - 1
        if (indexPath.row == lastElement && !isLastPage) {
            page += 1
            searchLocations(text: keywords)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}
