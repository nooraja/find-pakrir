//
//  SearchPlaceViewCell.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class SearchPlaceViewCell: UITableViewCell {
    
    @IBOutlet weak var locationName: UILabel?
    @IBOutlet weak var locationAddress: UILabel?
    @IBOutlet weak var locationDistance: UILabel?
    
    func updateViews(name: String, address: String, distance: String) {
        self.locationName?.text = name
        self.locationAddress?.text = address
        self.locationDistance?.text = distance
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
