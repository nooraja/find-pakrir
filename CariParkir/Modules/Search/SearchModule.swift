//
//  SearchModule.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class SearchModule: NSObject {

    func instantiate(_ lat: Double, _ long: Double) -> UIViewController {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let controller = storyboard.instantiateInitialViewController() as! SearchController
        let networkService = NetworkService.sharedInstance
        
        controller.networkService = networkService
        controller.lat = lat
        controller.long = long
        
        return controller
    }
}
