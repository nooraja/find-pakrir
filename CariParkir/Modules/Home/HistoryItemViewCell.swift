//
//  HistoryItemViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class HistoryItemViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var extraUsageLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var contentWrapper: UIView!
    
    override func layoutSubviews() {
        contentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        contentWrapper.layer.shadowOpacity = 1
        contentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentWrapper.layer.shadowRadius = 2
    }
}
