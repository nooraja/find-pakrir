//
//  FacilityCollectionViewCell.swift
//  CariParkir
//
//  Created by Indovti on 9/7/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class FacilityCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
