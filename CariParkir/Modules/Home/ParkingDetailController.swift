//
//  ParkingDetailController.swift
//  CariParkir
//
//  Created by Indovti on 8/26/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import iCarousel
import ImageViewer
import FSPagerView

struct DataItem {
    
    let imageView: UIImageView
    let galleryItem: GalleryItem
}

class ParkingDetailController: UIViewController, UIScrollViewDelegate {
    
    var park: Park = Park()
    var dataItem: DataItem!
    var facilities: [String] = [] {
        didSet {
            self.facilityView?.reloadData()
        }
    }
    
    // scheduleView
    var isHidden = true
    var scheduleViewHeight: CGFloat!
    
    let numFormatter = NumberFormatter()
    
    let dayNames = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
    
    let shortDayNames = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"]
    
    @IBOutlet weak var imageView: CustomImageView?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var availabilityLabel: UILabel?
    @IBOutlet weak var openHourLabel: UILabel?
    @IBOutlet weak var addressLabel: UILabel?
    @IBOutlet weak var facilityView: iCarousel?
    @IBOutlet weak var openDayTableView: UITableView?
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
   
    @IBOutlet weak var distanceFacilityToAddress: NSLayoutConstraint!
    @IBOutlet weak var secondDay: UIView!
    @IBOutlet weak var thirdDay: UIView!
    @IBOutlet weak var fourthDay: UIView!
    @IBOutlet weak var fifthDay: UIView!
    @IBOutlet weak var sixthDay: UIView!
    @IBOutlet weak var seventhDay: UIView!
    
    @IBOutlet weak var scheduleView: UIView!
    
    @IBOutlet weak var firstDayName: UILabel!
    @IBOutlet weak var firstDayTime: UILabel!
    @IBOutlet weak var secondDayName: UILabel!
    @IBOutlet weak var secondDayTime: UILabel!
    @IBOutlet weak var thirdDayName: UILabel!
    @IBOutlet weak var thirdDayTime: UILabel!
    @IBOutlet weak var fourthDayName: UILabel!
    @IBOutlet weak var fourthDayTime: UILabel!
    @IBOutlet weak var fifthDayName: UILabel!
    @IBOutlet weak var fifthDayTime: UILabel!
    @IBOutlet weak var sixthDayName: UILabel!
    @IBOutlet weak var sixthDayTime: UILabel!
    @IBOutlet weak var seventhDayName: UILabel!
    @IBOutlet weak var seventhDayTime: UILabel!
    @IBOutlet weak var imagePreview: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scheduleView.translatesAutoresizingMaskIntoConstraints = true
        
        numFormatter.groupingSeparator = "."
        numFormatter.numberStyle = .decimal
        self.setNavigationBarFont()
        if park.imageUrlList.isEmpty {
            imagePreview?.isHidden = false
        } else {
            imageView?.downloadedFrom(park.imageUrlList[0], contentMode: .scaleAspectFill)
        }
        
        nameLabel?.text = park.name
        
        
        let detailTextAtrr = NSMutableAttributedString()
        
        for vehicle in park.parkingType {
            let price   = numFormatter.string(from: NSNumber(value: vehicle.price))
//            let detailText : String = "\(vehicle.type), kapasitas \(vehicle.capacity), tarif Rp \(price!) /12jam \n"
            let detailText : String = "\(vehicle.type), tarif Rp \(price!) /12jam \n"
            
            
            let tmpDetailTextAttr = NSMutableAttributedString.init(string: detailText)
            var index: Int = 0
            var boldFont: UIFont = UIFont.systemFont(ofSize: 13, weight: UIFontWeightBold)
            if let titiliumBoldFont = UIFont(name: "Titillium-Bold", size: 13) {
                boldFont = titiliumBoldFont
            }
            var semiboldFont: UIFont = UIFont.systemFont(ofSize: 13, weight: UIFontWeightBold)
            if let titiliumSemiBoldFont = UIFont(name: "Titillium-Semibold", size: 15) {
                semiboldFont = titiliumSemiBoldFont
            }
            
            tmpDetailTextAttr.addAttribute(NSFontAttributeName, value: boldFont, range: NSRange(location: index,length: vehicle.type.count))
//            tmpDetailTextAttr.addAttribute(NSFontAttributeName, value: semiboldFont, range: NSRange(location: vehicle.type.count + 11,length: String(vehicle.capacity).count + 1))
//            tmpDetailTextAttr.addAttribute(NSFontAttributeName, value: semiboldFont, range: NSRange(location: vehicle.type.count + 11,length: String(vehicle.capacity).count + 1))
            
//            tmpDetailTextAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.color_primary, range: NSRange(location: index,length: vehicle.type.count + 11 + String(vehicle.capacity).count + 8))
//            index += vehicle.type.count + 11 + String(vehicle.capacity).count + 8
//            index += vehicle.type.count + 11
            index += vehicle.type.count
            tmpDetailTextAttr.addAttribute(NSFontAttributeName, value: semiboldFont, range: NSRange(location: index,length: "\(price!)".count + 12))
            
            detailTextAtrr.append(tmpDetailTextAttr)
        }
        
        availabilityLabel?.attributedText = detailTextAtrr
        
        var result = park.operationalDay
        
        // TODO: here
        if (result.count > 2) {
            openHourLabel?.text = "Setiap Hari, \(park.open) - \(park.close)"
        } else {
            openHourLabel?.text = "\(park.operationalDay.first!), \(park.open) - \(park.close)"
        }
        
        for i in (0..<(result.count-1)) {
            print(result[i])
        }
        
        addressLabel?.text = park.address
        facilities = park.facilitiesCode
        self.openDayTableView?.layer.masksToBounds = true
        self.openDayTableView?.layer.borderColor = UIColor.black.cgColor
        self.openDayTableView?.layer.borderWidth = 1.0
        

        self.button?.isHidden = true // hide unused view
        
        setupMultipleImages() // for multiple image display
        configurePageControl() // page controll for image carousel
        
        self.imageScrollView.delegate = self
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        
        
        self.showHide(false) // initialize show hide schedule
        self.decorateScheduleView() // for decorating schedule view
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(scheduleViewTouch))
        scheduleView.isUserInteractionEnabled = true
        scheduleView.addGestureRecognizer(gestureRecognizer)
        
        tryDate() // for try date in swift
        initSchedule()
    }
    
    func tryDate() {
        let day = Calendar.current.dateComponents([.weekday], from: Date()).weekday
        print("date: \(String(describing: day))")
    }
    
    func decorateScheduleView() {
        self.scheduleView.layer.borderWidth = 1
        self.scheduleView.layer.borderColor = UIColor.black.cgColor
        self.scheduleView.layer.cornerRadius = 25
    }
    
    func initSchedule() {
        let dayOrderInWeek = Calendar.current.dateComponents([.weekday], from: Date()).weekday
        
        let dayNameLabels = [firstDayName, secondDayName, thirdDayName, fourthDayName, fifthDayName, sixthDayName, seventhDayName]
        
        let dayTimeLabels = [firstDayTime, secondDayTime, thirdDayTime, fourthDayTime, fifthDayTime, sixthDayTime, seventhDayTime]
        
        var dayTimesIsOpen = [Bool](repeating: Bool(), count: 7)
        
        print("bool default value: \(dayTimesIsOpen[0])")
        
        for i in 0..<dayNameLabels.count {
            dayNameLabels[i]?.text = dayNames[(i + dayOrderInWeek! - 1) % 7]
            
            dayTimeLabels[i]?.text = "\(park.open)-\(park.close)"
        }
        
        if (park.operationalDay.count != 1) {
            for i in 0..<shortDayNames.count {
                if park.operationalDay.contains(dayNames[i]) {
                    dayTimesIsOpen[i] = true
                }
                print("dayTimeIsOpen: \(dayTimesIsOpen[i])")
            }
            
            for i in 0..<dayTimeLabels.count {
                if dayTimesIsOpen[(i + dayOrderInWeek! - 1) % 7] {
                    dayTimeLabels[i]?.text = "\(park.open)-\(park.close)"
                } else {
                    dayTimeLabels[i]?.text = "Tutup"
                }
            }
        }
    }
    
    
    @objc func scheduleViewTouch(_ sender:UITapGestureRecognizer) {
        self.showHide(self.isHidden)
        self.isHidden = !isHidden
    }
    
    func showHide(_ isHidden: Bool) {
        if isHidden {
            self.distanceFacilityToAddress.constant += 300
            self.scheduleView.frame.size.height = self.scheduleView.frame.size.height * 7
            
            self.secondDay.isHidden = false
            self.thirdDay.isHidden = false
            self.fourthDay.isHidden = false
            self.fifthDay.isHidden = false
            self.sixthDay.isHidden = false
            self.seventhDay.isHidden = false
        } else {
            self.distanceFacilityToAddress.constant -= 300
            self.scheduleView.frame.size.height = self.scheduleView.frame.size.height / 7
            
            self.secondDay.isHidden = true
            self.thirdDay.isHidden = true
            self.fourthDay.isHidden = true
            self.fifthDay.isHidden = true
            self.sixthDay.isHidden = true
            self.seventhDay.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goButtonTapped(_ sender: Any) {
        let coordinate = park.location
        //Working in Swift new versions.
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.open(URL(string:
                "comgooglemaps://?saddr=&daddr=\(coordinate)&directionsmode=driving")! as URL)
        } else
        {
            let string = "http://maps.apple.com/?saddr=Current%20Location&daddr=\(coordinate)"
            print(string)
            UIApplication.shared.open(URL(string: string)!)
            NSLog("Can't use com.google.maps://");
        }
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func imageButtonClicked(_ sender: CustomImageView?) {
        let image: UIImage = sender!.image!
        let galleryItem: GalleryItem = GalleryItem.image { $0(image) }
        self.dataItem = DataItem(imageView: imageView!, galleryItem: galleryItem)
        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.thumbnailsButtonMode(.none),
            GalleryConfigurationItem.overlayColor(UIColor.color_primary),
            ]))
    }
    
    func setupMultipleImages() {
        for i in 0..<park.imageUrlList.count {
            print("\(park.imageUrlList[i])")

            let imageView: CustomImageView = CustomImageView()
            imageView.downloadedFrom(park.imageUrlList[i], contentMode: .scaleAspectFill)

            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            
            let xPosition = imageScrollView.frame.width * CGFloat(i)
            let yPosition = imageScrollView.frame.minY

            imageView.frame =
                CGRect(x: xPosition, y: yPosition, width: self.imageScrollView.frame.width, height: self.imageScrollView.frame.height)

            imageScrollView.contentSize.width = imageScrollView.frame.width * CGFloat(i + 1)
            self.imageScrollView.addSubview(imageView)
        }
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = park.imageUrlList.count
        self.pageControl.currentPage = 0
//        self.pageControl.tintColor = UIColor.red
//        self.pageControl.pageIndicatorTintColor = UIColor.black
//        self.pageControl.currentPageIndicatorTintColor = UIColor.green
        self.pageControl.isEnabled = false
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * imageScrollView.frame.size.width
        imageScrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let imageView = tapGestureRecognizer.view as! UIImageView
        let image = imageView.image
        
        let galleryItem: GalleryItem = GalleryItem.image { $0(image) }
        self.dataItem = DataItem(imageView: imageView, galleryItem: galleryItem)
        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.thumbnailsButtonMode(.none),
            GalleryConfigurationItem.overlayColor(UIColor.color_primary),
            ]))
    }
}

extension ParkingDetailController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return facilities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "facilityCollectionItem", for: indexPath) as! FacilityCollectionViewCell
        
        let facility = facilities[indexPath.row]
        let facilityLabel = self.park.facilities[indexPath.row]
        print(facility)
        var image: UIImage?
        
        switch facility {
        case "M":
            image = #imageLiteral(resourceName: "fac_mesjid")
            break
        case "A":
            image = #imageLiteral(resourceName: "fac_atm")
            break;
        case "T":
            image = #imageLiteral(resourceName: "fac_toilet")
            break
        case "W":
            image = #imageLiteral(resourceName: "fac_wash")
            break
        case "S":
            image = #imageLiteral(resourceName: "fac_service")
            break
        default: image = #imageLiteral(resourceName: "fac_other")
        }
        
        cell.iconView.image = image
        
        cell.nameLabel.text = facilityLabel
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = 60 * facilities.count
        let totalSpacingWidth = 4 * (facilities.count - 1)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
}

extension ParkingDetailController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return 1
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return self.dataItem.galleryItem
    }
}

extension ParkingDetailController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.park.operationalDay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentRow = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "kOpenHoursSelectionCell") as! OpenHoursSelectionItemViewCell
        cell.lblDay.text = self.park.operationalDay[currentRow]
        cell.lblHours.text = "07.00 - 21.00"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let cellHeight:CGFloat = 44
        let y = targetContentOffset.pointee.y + scrollView.contentInset.top + cellHeight / 2
        let cellIndex  = floor(y / cellHeight)
        targetContentOffset.pointee.y = cellIndex * cellHeight - scrollView.contentInset.top;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

