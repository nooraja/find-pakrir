//
//  MenuController.swift
//  CariParkir
//
//  Created by Indovti on 8/18/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class MenuController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var dismissBg: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    let kMenuItemCell = "menuItemCell"
    
    @IBAction func dismissMenu(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        titleView.layer.shadowColor = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0).cgColor
        titleView.layer.shadowOpacity = 1
        titleView.layer.shadowOffset = CGSize(width: 0, height:2)
        titleView.layer.shadowRadius = 2
        
        collectionView.register(UINib(nibName: "MenuItemCell", bundle: Bundle.main), forCellWithReuseIdentifier: kMenuItemCell)
        collectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMenuItemCell, for: indexPath) as! MenuItemCell
        let borderColor: UIColor = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
        
        let rightBorderLayer = CALayer()
        rightBorderLayer.backgroundColor = borderColor.cgColor
        rightBorderLayer.frame = CGRect(x: cell.frame.width - 1, y: 0, width: 1, height: cell.frame.height)
        
        let leftBorderLayer = CALayer()
        leftBorderLayer.backgroundColor = borderColor.cgColor
        leftBorderLayer.frame = CGRect(x: 0, y: 0, width: 1, height: cell.frame.height)
        
        let topBorderLayer = CALayer()
        topBorderLayer.backgroundColor = borderColor.cgColor
        topBorderLayer.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: 1)
        
        let bottomBorderLayer = CALayer()
        bottomBorderLayer.backgroundColor = borderColor.cgColor
        bottomBorderLayer.frame = CGRect(x: 0, y: cell.frame.height - 1, width: cell.frame.width, height: 1)
        
        switch indexPath.row {
        case 0: cell.menuItemLabel.text = "Profile"
        cell.layer.addSublayer(rightBorderLayer)
        cell.layer.addSublayer(bottomBorderLayer)
        case 1: cell.menuItemLabel.text = "History"
        cell.layer.addSublayer(rightBorderLayer)
        cell.layer.addSublayer(bottomBorderLayer)
        case 2: cell.menuItemLabel.text = "News & Promo"
        cell.layer.addSublayer(bottomBorderLayer)
        case 3: cell.menuItemLabel.text = "Settings"
        cell.layer.addSublayer(rightBorderLayer)
        cell.layer.addSublayer(bottomBorderLayer)
        case 4: cell.menuItemLabel.text = "Logout"
        cell.layer.addSublayer(rightBorderLayer)
        cell.layer.addSublayer(bottomBorderLayer)
            
        default: break
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
            case 4: logout()
                
            default: break
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width / 3)
        return CGSize(width: width, height: width)
    }
    
    func logout() {
        let confirmDialog = UIAlertController(title: "Konfirmasi Logout", message: "Apakah Anda yakin ingin logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Kembali", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
        }
        confirmDialog.addAction(cancelAction)
        
        let sureAction = UIAlertAction(title: "Ya", style: UIAlertActionStyle.default) {
            (action) in
            UserDefaults.standard.setIsTermAndCondition(value: false)
            confirmDialog.dismiss(animated: true, completion: nil)
        }
        confirmDialog.addAction(sureAction)
        self.present(confirmDialog, animated: true, completion: nil)
    }
}
