//
//  VehicleItemView.swift
//  CariParkir
//
//  Created by Indovti on 8/24/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class VehicleItemViewCell: UITableViewCell {
    @IBOutlet weak var vehicleIcon: UIImageView!
    @IBOutlet weak var vehicleInfo: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
}
