//
//  HomeController.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import QRCode
import Firebase
import FirebaseAuth

class HomeController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var detailView: UIView?
    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var searchView: UIView?
    @IBOutlet weak var goButton: UIButton?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var vehicleTableView: UITableView?
    @IBOutlet weak var detailViewHeight: NSLayoutConstraint?
    @IBOutlet weak var iconArrowDetail: UIImageView?
    @IBOutlet weak var tabBarView: UITabBar?
    @IBOutlet weak var menuBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleSelectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundButton: UIButton!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var menuTitleView: UIView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    @IBAction func dismissMenu(_ sender: Any) {
        menuBottomConstraint.constant = -1000
        self.goButton?.isHidden = true
        self.detailView?.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.backgroundButton.alpha = 0
        })
    }
    
    
    let locationManager = CLLocationManager()
    var networkService: INetworkService!
    var parks: [Park] = []
    var detailPark: Park?
    let kMenuItemCell = "menuItemCell"
    var vehicleList: [ConsumerVehicle] = []
    var vehicleListExpanded: Bool = false
    var defaultVehicle: Int = -1
    var initialLoc: CLLocation = CLLocation()
    var modalQRCode: CustomModal?
    var modalBooking: CustomModal?
    var flagShow: Bool = false
    var tiketCode: String! = ""
    var flagShowQRCode: Bool = true
    let titles = ["On The Spot", "Booking"]
    let searchController = UISearchController(searchResultsController: nil)
    
    var searchViewBackground: UIView {
        let views = UIView()
        views.backgroundColor = .orange
        return views
    }
    
    lazy var btnScanBarqode: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("SCAN QR CODE TEMPAT PARKIR", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.titleLabel?.font = UIFont(name: "Titillium-Bold", size: 12)
        button.addTarget(self, action: #selector(handleBtnScanBarqode), for: .touchUpInside)
        return button
    }()
    
    lazy var btnTutup: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("TUTUP", for: .normal)
        button.setTitleColor(.toolbar_grey, for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.toolbar_grey.cgColor
        button.titleLabel?.font = UIFont(name: "Titillium-Bold", size: 12)
        button.addTarget(self, action: #selector(handleBtnTutup), for: .touchUpInside)
        return button
    }()
    
    let titleLabel1: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.green
        label.font = UIFont(name: "Titillium-Bold", size: 18)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "SUKSES MELAKUKAN \nON THE SPOT BOOKING!"
        return label
    }()
    
    let titleLabel2: UILabel = {
        var myString: String = "Punya Kode (Promo / Referal / Redeem) ?"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSFontAttributeName:UIFont(name: "Titillium-RegularUpright", size: 16.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: NSRange(location:6, length:31))
        
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = myMutableString
        return label
    }()
    
    let titleLabel3: UILabel = { 
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.font = UIFont(name: "Titillium-Regular", size: 16)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "Gunakan di sini! \nKosongkan jika tidak memiliki kode promo"
        return label
    }()
    
    let titleLabel4: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.orange
        label.font = UIFont(name: "Titillium-Regular", size: 16)
        label.text = "Kode"
        return label
    }()
    
    let textFieldKode: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.layer.backgroundColor = UIColor.white.cgColor
        
        textField.layer.masksToBounds = false
        textField.layer.shadowColor = UIColor.orange.cgColor
        textField.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        textField.layer.shadowRadius = 0.0
        return textField
    }()
    
    lazy var btnOK: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Ya", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnOK), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hideKeyboard()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        let loc = GMSCameraPosition.camera(withLatitude: -6.175168,
                                 longitude: 106.827185,
                                 zoom: 16)
        mapView?.delegate = self
        mapView?.camera = loc
        
        prepareViews()
        detailViewHeight?.constant = 130
        
        drawShadow()
        self.vehicleTableView?.layer.masksToBounds = true
        self.vehicleTableView?.layer.borderColor = UIColor.black.cgColor
        self.vehicleTableView?.layer.borderWidth = 1.0
        
        menuCollectionView.register(UINib(nibName: "MenuItemCell", bundle: Bundle.main), forCellWithReuseIdentifier: kMenuItemCell)
        menuCollectionView.layer.borderColor = UIColor.white.cgColor
//        menuCollectionView.layer.borderColor = UIColor.white.cgColor
//        menuCollectionView.alpha = 0.7
        menuCollectionView.reloadData()
        
        // print("[viewDidLoad] Location Coordinate: \(locationManager.location?.coordinate)")
        
        if UserDefaults.standard.isSosmed() {
            let storyBoard = UIStoryboard(name: "AlertCompleteProfile", bundle: nil)
            let customAlert = storyBoard.instantiateViewController(withIdentifier: "AlertCompleteController") as! AlertCompleteController
            customAlert.content = AlertComplete(title: "Informasi", message: "Mohon Lengkapi profil anda.")
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            self.present(customAlert, animated: true, completion: nil)
        }
        
    }
    
    func drawShadow() {
        menuTitleView.layer.shadowColor = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0).cgColor
        menuTitleView.layer.shadowOpacity = 1
        menuTitleView.layer.shadowOffset = CGSize(width: 0, height:2)
        menuTitleView.layer.shadowRadius = 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
        
        if let selectedLocation = selectedLocation {
            let location = CLLocationCoordinate2D(latitude: selectedLocation.latitude, longitude: selectedLocation.longitude)
            let camera = GMSCameraPosition(target: location, zoom: 17, bearing: 0, viewingAngle: 0)
            mapView?.camera = camera
            mapView?.animate(to: camera)
            mapView?.isMyLocationEnabled = false
            mapView?.settings.myLocationButton = true
            locationManager.stopUpdatingLocation()
            
            fetchParkPositons(latitude: selectedLocation.latitude, longitude: selectedLocation.longitude)
        }
        
//        print("Location name: \(selectedLocation?.name)")
//        print("Location type: \(selectedLocation?.type)")
//        print("Location lat: \(selectedLocation?.latitude)")
//        print("Location long: \(selectedLocation?.longitude)")
//
        self.vehicleList.removeAll()
        if UserDefaults.standard.isLoggedIn() {
            self.vehicleList = Config.instance.loggedInUser!.vehicleList
            var savedDefaultVehicle: ConsumerVehicle? = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id)
            if Config.instance.selectedVehicle != nil {
                savedDefaultVehicle = Config.instance.selectedVehicle
            }
            
            if savedDefaultVehicle != nil && Config.instance.selectedVehicle == nil {
                for (index, vehicle) in vehicleList.enumerated() {
                    if (vehicle.regCode == savedDefaultVehicle?.regCode) {
                        defaultVehicle = index
                        break
                    }
                }
                Config.instance.selectedVehicle = vehicleList[defaultVehicle]
            }
            DispatchQueue.main.async {
                self.vehicleTableView?.reloadData()
                self.vehicleTableView?.layoutIfNeeded()
                self.vehicleTableView?.scrollToRow(at: IndexPath(row: self.defaultVehicle, section: 0), at: .top, animated: false)
            }
        } else {
            vehicleSelectionHeightConstraint.constant = 0
        }
        expandButton.isHidden = vehicleList.count <= 1
        vehicleTableView?.isHidden = vehicleList.count < 1
        
        if flagShow {
            initializationModalBooking()
            if modalBooking != nil {
                modalBooking?.showModal()
                flagShow = false
            }
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        detailPark = nil
        // hidden menu when tap back button
        dismissMenu(backgroundButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = keyboardHeight - textFieldHeight
            modalBooking?.moveY(height: (resultHeight + 20))
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = textFieldHeight - keyboardHeight
            modalBooking?.moveY(height: (resultHeight - 20))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    let markerMe = #imageLiteral(resourceName: "ic_pin_me")
    let markerParkingYellow = #imageLiteral(resourceName: "ic_pinyellow")
    let markerParkingGrey = #imageLiteral(resourceName: "ic_pingrey")
    let marker = GMSMarker()
    func loadMarker(location: CLLocationCoordinate2D) {
        marker.position = location
        marker.icon = markerMe
        marker.map = mapView
        marker.isTappable = false
    }
    
    func loadParkMarker(location: CLLocationCoordinate2D, park: Park) {
        let marker = GMSMarker()
        marker.position = location
        if park.status == "true" {
            marker.icon = markerParkingYellow
        } else {
            marker.icon = markerParkingGrey
        }
        marker.isTappable = true
        marker.map = mapView
        marker.userData = park
    }
    
    func prepareViews() {
        let logo = UIImage(named: "ico_nav_bar")
        let imageView = UIImageView(image:logo)
        
        
        self.navigationItem.titleView = imageView
        
        searchView?.layer.cornerRadius = 10
        searchView?.addLineBorder(edges: [.bottom, .top, .right, .left], colour: UIColor(hex: 0xe2e2e2, alpha: 1), thickness: 1)
        
        let tapSearch = UITapGestureRecognizer(target: self, action: #selector(self.tapSearch))
        searchView?.addGestureRecognizer(tapSearch)
        
        let tapDetailArrow = UITapGestureRecognizer(target: self, action: #selector(HomeController.showDetail))
        iconArrowDetail?.addGestureRecognizer(tapDetailArrow)
        
        let swipeParkDetailUp = UISwipeGestureRecognizer(target: self, action: #selector(HomeController.showDetail))
        swipeParkDetailUp.direction = .up
        tableView?.addGestureRecognizer(swipeParkDetailUp)
        
        goButton?.isHidden = true
        detailView?.isHidden = true
//        detailView?.isHidden = false
    }
    
    func prepareHeightForParkDetail(show: Bool) {
        if show {
            UIView.animate(withDuration: 0.5, animations: { 
                self.detailViewHeight?.constant = 215
                self.tableView?.reloadData()
                self.view.layoutIfNeeded()
            })
            return
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.detailViewHeight?.constant = 130
            self.tableView?.reloadData()
            self.view.layoutIfNeeded()
        })
    }
    
    func tapSearch() {
       // print("[tapSearch] Location Coordinate: \(locationManager.location?.coordinate)")
        
//        let lat = Double((locationManager.location?.coordinate.latitude)!)
//        let long = Double((locationManager.location?.coordinate.longitude)!)
        
        let lat = -6.175168
        let long = 106.827185
        
        //print("[tapSearch] latitude: \(lat), longitude: \(long)")
        
        let controller = SearchModule().instantiate(lat, long)
        self.present(controller, animated: true, completion: nil)
    }
    
    func showDetail() {
        if (detailPark!.status == "true") {
            self.performSegue(withIdentifier: "showParkingDetail", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showParkingDetail") {
            let dest = segue.destination as! ParkingDetailController
            dest.park = detailPark!
        }
    }
    
    func fetchParkPositons(latitude: Double, longitude: Double) {
        networkService.fetchParkingNearby(latLong: "\(latitude),\(longitude)", success: { (result) in
            if let result = result {
                for park in result.data {
                    self.loadParkMarker(location: CLLocationCoordinate2D(latitude: park.latitude, longitude: park.longitude), park: park)
                }
             }
        }) { (error) in
            self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: error!))
        }
    }
    
    func parkDetailSwipe(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .up {
            iconArrowDetail?.image = UIImage(named: "ic-expand-more")
            prepareHeightForParkDetail(show: true)
        }
        
        if gesture.direction == .down {
            iconArrowDetail?.image = UIImage(named: "ic-expand-less")
            prepareHeightForParkDetail(show: false)
        }
    }
    
    
    @IBAction func goButtonTapped(_ sender: Any) {
        guard let coordinate = detailPark?.location else {
            return
        }
        //Working in Swift new versions.
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.open(URL(string:
                "comgooglemaps://?saddr=&daddr=\(coordinate)&directionsmode=driving")! as URL)
        } else
        {
            let appleMapAddr = "http://maps.apple.com/?saddr=Current%20Location&daddr=\(coordinate)"
            print("[OPEN APP] " + appleMapAddr)
            UIApplication.shared.open(URL(string: appleMapAddr)!)
            NSLog("Can't use com.google.maps://");
        }
    }
    
    @IBAction func expandButtonClicked(_ sender: Any) {
        toggleVehicleView()
    }
    
    func toggleVehicleView() {
        if (vehicleList.count == 0) {
            return
        }
        var newHeight = vehicleSelectionHeightConstraint.constant
        if (vehicleListExpanded) {
            newHeight = 44
            expandButton.setImage(#imageLiteral(resourceName: "ic_arrow_top"), for: .normal)
            vehicleTableView?.isScrollEnabled = false
        } else {
            newHeight = CGFloat(vehicleList.count > 3 ? 3 : vehicleList.count) * 44
            expandButton.setImage(#imageLiteral(resourceName: "ic_arrow_bottom"), for: .normal)
            vehicleTableView?.isScrollEnabled = true
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.vehicleSelectionHeightConstraint.constant = newHeight
            self.view.layoutIfNeeded()
        })
        vehicleListExpanded = !vehicleListExpanded
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.onVehicleSelected), userInfo: nil, repeats: false);
    }
    
    func onVehicleSelected() {
        vehicleTableView?.scrollToRow(at: IndexPath(row: defaultVehicle, section: 0), at: .top, animated: false)
        Config.instance.selectedVehicle = vehicleList[defaultVehicle]
    }
    
    func alertHome(title: String, msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
        
    }
    
    func initializationModalQRCode() {
        let vehicle = Config.instance.selectedVehicle!
        let qrCodeString = Config.instance.loggedInUser!.qrCode + "/" + (vehicle.vehicleType == .motor ? "M" : "C") + "/" + vehicle.regCode
        let myQRCode: UIImageView = {
            var qrCode = QRCode(qrCodeString)
            qrCode?.size = CGSize(width: 170, height: 170)
            qrCode?.errorCorrection = .High
            let imageView = UIImageView(qrCode: qrCode!)
            return imageView
        }()
        
        modalQRCode = CustomModal()
        modalQRCode?.modal(self.view, title: "Scan My QR Code!", height: 370)
        modalQRCode?.addContent(view: myQRCode, space: -100, width: 170, height: 170)
        modalQRCode?.addContent(view: btnScanBarqode, space: 30, width: 0, height: 35, referenceBottom: myQRCode)
        modalQRCode?.addBtnBottom(btn1: btnTutup, btn2: nil, height: 33)
    }
    
    func initializationModalBooking() {
        modalBooking = CustomModal()
        modalBooking?.modal(self.view, title: "Konfirmasi Tiket", height: 370)
        modalBooking?.addContent(view: titleLabel1, space: -100, width: 180, height: 40)
        modalBooking?.addContent(view: titleLabel2, space: 10, width: 0, height: 16, referenceBottom: titleLabel1)
        modalBooking?.addContent(view: titleLabel3, space: 0, width: 0, height: 25, referenceBottom: titleLabel2)
        modalBooking?.addContent(view: titleLabel4, space: 20, width: 0, height: 16, referenceBottom: titleLabel3)
        modalBooking?.addContent(view: textFieldKode, space: 10, width: 0, height: 20, referenceBottom: titleLabel4)
        modalBooking?.addBtnBottom(btn1: btnTutup, btn2: btnOK, height: 33)
    }
    
    func handleBtnTutup() {
        if (modalQRCode != nil){
            modalQRCode?.hideModal()
            modalQRCode = nil
        }
        
        if (modalBooking != nil){
            modalBooking?.hideModal()
            modalBooking = nil
        }
    }
    
    func handleBtnOK() {
        if (textFieldKode.text?.isEmpty)! {
            self.alertHome(title: "Oops!", msg: "Mohon di isi")
            return
        }
        
        if (modalBooking != nil){
            modalBooking?.hideModal()
            modalBooking = nil
            
            ProgressView.shared.showProgressView(self.view)
            NetworkService.sharedInstance.claimPromo(redeem: textFieldKode.text!, ticketCode: tiketCode, success: {data in
                ProgressView.shared.hideProgressView()
                if (data?.message == "OK") {
                    self.alertHome(title: "Sukses", msg: data!.result)
                    Config.instance.showNotification(title: "Check In", message: "Selamat! Anda Mendapatkan Promo")
                } else {
                    self.alertHome(title: "Oops!", msg: data!.result)
                }
            }, failure: {error in
                ProgressView.shared.hideProgressView()
                self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: error!))
            })
        }
    }
    
    func handleBtnScanBarqode() {
        NetworkService.sharedInstance.fetchActiveTicket(success: {data in
            if (data?.message == "OK") {
                let vehicle = Config.instance.selectedVehicle!
                for ticket in data!.result {
                    if (vehicle.regCode == ticket.regCode) {
                        self.alertHome(title: "Oops!", msg: "Kendaraan Anda masih dalam proses transaksi")
                        self.flagShowQRCode = true
                        return
                    }
                }
//                if (self.modalQRCode != nil){
//                    self.modalQRCode?.hideModal()
//                }
                self.flagShowQRCode = true
                self.performSegue(withIdentifier: "scanQRcode", sender: self)
            }
        }, failure: {err in
            self.flagShowQRCode = true
            self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: err!))
        })
        
    }
    
    func showLogoutAlert() {
        let storyboard = UIStoryboard(name: "Alerts", bundle: nil)
        let customAlert = storyboard.instantiateViewController(withIdentifier: "MyAlertController") as! MyAlertController
        customAlert.content = MyAlert(title: "Logout", message: "Apakah Anda yakin ingin keluar dari aplikasi?")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
}

// Mark: Tab Bar
extension HomeController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if (item.title == "Menu") {
            menuBottomConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.backgroundButton.alpha = 0.8
            })
        } else if (item.title == "Tickets") {
            if !UserDefaults.standard.isLoggedIn() {
                goToLogin()
                return
            }
            self.performSegue(withIdentifier: "showActiveTicket", sender: self)
        } else if (item.title == "Scan QR Code"){
            if !UserDefaults.standard.isLoggedIn() {
                goToLogin()
                return
            }
//            self.initializationModalQRCode()
//            if (modalQRCode != nil){
//                modalQRCode?.showModal()
//            }
            if self.flagShowQRCode {
                self.flagShowQRCode = false
                handleBtnScanBarqode()
            }
        }
    }
}

// Mark: Menu Collection
extension HomeController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let width = (view.frame.width / 3)
        if UserDefaults.standard.isLoggedIn() {
            menuHeightConstraint.constant = width * 2 + 50
            return 6
        } else {
            menuHeightConstraint.constant = width + 50
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMenuItemCell, for: indexPath) as! MenuItemCell
        
        let borderColor: UIColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        borderColor.withAlphaComponent(0.3)
        
        let rightBorderLayer = CALayer()
        rightBorderLayer.backgroundColor = borderColor.cgColor
        rightBorderLayer.frame = CGRect(x: cell.frame.width - 1, y: 0, width: 1, height: cell.frame.height)
        
        let leftBorderLayer = CALayer()
        leftBorderLayer.backgroundColor = borderColor.cgColor
        leftBorderLayer.frame = CGRect(x: 0, y: 0, width: 1, height: cell.frame.height)
        
        let topBorderLayer = CALayer()
        topBorderLayer.backgroundColor = borderColor.cgColor
        topBorderLayer.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: 1)
        
        let bottomBorderLayer = CALayer()
        bottomBorderLayer.backgroundColor = borderColor.cgColor
        bottomBorderLayer.frame = CGRect(x: 0, y: cell.frame.height - 1, width: cell.frame.width, height: 1)
        
        if UserDefaults.standard.isLoggedIn() {
            switch indexPath.row {
            case 0: cell.menuItemLabel.text = "Profile"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_profile")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            case 1: cell.menuItemLabel.text = "History"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_history")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            case 2: cell.menuItemLabel.text = "Deals"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_promo")
            cell.layer.addSublayer(bottomBorderLayer)
            case 3: cell.menuItemLabel.text = "Help"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_help")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            case 4: cell.menuItemLabel.text = "News"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_news")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            case 5: cell.menuItemLabel.text = "Logout"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_logout")
            cell.layer.addSublayer(bottomBorderLayer)
            default: break
            }
        } else {
            switch indexPath.row {
            case 0: cell.menuItemLabel.text = "Login"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_login")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            case 1: cell.menuItemLabel.text = "Help"
            cell.menuItemImage.image = #imageLiteral(resourceName: "ic_help")
            cell.layer.addSublayer(rightBorderLayer)
            cell.layer.addSublayer(bottomBorderLayer)
            default: break
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserDefaults.standard.isLoggedIn() {
            switch indexPath.row {
            case 0: self.performSegue(withIdentifier: "showProfile", sender: self)
            case 1: self.performSegue(withIdentifier: "showHistory", sender: self)
            case 2: self.performSegue(withIdentifier: "showPromo", sender: self)
            case 3: self.performSegue(withIdentifier: "showHelp", sender: self)
            case 4: self.performSegue(withIdentifier: "showNews", sender: self)
            // case 5: logout()
            case 5: showLogoutAlert()
            default: break
            }
        } else {
            switch indexPath.row {
            case 0: goToLogin()
            case 1: self.performSegue(withIdentifier: "showHelp", sender: self)
            default: break
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width / 3)
        return CGSize(width: width, height: width)
    }
    
    func logout() {
        let confirmDialog = UIAlertController(title: "Konfirmasi Logout", message: "Apakah Anda yakin ingin logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Tidak", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
        }
        confirmDialog.addAction(cancelAction)
        
        let sureAction = UIAlertAction(title: "Ya", style: UIAlertActionStyle.default) {
            (action) in
            
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
            ProgressView.shared.showProgressView(self.view)
            NetworkService.sharedInstance.logout(success: {data in
                ProgressView.shared.hideProgressView()
                if (data?.message == "OK") {
                    
                    UserDefaults.standard.setIsLoggedIn(value: false)
                    UserDefaults.standard.setIsTermAndCondition(value: false)
                    UserDefaults.standard.setIsSosmed(value: false)
                    UserDefaults.standard.setIsEmailVerified(value: false)
                    UserDefaults.standard.setUserName(value: "")
                    UserDefaults.standard.setFullName(value: "")
                    UserDefaults.standard.setToken(value: "")
                    UserDefaults.standard.setRefreshToken(value: "")
                    Config.instance.selectedVehicle = nil
                    Config.instance.loggedInUser = nil
                    self.goToLogin()
                } else {
                    self.alertHome(title: "Oops!", msg: (data?.result)!)
                }
            }, failure: {err in
                ProgressView.shared.hideProgressView()
                self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: err!))
            })
            
        }
        confirmDialog.addAction(sureAction)
        self.present(confirmDialog, animated: true, completion: nil)
    }
}

// Mark: Tableview
extension HomeController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.tableView) {
            return 1
        } else if (tableView == self.vehicleTableView) {
            return vehicleList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentRow = indexPath.row
        if (tableView == self.tableView) {
            if currentRow == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ParkDetailFacilitiesCell") as! ParkDetailFacilitiesCell
                
                if let detailPark = detailPark {
                    cell.updateViews(park: detailPark)
                }
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkDetailHeaderCell") as! ParkDetailHeaderCell
            
            if let detailPark = detailPark {
                cell.updateViews(park: detailPark)
            }
            
            return cell
        } else if (tableView == self.vehicleTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "kVehicleSelectionCell") as! VehicleSelectionItemViewCell
            if (indexPath.row == defaultVehicle) {
                cell.iconBgView.backgroundColor = UIColor.yellow_button
            } else {
                cell.iconBgView.backgroundColor = UIColor.white
            }
            let vehicle: ConsumerVehicle = vehicleList[indexPath.row]
            cell.brand.text = vehicle.brandName + " " + vehicle.brandType
            cell.regNo.text = vehicle.regCode
            if vehicle.vehicleType == .motor {
                cell.icon.image = #imageLiteral(resourceName: "motor_black")
            } else if vehicle.vehicleType == .mobil {
                cell.icon.image = #imageLiteral(resourceName: "car_black")
            }
            
            return cell
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == self.tableView) {
            let currentRow = indexPath.row
            
            if currentRow == 1 {
                return 90
            }
            
            return 130
        } else if (tableView == self.vehicleTableView) {
            return 44
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let cellHeight:CGFloat = 44
        let y = targetContentOffset.pointee.y + scrollView.contentInset.top + cellHeight / 2
        let cellIndex  = floor(y / cellHeight)
        targetContentOffset.pointee.y = cellIndex * cellHeight - scrollView.contentInset.top;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == vehicleTableView) {
            if (vehicleListExpanded) {
                defaultVehicle = indexPath.row
                vehicleTableView?.reloadData()
                toggleVehicleView()
            } else {
                toggleVehicleView()
            }
        }
        
    }
}

// Mark: -CLLocationManagerDelegate
extension HomeController: CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView?.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let camera = GMSCameraPosition(target: location.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
            mapView?.camera = camera
            mapView?.animate(to: camera)
            initialLoc = location
            mapView?.isMyLocationEnabled = false
            mapView?.settings.myLocationButton = true
            loadMarker(location: location.coordinate)
            
            fetchParkPositons(latitude: Double(location.coordinate.latitude), longitude: Double(location.coordinate.longitude))
            locationManager.stopUpdatingLocation()
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        let camera = GMSCameraPosition(target: initialLoc.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
        mapView.camera = camera
        mapView.animate(to: camera)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let park = marker.userData as? Park else {
            return false
        }
        
        mapView.animate(toLocation: marker.position)
        
        // look at here
        selectedLocation?.latitude = marker.position.latitude
        selectedLocation?.longitude = marker.position.longitude
        
        ProgressView.shared.showProgressView(self.view)
        networkService.fetchParkDetail(id: park.id, success: { (parkDetail) in
            ProgressView.shared.hideProgressView()
            if let result = parkDetail?.result {
                self.goButton?.isHidden = false
                self.detailView?.isHidden = false
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                    self.backgroundButton.alpha = 0.8
                })
                
                self.detailPark = result
                self.detailPark?.status = "true"//park.status
                self.tableView?.reloadData()
            } else {
                self.showToast(message: "Maaf, Parkir tidak di temukan")
            }
        }) { (error) in
            ProgressView.shared.hideProgressView()
            self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: error!))
        }
        
        return true
    }
}

extension HomeController: MyAlertControllerDelegate {
    func okButtonTapped() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.logout(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                
                UserDefaults.standard.setIsLoggedIn(value: false)
                UserDefaults.standard.setIsTermAndCondition(value: false)
                UserDefaults.standard.setIsSosmed(value: false)
                UserDefaults.standard.setUserName(value: "")
                UserDefaults.standard.setFullName(value: "")
                UserDefaults.standard.setToken(value: "")
                UserDefaults.standard.setRefreshToken(value: "")
                Config.instance.selectedVehicle = nil
                Config.instance.loggedInUser = nil
                self.goToLogin()
            } else {
                self.alertHome(title: "Oops!", msg: (data?.result)!)
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertHome(title: "Oops!", msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    func cancelButtonTapped() {
        print("Cancel")
    }
}

extension HomeController: AlertCompleteDelegate{
    func okCompleteButtonTapped() {
         self.performSegue(withIdentifier: "showProfile", sender: self)
    }
}


