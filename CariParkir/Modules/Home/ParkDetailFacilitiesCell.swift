//
//  ParkDetailFacilitiesCell.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 7/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import iCarousel

class ParkDetailFacilitiesCell: UITableViewCell, iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var parkCollectionView: UICollectionView?
    @IBOutlet weak var parkAddress: UILabel?
    @IBOutlet weak var parkView: iCarousel?
    
    var facilities: [String] = [] {
        didSet {
            let defaultMargin: CGFloat = 8
            let width: CGFloat = UIScreen.main.bounds.width * 0.63
            let position: CGFloat = (width / 2) - 25 + defaultMargin// ((UIScreen.main.bounds.width - width))
            
            self.parkView?.viewpointOffset = CGSize(width: position, height: 0)
            self.parkView?.reloadData()
        }
    }
    
    func updateViews(park: Park) {
        parkAddress?.text = park.address
        facilities = park.facilitiesCode
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        parkView?.type = .linear
        parkView?.decelerationRate = 0.1
        parkView?.isPagingEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: CAROUSEL
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return facilities.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let facility = facilities[index]
        let width: CGFloat = 40
        let height: CGFloat = 40
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        var image: UIImage?
        
        switch facility {
        case "M":
            image = imageWithImage(image: UIImage(named: "ico-masjid")!, scaledToSize: CGSize(width: width, height: height))
            break
        case "A":
            image = imageWithImage(image: UIImage(named: "ico-atm")!, scaledToSize: CGSize(width: width, height: height))
            break;
        case "T":
            image = imageWithImage(image: UIImage(named: "ico-toilet")!, scaledToSize: CGSize(width: width, height: height))
            break
        case "W":
            image = imageWithImage(image: UIImage(named: "ico-wash")!, scaledToSize: CGSize(width: width, height: height))
            break
        case "S":
            image = imageWithImage(image: UIImage(named: "ico-service-engine")!, scaledToSize: CGSize(width: width, height: height))
            break
        default: break
        }
        
        imageView.image = image
        imageView.contentMode = .center
        
        return imageView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .wrap)
        {
            return 0
        }
        
        return value
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return 50
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        if let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return newImage
        }
        
        return UIImage()
    }

}
