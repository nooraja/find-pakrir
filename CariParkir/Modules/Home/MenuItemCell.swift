//
//  MenuItemCell.swift
//  CariParkir
//
//  Created by Indovti on 8/18/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class MenuItemCell: UICollectionViewCell {
    @IBOutlet weak var menuItemLabel: UILabel!
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var menuView: UIView!
}
