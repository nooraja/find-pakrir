//
//  VehicleFormController.swift
//  CariParkir
//
//  Created by Indovti on 9/12/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import DLRadioButton

class VehicleFormController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    @IBOutlet weak var vehicleScrollView: UIScrollView!
    @IBOutlet weak var vehicleFormView: UIView!
    @IBOutlet weak var vehicleCategory: CustomTextField!
    @IBOutlet weak var vehicleBrand: CustomTextField!
    @IBOutlet weak var vehicleType: CustomTextField!
    @IBOutlet weak var vehicleNo: UITextField!
    @IBOutlet weak var vehicleYear: CustomTextField!
    @IBOutlet weak var defaulVehicleCheck: DLRadioButton!
    @IBOutlet weak var addEditButton: UIButton!
    @IBOutlet weak var cancelEditButton: UIButton!
    
    weak var activeField: UITextField?
    var categoryPickerView: CustomPickerView = CustomPickerView()
    var brandPickerView: CustomPickerView = CustomPickerView()
    var brandTypePickerView: CustomPickerView = CustomPickerView()
    var yearPickerView: CustomPickerView = CustomPickerView()
    var selectedCategoryIndex: Int = 0
    var selectedBrandIndex: Int = 0
    var selectedBrandTypeIndex: Int = 0
    var selectedBrand: String?
    var selectedBrandType: String?
    var categoryList = ["Motor", "Mobil"]
    var brandList = [String]()
    var brandTypeList = [Brand]()
    
    var editMode = false
    var defaultRow = -1
    
    let vehicleValidator = Validator()
    var vehicleToEdit: ConsumerVehicle? = nil
    var vehicleToEditIndex = 0
    
    var currentYear: Int!
    var years = [Int] ()
    var defaultYear: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var years: [Int] = []
        currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
        var oldyear: Int = 1900
        if years.count == 0 {
            
            for _ in oldyear...currentYear {
                years.append(oldyear)
//                print("\(String(describing: years))")
                oldyear += 1
            }
//            print("\(years.index(of: 0))")
        } else {
            for _ in oldyear...currentYear {
                years.append(oldyear)
                //                print("\(String(describing: years))")
                oldyear += 1
            }
        }
        
//        print("\(String(describing: vehicleToEdit!.vehicleYear)) tahun")
//        print("\(String(describing: years[vehicleToEdit!.vehicleYear - 1900])) tahun")
        self.years = years
        
        cancelEditButton.round(corners: [.allCorners], radius: 3, borderColor: UIColor.gray, borderWidth: 1)
        if (vehicleToEdit == nil) {
            editMode = false
            selectedBrand = "Honda"
            selectedBrandType = nil
            selectedCategoryIndex = 0
            selectedBrandIndex = 0
            selectedBrandTypeIndex = 0
            vehicleCategory.text = categoryList[selectedCategoryIndex]
            categoryPickerView.selectRow(selectedCategoryIndex, inComponent: 0, animated: false)
            vehicleType.text = ""
            vehicleBrand.text = ""
            defaultYear = years.index(of: currentYear)!
            yearPickerView.selectRow(defaultYear, inComponent: 0, animated: false)
            vehicleYear.text = "\(defaultYear)"
            vehicleNo.text = ""
            addEditButton.setTitle("ADD", for: .normal)
            cancelEditButton.setTitle("CANCEL", for: .normal)
            defaulVehicleCheck.isSelected = false
            getBrandFromAPI(type: categoryList[selectedCategoryIndex])
        } else {
            addEditButton.setTitle("SAVE", for: .normal)
            cancelEditButton.setTitle("CANCEL", for: .normal)
            editMode = true
            print("\(editMode) is develop")
            selectedBrand = vehicleToEdit!.brandName
            selectedBrandType = vehicleToEdit!.brandType
            selectedCategoryIndex = vehicleToEdit!.vehicleType == .motor ? 0 : 1
            selectedBrandIndex = 0
            selectedBrandTypeIndex = 0
            vehicleCategory.text = categoryList[selectedCategoryIndex]
            categoryPickerView.selectRow(selectedCategoryIndex, inComponent: 0, animated: false)
            vehicleBrand.text = ""
            vehicleType.text = ""
//            defaultYear = years.index(of: vehicleToEdit!.vehicleYear)!
            defaultYear = years[vehicleToEdit!.vehicleYear - 1900]
            print("default year is \(defaultYear)")
            yearPickerView.selectRow(defaultYear, inComponent: 0, animated: false)
            vehicleYear.text = "\(defaultYear)"
            vehicleNo.text = vehicleToEdit!.regCode
            var vehicleRegCode = ""
            let consumer = Config.instance.loggedInUser!.copy() as! Consumer
            if let defaultVehicle = UserDefaults.standard.getDefaultVehicle(consumerId: consumer.id) {
                vehicleRegCode = defaultVehicle.regCode
            }
            for (idx, vehicleItem) in consumer.vehicleList.enumerated() {
                if vehicleItem.id == vehicleToEdit!.id {
                    vehicleToEditIndex = idx
                }
                if vehicleItem.regCode == vehicleRegCode {
                    defaultRow = idx
                }
            }
            defaulVehicleCheck.isSelected = defaultRow == vehicleToEditIndex
            getBrandFromAPI(type: categoryList[selectedCategoryIndex])
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.hideKeyboard()
        addEditButton.titleLabel!.textAlignment = .center
        
        categoryPickerView.delegate = self
        categoryPickerView.dataSource = self
        
        brandPickerView.delegate = self
        brandPickerView.dataSource = self
        
        brandTypePickerView.delegate = self
        brandTypePickerView.dataSource = self
        
        yearPickerView.delegate = self
        yearPickerView.dataSource = self
        
        assign(picker: categoryPickerView, to: vehicleCategory)
        assign(picker: brandPickerView, to: vehicleBrand)
        assign(picker: brandTypePickerView, to: vehicleType)
        assign(picker: yearPickerView, to: vehicleYear)
        
        registerValidation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func registerValidation() {
        vehicleValidator.registerField(vehicleCategory, rules: [RequiredRule(message: "Jenis Kendaraan wajib diisi")])
        vehicleValidator.registerField(vehicleBrand, rules: [RequiredRule(message: "Merk Kendaraan wajib diisi")])
        vehicleValidator.registerField(vehicleType, rules: [RequiredRule(message: "Tipe Kendaraan wajib diisi")])
        vehicleValidator.registerField(vehicleNo, rules: [RequiredRule(message: "Nomor Polisi wajib diisi"), NoPolisiRule()])
//        vehicleValidator.registerField(vehicleYear, rules: [RequiredRule(message: "Tahun Kendaraan wajib diisi"), ExactLengthRule(length: 4, message: "Tahun Kendaraan wajib 4 angka"), TahunKendaraanRule(message: "Tahun Kendaraan tidak boleh melebihi tahun saat ini")])
    }
    
    func validationSuccessful(_ validator: Validator) {
        let vehicle: ConsumerVehicle = ConsumerVehicle()
        vehicle.vehicleType = VehicleType(rawValue: vehicleCategory.text!) ?? .motor
        vehicle.brandName = vehicleBrand.text!
        vehicle.brandType = vehicleType.text!
        vehicle.vehicleId = brandTypeList[selectedBrandTypeIndex].id!
        vehicle.vehicleYear = Int(vehicleYear.text!) ?? currentYear
        vehicle.regCode = vehicleNo.text!
        
        let consumer: Consumer = Config.instance.loggedInUser!.copy() as! Consumer
        if editMode {
            consumer.vehicleList[vehicleToEditIndex] = vehicle
        } else {
            consumer.vehicleList.append(vehicle)
        }
        
        // Check duplicate Reg Code
        var crossReference = [String: [ConsumerVehicle]]()
        for vehicleItem in consumer.vehicleList {
            let key = vehicleItem.regCode
            if crossReference.index(forKey: key) != nil {
                crossReference[key]?.append(vehicleItem)
            } else {
                crossReference[key] = [vehicleItem]
            }
        }
        let duplicates = crossReference
            .filter { $1.count > 1 }
            .sorted { $0.1.count > $1.1.count }
        
        if (duplicates.count > 0) {
            alertError(msg: "Nomor Polisi telah digunakan")
            return
        }
        
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.updateConsumerDetail(consumer: consumer, success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                if (self.defaulVehicleCheck.isSelected) {
                    UserDefaults.standard.setDefaultVehicle(consumerId: consumer.id, vehicle: vehicle)
                    Config.instance.selectedVehicle = nil
                }
//                Config.instance.showNotification(title: "Sukses", message: self.vehicleToEdit == nil ? "Tambah kendaraan berhasil" : "Ubah kendaraan berhasil")
                
                let storyBoard = UIStoryboard(name: "AlertSuccess", bundle: nil)
                let customAlert = storyBoard.instantiateViewController(withIdentifier: "alertsuccess") as! MyAlertSuccessController
                customAlert.content = MyAlertSuccess(title: "Sukses", message: self.vehicleToEdit == nil ? "Tambah kendaraan berhasil" : "Ubah kendaraan berhasil")
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.delegate = self as? MyAlertSuccessDelegate
                self.present(customAlert, animated: true, completion: nil)
            } else {
                let message = data!.errorMessage
                if message.lowercased().contains("version") {
                    self.refetchConsumerProfile {
                        self.validationSuccessful(validator)
                    }
                } else {
                    self.alertError(msg: data!.errorMessage)
                }
            }
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    func validationFailed(_ validator: Validator, _ errors:[(Validatable ,ValidationError)]) {
        if (errors.count > 0) {
            alertError(msg: errors[0].1.errorMessage)
        }
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.vehicleScrollView.contentInset = contentInsets
            self.vehicleScrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                self.vehicleScrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.vehicleScrollView.contentInset = contentInsets
        self.vehicleScrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField) {
        self.activeField = nil
    }
    
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        self.activeField = sender
    }
    
    @IBAction func vehicleNoChanged(_ sender: UITextField) {
        sender.text = sender.text?.uppercased()
    }
    
    @IBAction func cancelDidTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func alertError(msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: "Oops!", message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    let limitLength = 4
    var flagNumber = false
    var flagText = false
    
    func isNumber(s : String) -> Bool {
        let numberCharacters = CharacterSet.decimalDigits.inverted
        return !s.isEmpty && s.rangeOfCharacter(from:numberCharacters) == nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidChars = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").inverted
        let newString = string.trimmingCharacters(in: invalidChars)
        
        if string.count != newString.count {
            return false
        }
        
//        if textField == vehicleYear {
//            guard let text = textField.text else { return true }
//            let newLength = text.characters.count + string.characters.count - range.length
//            
//            return newLength <= limitLength
//        }
        
        if textField == vehicleNo {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (textField.text?.isEmpty)! {
                flagNumber = false
                flagText = false
            }
            
            if (isBackSpace != -92) {
                if (isNumber(s: string)) {
                    if (flagNumber) {
                        textField.text?.insert(" ", at: (textField.text?.endIndex)!)
                        flagNumber = false
                        flagText = true
                    }else{
                        flagText = true
                    }
                }else {
                    if (flagText) {
                        textField.text?.insert(" ", at: (textField.text?.endIndex)!)
                        flagText = false
                        flagNumber = true
                    }else{
                        flagNumber = true
                    }
                }
            }else {
                print((textField.text?.last)!)
            }
            return true
        }
        return true
    }
    
    func refetchConsumerProfile(completed: @escaping () -> Void) {
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.fetchConsumerDetail(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                completed()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {error in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
        })
    }
}

extension VehicleFormController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case categoryPickerView:
            return categoryList.count
        case brandPickerView:
            return brandList.count
        case brandTypePickerView:
            return brandTypeList.count
        case yearPickerView:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView {
        case categoryPickerView:
            return categoryPickerView.getFontAttribute(categoryList[row])
        case brandPickerView:
            return brandPickerView.getFontAttribute(brandList[row])
        case brandTypePickerView:
            return brandTypePickerView.getFontAttribute(brandTypeList[row].brandType!)
        case yearPickerView:
            return yearPickerView.getFontAttribute("\(years[row])")
        default:
            return nil
        }
    }
    
    @IBAction func vehicleSubmitButtonClicked(_ sender: UIButton) {
        vehicleValidator.validateAll(self)
    }
    
    
    
    func assign(picker: UIPickerView, to txtPickerTextField: UITextField){
        
        let pickerView = picker
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true
        
        let toolBar = UIToolbar()
        toolBar.barTintColor = UIColor.yellow_button
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Pilih", style: .done, target: self, action: #selector(self.pickerDoneButtonClicked))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Batal", style: .plain, target: self, action: #selector(self.pickerCancelButtonClicked))
        
        switch pickerView {
        case categoryPickerView:
            doneButton.tag = 0
            cancelButton.tag = 0
        case brandPickerView:
            doneButton.tag = 1
            cancelButton.tag = 1
        case brandTypePickerView:
            doneButton.tag = 2
            cancelButton.tag = 2
        case yearPickerView :
            doneButton.tag = 3
            cancelButton.tag = 3
            defaultYear = years.index(of: currentYear)!
            picker.selectRow(defaultYear, inComponent: 0, animated: true)
            txtPickerTextField.text = "\(years[defaultYear])"
        default:
            return
        }
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtPickerTextField.inputView = pickerView
        txtPickerTextField.inputAccessoryView = toolBar
    }
    
    func pickerDoneButtonClicked(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 0:
            let oldCategory = selectedCategoryIndex
            selectedCategoryIndex = categoryPickerView.selectedRow(inComponent: 0)
            if oldCategory == selectedCategoryIndex {
                break
            }
            selectedBrandIndex = 0
            selectedBrandTypeIndex = 0
            selectedBrand = selectedCategoryIndex == 1 ? "BMW" : "Honda"
            selectedBrandType = nil
            vehicleCategory.text = categoryList[selectedCategoryIndex]
            getBrandFromAPI(type: categoryList[selectedCategoryIndex])
        case 1:
            let oldBrand = selectedBrandIndex
            selectedBrandIndex = brandPickerView.selectedRow(inComponent: 0)
            if oldBrand == selectedBrandIndex {
                break
            }
            selectedBrandTypeIndex = 0
            selectedBrandType = nil
            vehicleBrand.text = brandList[selectedBrandIndex]
            getBrandTypeFromAPI(type: categoryList[selectedCategoryIndex], brand: brandList[selectedBrandIndex])
        case 2:
            selectedBrandTypeIndex = brandTypePickerView.selectedRow(inComponent: 0)
            vehicleType.text = brandTypeList[selectedBrandTypeIndex].brandType
        case 3:
            defaultYear = yearPickerView.selectedRow(inComponent: 0)
            vehicleYear.text = "\(years[defaultYear])"
        default:
            break
        }
        
        self.view.endEditing(true)
    }
    
    func pickerCancelButtonClicked(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 0:
            categoryPickerView.selectRow(selectedCategoryIndex, inComponent: 0, animated: false)
        case 1:
            brandPickerView.selectRow(selectedBrandIndex, inComponent: 0, animated: false)
        case 2:
            brandTypePickerView.selectRow(selectedBrandTypeIndex, inComponent: 0, animated: false)
        case 3:
            yearPickerView.selectRow(defaultYear, inComponent: 0, animated: false)
        default:
            return
        }
        
        self.view.endEditing(true)
    }
    
    func getBrandFromAPI(type: String) {
        vehicleBrand.text = ""
        vehicleType.text = ""
        let typeCode = type == "Mobil" ? "C" : "M"
        
        print(self.brandList)
        self.brandList.removeAll()
        self.brandPickerView.reloadAllComponents()
        self.brandList.sort()
        NetworkService.sharedInstance.fetchVehicles(type: typeCode, success: { (data) in
            if(data?.message == "OK"){
                self.brandList = data!.result as! [String]
                
                if(self.brandList.count > 0 ){
                    if let selectedBrand = self.selectedBrand {
                        for (idx,brand) in self.brandList.enumerated() {
                            if (brand == selectedBrand) {
                                self.selectedBrandIndex = idx
                                break
                            }
                        }
                    }
                    self.brandPickerView.reloadAllComponents()
                    self.brandList.sort()
                    self.vehicleBrand.text = self.brandList[self.selectedBrandIndex]
                    print(self.vehicleBrand.text)
                    self.brandPickerView.selectRow(self.selectedBrandIndex, inComponent: 0, animated: true)
                    self.brandPickerView.isUserInteractionEnabled = true
                    self.getBrandTypeFromAPI(type: self.vehicleCategory.text!, brand: self.vehicleBrand.text!)
                } else {
                    self.vehicleBrand.text = ""
                }
            }else{
                print((data?.errorMessage)!)
            }
            
        }) { (error) in
            print(Config.instance.getErrorMessage(error: error!))
        }
    }
    
    func getBrandTypeFromAPI(type: String, brand: String) {
        let typeCode = type == "Mobil" ? "C" : "M"
        self.brandTypeList.removeAll()
        self.brandTypePickerView.reloadAllComponents()
        NetworkService.sharedInstance.fetchBrands(type: typeCode, brand: brand, success: { (data) in
            if(data?.message == "OK"){
                for node in data!.result! {
                    if(node.vehicleType == type && node.brandName == brand){
                        self.brandTypeList.append(node)
                    }
                }
                if let selectedBrandType = self.selectedBrandType {
                    for (idx,brandType) in self.brandTypeList.enumerated() {
                        if (brandType.brandType == selectedBrandType) {
                            self.selectedBrandTypeIndex = idx
                            break
                        }
                    }
                }
                self.brandTypePickerView.reloadAllComponents()
                if(self.brandTypeList.count > 0){
                    self.vehicleType.text = self.brandTypeList[self.selectedBrandTypeIndex].brandType
                    self.brandTypePickerView.selectRow(self.selectedBrandTypeIndex, inComponent: 0, animated: true)
                    self.vehicleType.isUserInteractionEnabled = true
                } else {
                    self.vehicleType.text = ""
                }
                
            }else{
                print((data?.errorMessage)!)
            }
            
        }) { (error) in
            print(Config.instance.getErrorMessage(error: error!))
        }
    }
}

