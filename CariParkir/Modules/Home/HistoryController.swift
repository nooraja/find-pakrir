//
//  HistoryController.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class HistoryController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let kCellIdentifier: String = "kHistoryItemCell"
    var parkingHistoryList:[ParkingHistory] = []
    var usedPromoList:[Redeem] = []
    
    var page = 0
    var isLastPage = false
    let numFormatter = NumberFormatter()
    
    @IBOutlet weak var historyPageLeadingContraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        tableView.register(UINib(nibName: "HistoryItemViewCell", bundle: Bundle.main), forCellReuseIdentifier: kCellIdentifier)
        numFormatter.groupingSeparator = "."
        numFormatter.numberStyle = .decimal
        self.getDataFromAPI()
    }
    
    func getDataFromAPI() {
        ProgressView.shared.showProgressView(self.view)
        parkingHistoryList.removeAll()
        getParkingHistoryFromAPI()
    }
    
    func getParkingHistoryFromAPI() {
        NetworkService.sharedInstance.fetchParkingHistory(page: page, success: {data in
            if (data?.message == "OK") {
                for parkingHistory in data!.result {
                    if (parkingHistory.checkOut > 0) {
                        self.parkingHistoryList.append(parkingHistory)
                    }
                }
                print("data!.result.count: \(data!.result.count)")
                if data!.result.count == 0 {
                    self.isLastPage = true
                }
                self.tableView.reloadData()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
            ProgressView.shared.hideProgressView()
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension HistoryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.tableView) {
            return parkingHistoryList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == self.tableView) {
            return getHistoryCell(index: indexPath.row)
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (ProgressView.shared.isProgressViewAnimating()) {
            return
        }
        if (tableView == self.tableView) {
            let lastElement = parkingHistoryList.count - 1
            if (indexPath.row == lastElement && !isLastPage) {
                page = page + 1
                ProgressView.shared.showProgressView(self.view)
                getParkingHistoryFromAPI()
            }
        }
    }
    
    func getHistoryCell(index: Int) -> HistoryItemViewCell {
        let parkingHistory: ParkingHistory = parkingHistoryList[index]
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) as! HistoryItemViewCell
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy - HH:mm"
        cell.dateLabel.text = formatter.string(from: Date(milliseconds: parkingHistory.checkIn))
        let vehicleDetail = "\(parkingHistory.vehicleType == "C" ? "Mobil" : "Motor")\n\(parkingHistory.brandName) - \(parkingHistory.brandType) - \(parkingHistory.regCode)"
        cell.userLabel.text = vehicleDetail
        if (parkingHistory.promoCode != "") {
            cell.promoLabel.text = parkingHistory.promoDesc
        } else {
            cell.promoLabel.text = "Tidak ada Promo"
        }
        cell.pointLabel.text = "\(parkingHistory.point)"
        var facilityText:[String] = []
        for facility in parkingHistory.facilities.components(separatedBy: ",") {
            switch (facility) {
            case "T": facilityText.append("+ Toilet")
            case "M": facilityText.append("+ Mesjid")
            case "A": facilityText.append("+ ATM")
            case "W": facilityText.append("+ Wash")
            case "S": facilityText.append("+ Service")
            default: break
            }
        }
        cell.extraUsageLabel.text = "" //facilityText.joined(separator: ", ")
        if (parkingHistory.checkOut > 0) {
            cell.statusLabel.text = "Selesai"
        } else {
            cell.statusLabel.text = ""
        }
        
        let checkOutTime: Int = parkingHistory.checkOut > 0 ? parkingHistory.checkOut : Date().millisecondsSince1970
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: (checkOutTime - parkingHistory.checkIn)/1000)
        cell.lengthLabel.text = String(format: "%02d:%02d:%02d", h, m, s)
        cell.costLabel.text = "Rp \(numFormatter.string(from: NSNumber(value: parkingHistory.total))!)"
        return cell
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == self.tableView) {
            if (parkingHistoryList.count > 0) {
                tableView.backgroundView = nil
            } else {
                let bg = generateTableBackground(tableView: tableView, title: "Oops!", text: "Anda Belum Pernah Melakukan Transaksi", image: #imageLiteral(resourceName: "no_ticket"), proporsion: 0.38, tint: UIColor.white)
                tableView.backgroundView = bg
            }
        }
        return 1
    }
}
