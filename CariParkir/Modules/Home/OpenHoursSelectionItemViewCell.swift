//
//  OpenHoursSelectionItemViewCell.swift
//  CariParkir
//
//  Created by vti on 11/14/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class OpenHoursSelectionItemViewCell: UITableViewCell {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblHours: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
