//
//  NewsPromoController.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class NewsPromoController: UIViewController {
    let kPromoPointCell: String = "kPromoPointCell"
    let kPromoRedeemCell: String = "kPromoRedeemCell"
    let kPromoCellIdentifier: String = "kUsedPromoItemCell"
    var promoList: [NewsPromo] = []
    var usedPromoList:[Redeem] = []
    var point: Int = 0
    var tags: Int = 0
    
    @IBOutlet weak var promoTableView: UITableView!
    @IBOutlet weak var usedPromoTableView: UITableView!
    @IBOutlet weak var menuSegment: CustomSegmentedControl!
    
    @IBOutlet weak var promoLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var usedPromoTrailingConstraint: NSLayoutConstraint!
    
    var isLastPage = [false, false]
    var page = [0,0]
    
    @IBAction func segmentControlValueChanged(_ sender: CustomSegmentedControl) {
        sender.changeSelectedIndex(to: sender.selectedSegmentIndex)
    }
    
    func segmentChanged(sender: AnyObject) {
        if (menuSegment.selectedSegmentIndex == 0) {
            promoLeadingConstraint.constant = -16
            usedPromoTrailingConstraint.constant = -1000
        } else if (menuSegment.selectedSegmentIndex == 1) {
            promoLeadingConstraint.constant = -1000
            usedPromoTrailingConstraint.constant = -16
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        usedPromoTableView.register(UINib(nibName: "UsedPromoItemViewCell", bundle: Bundle.main), forCellReuseIdentifier: kPromoCellIdentifier)
        
//        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        menuSegment.addTarget(self, action: #selector(self.segmentChanged), for: .valueChanged)
//        menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)

        
        getDataFromAPI()
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if self.menuSegment.selectedSegmentIndex == 1 {
                    self.menuSegment.selectedSegmentIndex = 0
                    self.promoLeadingConstraint.constant = -16
                    self.usedPromoTrailingConstraint.constant = -1000
                    self.menuSegment.changeSelectedIndex(to: self.menuSegment.selectedSegmentIndex)
                }
            case UISwipeGestureRecognizerDirection.left:
                if self.menuSegment.selectedSegmentIndex == 0 {
                    self.menuSegment.selectedSegmentIndex = 1
                    self.promoLeadingConstraint.constant = -1000
                    self.usedPromoTrailingConstraint.constant = -16
                    self.menuSegment.changeSelectedIndex(to: self.menuSegment.selectedSegmentIndex)
                }
            default:
                break
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    var apiStatus = [false, false, false]
    func getDataFromAPI() {
        apiStatus = [false, false, false]
        ProgressView.shared.showProgressView(self.view)
        getPointFromAPI()
        promoList.removeAll()
        getPromoFromAPI()
        usedPromoList.removeAll()
        getUsedPromoFromAPI()
    }
    
    func getPointFromAPI() {
        NetworkService.sharedInstance.fetchRedeemPoint(success: {data in
            if (data?.message == "OK") {
                self.point = data!.result
                self.promoTableView.reloadData()
                self.apiStatus[0] = true
                if (self.apiStatus.filter({$0 == true}).count == self.apiStatus.count) {
                    ProgressView.shared.hideProgressView()
                }
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
                ProgressView.shared.hideProgressView()
            }
        }, failure: {error in
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    func getPromoFromAPI() {
        NetworkService.sharedInstance.fetchPromo(page: page[0], success: {data in
            if (data?.message == "OK") {
                self.promoList.append(contentsOf: data!.result)
                if data!.result.count == 0 {
                    self.isLastPage[0] = true
                }
                self.promoTableView.reloadData()
                self.apiStatus[1] = true
                if (self.apiStatus.filter({$0 == true}).count == self.apiStatus.count) {
                    ProgressView.shared.hideProgressView()
                }
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
                ProgressView.shared.hideProgressView()
            }
        }, failure: {error in
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    func getUsedPromoFromAPI() {
        NetworkService.sharedInstance.fetchUsedRedeem(page: page[1], success: {data in
            print(data!.toJSONString(prettyPrint: true)!)
            if (data?.message == "OK") {
                self.usedPromoList.append(contentsOf: data!.result)
                if data!.result.count == 0 {
                    self.isLastPage[1] = true
                }
                self.usedPromoTableView.reloadData()
                self.apiStatus[2] = true
                if (self.apiStatus.filter({$0 == true}).count == self.apiStatus.count) {
                    ProgressView.shared.hideProgressView()
                }
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
                ProgressView.shared.hideProgressView()
            }
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    @IBAction func redeemButtonClicked(_ sender: UIButton) {
        tags = sender.tag
        
        let storyBoard = UIStoryboard(name: "AlertCustom", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertCustomViewController") as! MyAlertCustomViewController
        customAlert.content = MyAlertCustom(title: "Konfirmasi Penukaran Promo", message: "Apakah Anda yakin ingin mengambil promo ini?", yes: "Ya", no: "Cancel")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertCustomControllerDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func redeem(promo: NewsPromo) {
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.redeem(promo: promo, success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                self.showShowSuccessAlert()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {err in
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
            ProgressView.shared.hideProgressView()
        })
    }
    

    
    func showShowSuccessAlert() {
        let confirmDialog = UIAlertController(title: "Konfirmasi Penggunaan Promo", message: "Sukses!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ya", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
            ProgressView.shared.showProgressView(self.view)
            self.page[1] = 0
            self.isLastPage[1] = false
            self.apiStatus[2] = false
            self.usedPromoList.removeAll()
            self.usedPromoTableView.reloadData()
            self.menuSegment.selectedSegmentIndex = 1
            self.promoLeadingConstraint.constant = -1000
            self.usedPromoTrailingConstraint.constant = -16
            self.menuSegment.changeSelectedIndex(to: self.menuSegment.selectedSegmentIndex)
            self.getUsedPromoFromAPI()
        }
        confirmDialog.addAction(cancelAction)
        
        self.present(confirmDialog, animated: true, completion: nil)
    }
    
    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NewsPromoController: UITableViewDelegate, UITableViewDataSource, MyAlertCustomControllerDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == self.promoTableView) {
            self.setPromoTableBackground()
        } else if (tableView == self.usedPromoTableView) {
            self.setUsedPromoTableBackground()
        }
        return 1
    }
    
    func setPromoTableBackground() {
        if (promoList.count > 1) {
            promoTableView.backgroundView = nil
            
        } else {
            let bg = generateTableBackground(tableView: promoTableView, title: "Oops!", text: "Tidak ada Promo untuk saat ini", image: UIImage(), proporsion: 0.38, tint: UIColor.white)
            promoTableView.backgroundView = bg
        }
    }
    
    func setUsedPromoTableBackground() {
        if (usedPromoList.count > 0) {
            usedPromoTableView.backgroundView = nil
        } else {
            let bg = generateTableBackground(tableView: usedPromoTableView, title: "Oops!", text: "Tidak ada Claimed Promo untuk saat ini", image: #imageLiteral(resourceName: "no_claimed"), proporsion: 0.38, tint: UIColor.white)
            usedPromoTableView.backgroundView = bg
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if (tableView == promoTableView) {
            if (indexPath.row == 0) {
                let cellPoint = tableView.dequeueReusableCell(withIdentifier: kPromoPointCell) as! PromoPointViewCell
                cellPoint.pointLabel.text = "\(self.point) Poin"
                
                return cellPoint
            } else {
                let promo: NewsPromo = promoList[indexPath.row - 1]
                let cellPromo = tableView.dequeueReusableCell(withIdentifier: kPromoRedeemCell) as! PromoViewCell
                cellPromo.redeemButton.tag = indexPath.row - 1
                cellPromo.promoDesc.text = promo.title
                cellPromo.promoPoint.text = "\(promo.point) Points"
                cellPromo.promoImage.downloadedFrom(promo.image, contentMode: .scaleAspectFill)
                return cellPromo
            }
        } else if (tableView == usedPromoTableView) {
            return getUsedPromoCell(index: indexPath.row)
        }
        return cell
    }
    
    
    
    func getUsedPromoCell(index: Int) -> UsedPromoItemViewCell {
        let redeem: Redeem = usedPromoList[index]
        var cell = usedPromoTableView.dequeueReusableCell(withIdentifier: kPromoCellIdentifier) as! UsedPromoItemViewCell
        cell.usedPromoImage.downloadedFrom(redeem.promo_image, contentMode: .scaleAspectFill)
        cell.promoLabel.text = redeem.title
        cell.pointLabel.text = "\(redeem.point) Points"
        cell.usedButton.tag = index - 1
        cell.usedButton.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        if redeem.usedDate != nil {
            markCellAsUsed(cell: &cell)
        } else {
            markCellAsUnused(cell: &cell)
        }
        
        return cell
    }
    
    func markCellAsUsed( cell: inout UsedPromoItemViewCell) {
        cell.contentWrapper.backgroundColor = UIColor.search_title_gray
        cell.promoLabel.textColor = UIColor.color_primary_dark
    }
    
    func markCellAsUnused( cell: inout UsedPromoItemViewCell) {
        cell.contentWrapper.backgroundColor = UIColor.white
        cell.promoLabel.textColor = UIColor.orange
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == promoTableView) {
            return promoList.count + 1
        } else if (tableView == self.usedPromoTableView) {
            return usedPromoList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: Float = 60
        if (tableView == promoTableView) {
            height = 223
        }
        if (tableView == usedPromoTableView) {
            height = 223
        }
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (ProgressView.shared.isProgressViewAnimating()) {
            return
        }
        if (tableView == promoTableView) {
            let lastElement = promoList.count - 1
            if (indexPath.row == lastElement && !isLastPage[0]) {
                ProgressView.shared.showProgressView(self.view)
                page[0] = page[0] + 1
                apiStatus[1] = false
                getPromoFromAPI()
            }
        } else if (tableView == self.usedPromoTableView) {
            let lastElement = usedPromoList.count - 1
            if (indexPath.row == lastElement && !isLastPage[1]) {
                page[1] = page[1] + 1
                apiStatus[2] = false
                ProgressView.shared.showProgressView(self.view)
                getUsedPromoFromAPI()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView.indexPathForSelectedRow == promoTableView.indexPathForSelectedRow) {
            if (usedPromoList[indexPath.row].usedDate == nil) {
                let news = promoList[indexPath.row - 1]
                self.performSegue(withIdentifier: "showDetailPromo", sender: news)
            }
        }
        if (tableView.indexPathForSelectedRow == usedPromoTableView.indexPathForSelectedRow) {
            if (usedPromoList[indexPath.row].usedDate == nil) {
                let ticketController = UIStoryboard(name: "Ticket", bundle: nil).instantiateViewController(withIdentifier: "TicketController") as! TicketController
                ticketController.redeem = usedPromoList[indexPath.row]
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailPromo" {
            let vc = segue.destination as! NewsPromoDetailController
            let newsPromo = sender as! NewsPromo
            
            vc.newsPromo = newsPromo
        }
    }
    
    func okButtonTapped() {
        let selectedPromo = self.promoList[tags]
        self.redeem(promo: selectedPromo)
    }
    
    func cancelButtonTapped() {
    }
    
    @objc func connected(sender: UIButton){
        
        let storyBoard = UIStoryboard(name: "AlertCustom", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertCustomViewController") as! MyAlertCustomViewController
        customAlert.content = MyAlertCustom(title: "Konfirmasi Penggunaan Promo", message: "Apakah Anda ingin menggunakan promo ini?", yes: "Ya", no: "Cancel")
        print("button telah diklik")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        customAlert.delegate = self
        let ticketController = UIStoryboard(name: "Ticket", bundle: nil).instantiateViewController(withIdentifier: "TicketController") as! TicketController
        ticketController.redeem = usedPromoList[tags]
        
        self.navigationController?.pushViewController(ticketController, animated: true)
        
        
    }
}
