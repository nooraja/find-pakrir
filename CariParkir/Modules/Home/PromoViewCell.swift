//
//  PromoViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class PromoViewCell: UITableViewCell {
    @IBOutlet weak var contentWrapper: UIView!
    @IBOutlet weak var promoDesc: UILabel!
    @IBOutlet weak var promoPoint: UILabel!
    @IBOutlet weak var promoImage: CustomImageView!
    @IBOutlet weak var redeemButton: UIButton!
    
    override func layoutSubviews() {
        contentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        contentWrapper.layer.shadowOpacity = 1
        contentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentWrapper.layer.shadowRadius = 2
    }
}
