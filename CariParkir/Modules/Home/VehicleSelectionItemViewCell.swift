//
//  VehicleSelectionItemViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/25/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class VehicleSelectionItemViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var iconBgView: UIView!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var regNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
