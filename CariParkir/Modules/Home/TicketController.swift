//
//  TicketController.swift
//  CariParkir
//
//  Created by Indovti on 8/30/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class TicketController: UIViewController {
    @IBOutlet weak var ticketTableView: UITableView!
    @IBOutlet weak var menuSegment: CustomSegmentedControl!
    
    @IBOutlet weak var bookingTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ticketLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var noTicketBg: UIView!
    var confirmationModal: CustomModal?
    var successModal: CustomModal?
    let numFormatter = NumberFormatter()
    
    @IBAction func segmentControlValueChanged(_ sender: CustomSegmentedControl) {
        sender.changeSelectedIndex(to: sender.selectedSegmentIndex)
    }
    
    var redeem: Redeem? = nil
    var selectedTicket: ParkingHistory? = nil
    var parkingHistoryList:[ParkingHistory] = []
    
    func segmentChanged(sender: AnyObject) {
        if (menuSegment.selectedSegmentIndex == 0) {
            ticketLeadingConstraint.constant = -16
            bookingTrailingConstraint.constant = -1000
        } else if (menuSegment.selectedSegmentIndex == 1) {
            ticketLeadingConstraint.constant = -1000
            bookingTrailingConstraint.constant = -16
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
//        numFormatter.groupingSeparator = "."
//        numFormatter.numberStyle = .decimal
//        menuSegment.addTarget(self, action: #selector(self.segmentChanged), for: .valueChanged)
//        menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)
//        ProgressView.shared.showProgressView(self.view)
//        parkingHistoryList.removeAll()
//        self.getDataFromAPI()
        
//        if (redeem != nil) {
//            self.navigationItem.title = "Select Ticket"
//            self.initConfirmationModal()
//            self.initSuccessModal()
//        }
        
        print("view did load called")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        
        numFormatter.groupingSeparator = "."
        numFormatter.numberStyle = .decimal
        menuSegment.addTarget(self, action: #selector(self.segmentChanged), for: .valueChanged)
        menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)
        ProgressView.shared.showProgressView(self.view)
        parkingHistoryList.removeAll()
        self.getDataFromAPI()
        
        if (redeem != nil) {
            self.navigationItem.title = "Select Ticket"
            self.initConfirmationModal()
            self.initSuccessModal()
        }
    }
    
    func getDataFromAPI() {
        NetworkService.sharedInstance.fetchActiveTicket(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                for parkingHistory in data!.result {
                    if (parkingHistory.checkOut == 0) {
                        self.parkingHistoryList.append(parkingHistory)
                    }
                }
                self.noTicketBg.isHidden = self.parkingHistoryList.count > 0
                self.ticketTableView.reloadData()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetail") {
            let destVC = segue.destination as! TicketDetailController
            destVC.selectedTicket = sender as! ParkingHistory
        }
    }
    
    let buttonFont = UIFont(name: "Titillium-Semibold", size: 15)
    
    let successImageView: UIImageView = {
        let image = UIImage(named: "ic_success")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    let redeemCodeTextField: UITextField = {
        let textField = UITextField()
        textField.layer.cornerRadius = 2
        textField.layer.masksToBounds = true
        textField.layer.borderColor = UIColor.soft_grey.cgColor
        textField.layer.borderWidth = 1
        textField.textColor = UIColor.edit_text_gray
        textField.textAlignment = .center
        textField.isEnabled = false
        let font = UIFont(name: "Titillium-Semibold", size: 15)
        if font != nil {
            textField.font = font
        }
        
        return textField
    }()
    
    let confirmationLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.edit_text_gray
        label.font = label.font.withSize(15)
        let buttonFont = UIFont(name: "Titillium-Regular", size: 15)
        if (buttonFont != nil) {
            label.font = buttonFont
        }
        label.text = "Apakah Anda yakin ingin menggunakan promo ini?"
        label.numberOfLines = 2
        return label
    }()
    
    let successLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.edit_text_gray
        label.font = label.font.withSize(12)
        let buttonFont = UIFont(name: "Titillium-Regular", size: 15)
        if (buttonFont != nil) {
            label.font = buttonFont
        }
        label.text = "Sukses!"
        return label
    }()
    
    lazy var btnOK: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Ya", for: .normal)
        button.setTitleColor(.white, for: .normal)
        if (self.buttonFont != nil) {
            button.titleLabel?.font = self.buttonFont
        }
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(onPopupOKClicked), for: .touchUpInside)
        return button
    }()
    
    lazy var btnCancel: UIButton = {
        let button = CustomButton(type: .system)
        button.backgroundColor = UIColor.white
        button.setTitle("Batal", for: .normal)
        button.setTitleColor(UIColor.edit_text_gray, for: .normal)
        if (self.buttonFont != nil) {
            button.titleLabel?.font = self.buttonFont
        }
        
        button.layer.cornerRadius = 3
        button.borderWidth = 1
        button.borderColor = UIColor.edit_text_gray
        button.addTarget(self, action: #selector(onPopupCancelClicked), for: .touchUpInside)
        return button
    }()
    
    lazy var btnYes: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Ya", for: .normal)
        button.setTitleColor(.white, for: .normal)
        if (self.buttonFont != nil) {
            button.titleLabel?.font = self.buttonFont
        }
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(onPopupYesClicked), for: .touchUpInside)
        return button
    }()
    
    func onPopupOKClicked() {
        if (successModal != nil) {
            successModal?.hideModal()
            self.navigationController!.popToRootViewController(animated: true)
        }
    }
    
    func onPopupYesClicked() {
        if (confirmationModal != nil) {
            confirmationModal?.hideModal()
            if (selectedTicket!.promoCode != "") {
                self.alertError(msg: "Ticket ini telah menggunakan promo")
                return
            }
            ProgressView.shared.showProgressView(self.view)
            NetworkService.sharedInstance.claimPromo(redeem: (redeem?.code)!, ticketCode: (selectedTicket?.code)!, success: {data in
                ProgressView.shared.hideProgressView()
                if (data?.message == "OK") {
                    self.successModal?.showModal()
                } else {
                    self.alertError(msg: data!.result)
                }
            }, failure: {error in
                ProgressView.shared.hideProgressView()
                self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            })
        }
    }
    
    func onPopupCancelClicked() {
        if (confirmationModal != nil) {
            confirmationModal?.hideModal()
        }
    }
    
    func initConfirmationModal() {
        confirmationModal = CustomModal()
        confirmationModal?.modal(self.view, title: "Konfirmasi Penggunaan Promo", height: 240)
        confirmationModal?.addContent(view: confirmationLabel, space: -30, width: 200, height: 32)
        redeemCodeTextField.text = "   " + redeem!.code
        confirmationModal?.addContent(view: redeemCodeTextField, space: 20, width: 200, height: 30)
        confirmationModal?.addBtnBottom(btn1: btnCancel, btn2: btnYes, height: 35)
    }
    
    func initSuccessModal() {
        successModal = CustomModal()
        successModal?.modal(self.view, title: "Konfirmasi Penggunaan Promo", height: 250)
        successModal?.addContent(view: successImageView, space: -70, width: 100, height: 100)
        successModal?.addContent(view: successLabel, space: 20, width: 0, height: 17, referenceBottom: successImageView)
        successModal?.addBtnBottom(btn1: btnOK, btn2: nil, height: 35)
    }
    
    func showConfirmationAlert() {
        let confirmDialog = UIAlertController(title: "Konfirmasi Penggunaan Promo", message: "Apakah Anda yakin ingin menggunakan promo ini?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Tidak", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
        }
        confirmDialog.addAction(cancelAction)
        
        let sureAction = UIAlertAction(title: "Ya", style: UIAlertActionStyle.default) {
            (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
            self.showShowSuccessAlert()
        }
        confirmDialog.addAction(sureAction)
        self.present(confirmDialog, animated: true, completion: nil)
    }
    
    func showShowSuccessAlert() {
        let confirmDialog = UIAlertController(title: "Konfirmasi Penggunaan Promo", message: "Sukses!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ya", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
            self.navigationController!.popToRootViewController(animated: true)
        }
        confirmDialog.addAction(cancelAction)
        
        self.present(confirmDialog, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TicketController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if (tableView == ticketTableView) {
            let ticketCell = tableView.dequeueReusableCell(withIdentifier: "kActiveTicketCell") as! ActiveTicketViewCell
            let parkingHistory = parkingHistoryList[indexPath.row]
            ticketCell.parkingAddress.text = parkingHistory.address
            var vehicleType = ""
            for vehicle in Config.instance.loggedInUser!.vehicleList {
                if (vehicle.regCode == parkingHistory.regCode) {
                    vehicleType = "\(vehicle.vehicleType.rawValue)"
                    break
                }
            }
            ticketCell.vehicleTypeLabel.text = vehicleType
            ticketCell.parkingNameLabel.text = "\(parkingHistory.parkingName)"
            if parkingHistory.promoCode != "" {
                ticketCell.promoCodeLabel.text = "Promo Code \(parkingHistory.promoCode.uppercased())"
            } else {
                ticketCell.promoCodeLabel.text = "Tidak ada Promo"
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy - HH:mm"
            ticketCell.ticketDetailLabel.text = "\(parkingHistory.regCode) - \(formatter.string(from: Date(milliseconds: parkingHistory.checkIn)))"
            
            ticketCell.priceLabel.text = "Rp \(numFormatter.string(from: NSNumber(value: parkingHistory.priceHour))!)/ 12 jam, "
            
            return ticketCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == ticketTableView) {
            return parkingHistoryList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == ticketTableView) {
            if (redeem != nil) {
                selectedTicket = parkingHistoryList[indexPath.row]
                confirmationModal?.showModal()
                return
            }
            let parkingHistory = parkingHistoryList[indexPath.row]
            self.performSegue(withIdentifier: "showDetail", sender: parkingHistory)
        }
    }
}
