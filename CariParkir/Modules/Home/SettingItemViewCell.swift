//
//  SettingItemViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class SettingItemViewCell: UITableViewCell {
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var contentWrapper: UIView!
    
    override func layoutSubviews() {
        contentWrapper.layer.shadowColor = UIColor.soft_grey.cgColor
        contentWrapper.layer.shadowOpacity = 1
        contentWrapper.layer.shadowOffset = CGSize(width: 2, height: 2)
        contentWrapper.layer.shadowRadius = 2
    }
}
