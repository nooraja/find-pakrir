//
//  ActiveTicketViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/30/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class ActiveTicketViewCell: UITableViewCell {
    @IBOutlet weak var contentWrapper: UIView!
    @IBOutlet weak var ticketDetailLabel: UILabel!
    @IBOutlet weak var parkingNameLabel: UILabel!
    @IBOutlet weak var parkingAddress: UILabel!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func layoutSubviews() {
        contentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        contentWrapper.layer.shadowOpacity = 1
        contentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentWrapper.layer.shadowRadius = 2
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
