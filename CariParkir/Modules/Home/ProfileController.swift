//
//  ProfileController.swift
//  CariParkir
//
//  Created by Indovti on 8/21/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import DLRadioButton
import Floaty

class ProfileController: UIViewController, UITextFieldDelegate, ValidationDelegate {
    @IBOutlet weak var menuSegment: CustomSegmentedControl!
    @IBOutlet weak var vehicleTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var genderField: CustomTextField!
    @IBOutlet weak var birthdayField: CustomTextField!
    @IBOutlet weak var emailContentWrapper: UIView!
    @IBOutlet weak var firstNameContentWrapper: UIView!
    @IBOutlet weak var lastNameContentWrapper: UIView!
    @IBOutlet weak var phoneContentWrapper: UIView!
    @IBOutlet weak var genderContentWrapper: UIView!
    @IBOutlet weak var birthdayContentWrapper: UIView!
    @IBOutlet weak var floaty:Floaty!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    @IBOutlet weak var referralPageView: UIView!
    @IBOutlet weak var referralCodeLabel: UILabel!
    
    weak var activeField: UITextField?
    var vehicleList: [ConsumerVehicle] = []
    let kVehicleListItemCell: String = "kVehicleListItemCell"
    var birthdayPickerView: CustomPickerView = CustomPickerView()
    var genderPickerView: CustomPickerView = CustomPickerView()
    
    var selectedMonthIndex = 0
    var selectedYearIndex = 0
    var vehicleToEditIndex = 0
    var selectedGenderIndex = 0
    var genderList = ["Pria", "Wanita"]
    var months = [String]()
    var years = [Int]()
    var vehicleDelete = ConsumerVehicle()
    
    var isDataChanged: Bool {
        let consumer = Config.instance.loggedInUser!.copy() as! Consumer
        
        var isDifferent = false
        if consumer.phone != self.phoneField.text! || consumer.phone == "" {
            isDifferent = true
        }
        
        if consumer.firstName != firstNameField.text || consumer.firstName == "" {
            isDifferent = true
        }
        
        if consumer.lastName != lastNameField.text || consumer.lastName == "" {
            isDifferent = true
        }
        
        if consumer.birthDate == "" && birthdayField.text == "" {
            isDifferent = true
            profileValidator.unregisterField(birthdayField)
            profileValidator.registerField(birthdayField, errorLabel: nil, rules: [RequiredRule(message: "Bulan Lahir wajib diisi")])
        }
        
        if consumer.birthDate != "\(String(format: "%02d", selectedMonthIndex + 1))-\(years[selectedYearIndex])" || consumer.birthDate == "" {
            isDifferent = true
        }
        
        let jenisKL = genderField.text! == "Pria" ? "Male" : "Female"
        isDifferent = isDifferent || consumer.gender != Gender(rawValue: jenisKL)!
        return isDifferent
    }
    
    var editMode = false
    var defaultRow = 0
    
    let profileValidator = Validator()
    
    @IBOutlet weak var vehicleTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var referralTrailingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        menuSegment.addTarget(self, action: #selector(self.segmentChanged), for: .valueChanged)
//        print("Menu utama : \(menuSegment.frame)")
//        menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)
//        print("Menu berubah : \(menuSegment.frame)")
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        self.hideKeyboard()

        drawShadow()
        
        // populate years
        var years: [Int] = []
        let currentYear = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
        
        if years.count == 0 {
            var oldyear: Int = 1900
            for _ in oldyear...currentYear {
                years.append(oldyear)
                oldyear += 1
            }
        }
        self.years = years
        
        
        // populate months with localized names
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(DateFormatter().monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months
        
        birthdayPickerView.delegate = self
        birthdayPickerView.dataSource = self
        
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        assign(picker: birthdayPickerView, to: birthdayField)
        assign(picker: genderPickerView, to: genderField)
        self.navigationItem.hidesBackButton = true
        
        selectedYearIndex = self.years.count - 1
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month], from: date)
        selectedMonthIndex = components.month! - 1
        
        refetchConsumerProfile {
            self.getDataFromLocal()
        }
        
        registerValidation()
        
        layoutFAB()
    }
    
    var floatyPos: CGPoint?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        floatyPos = floaty.frame.origin
        print("\(floaty.frame.origin)")
        floaty.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (floatyPos != nil) {
            floaty.frame.origin = floatyPos!
        }
        floaty.isHidden = false
        setupBarButton()
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if self.menuSegment.selectedSegmentIndex == 1 {
                    self.menuSegment.selectedSegmentIndex = 0
                    self.profileLeadingConstraint.constant = -16
                    self.referralTrailingConstraint.constant = -999
                    self.floaty.isHidden = false
                    print("Menu utama : \(menuSegment.frame)")
                    menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)
                    print("Menu berubah : \(menuSegment.frame)")
                }
            case UISwipeGestureRecognizerDirection.left:
                if self.menuSegment.selectedSegmentIndex == 0 {
                    self.menuSegment.selectedSegmentIndex = 1
                    self.profileLeadingConstraint.constant = -999
                    self.referralTrailingConstraint.constant = -16
                    self.floaty.isHidden = true
                    print("Menu utama : \(menuSegment.frame)")
                    menuSegment.changeSelectedIndex(to: menuSegment.selectedSegmentIndex)
                    print("Menu berubah : \(menuSegment.frame)")
                }
            default:
                break
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func layoutFAB() {
        floaty.addItem("Add Vehicle", icon: #imageLiteral(resourceName: "menu_vehicle"), titlePosition: .left) { (item) in
            self.addButtonClicked()
        }
        
        let font = UIFont(name: "Titillium-Semibold", size: 14)
        for item in floaty.items {
            item.titleLabel.font = font!
            item.title = {item.title}()
        }
    }
    
    func registerValidation() {
        profileValidator.registerField(firstNameField, errorLabel: nil, rules: [RequiredRule(message: "Nama Depan wajib diisi")])
        profileValidator.registerField(lastNameField, errorLabel: nil, rules: [RequiredRule(message: "Nama Belakang wajib diisi")])
        profileValidator.registerField(phoneField, errorLabel: nil, rules: [RequiredRule(message: "Nomor Telepon wajib diisi"), PhoneNumberRule(message: "Nomor telepon tidak valid")])
        profileValidator.registerField(birthdayField, errorLabel: nil, rules: [RequiredRule(message: "Bulan Lahir wajib diisi"), AgeRule(limit: 15)])
        profileValidator.registerField(genderField, errorLabel: nil, rules: [RequiredRule(message: "Gender wajib diisi")])
    }
    
    var directSave = true
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.registerValidation()
        if (!isDataChanged) {
            _ = self.navigationController?.popViewController(animated: true)
            return
        }
        directSave = false
        
        let storyBoard = UIStoryboard(name: "AlertCustom", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertCustomViewController") as! MyAlertCustomViewController
        customAlert.content = MyAlertCustom(title: "Konfirmasi Profile", message: "Apakah Anda ingin menyimpan perubahan data profile?", yes: "Ya", no: "Cancel")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertCustomControllerDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.registerValidation()
        if (!isDataChanged) {
            return
        }
        directSave = true
        profileValidator.validateAll(self)
    }
    
    func validationSuccessful(_ validator: Validator) {
        let consumer = Config.instance.loggedInUser!.copy() as! Consumer
        consumer.firstName = self.firstNameField.text!
        consumer.lastName = self.lastNameField.text!
        consumer.phone = self.phoneField.text!
        consumer.birthDate = consumer.birthDate == "" && birthdayField.text == "" ? "" : "\(String(format: "%02d", selectedMonthIndex + 1))-\(years[selectedYearIndex])"
        
        let jenisKL = genderField.text! == "Pria" ? "Male" : "Female"
        consumer.gender = Gender(rawValue: jenisKL)!
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.updateConsumerDetail(consumer: consumer, success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
//                Config.instance.showNotification(title: "Sukses", message: "Ubah profile berhasil")
                
                let storyBoard = UIStoryboard(name: "AlertSuccess", bundle: nil)
                let customAlert = storyBoard.instantiateViewController(withIdentifier: "alertsuccess") as! MyAlertSuccessController
                customAlert.content = MyAlertSuccess(title: "Sukses", message: "Ubah profile berhasil")
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.delegate = self as? MyAlertSuccessDelegate
                self.present(customAlert, animated: true, completion: nil)
                
                UserDefaults.standard.setIsSosmed(value: false)
            } else {
                let message = data!.errorMessage
                if message.lowercased().contains("version") {
                    self.refetchConsumerProfile {
                        self.validationSuccessful(validator)
                    }
                } else {
                    self.alertError(msg: data!.errorMessage)
                }
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    func validationFailed(_ validator: Validator, _ errors:[(Validatable ,ValidationError)]) {
        if (errors.count > 0) {
            alertError(msg: errors[0].1.errorMessage)
        }
    }
    
    func getDataFromLocal() {
        let user: Consumer = Config.instance.loggedInUser!.copy() as! Consumer
        self.emailField.text = user.username
        self.firstNameField.text = user.firstName
        self.lastNameField.text = user.lastName
        self.phoneField.text = user.phone
        let jenisKL = user.gender.rawValue == "Male" ? "Pria" : "Wanita"
        self.genderField.text = jenisKL
        for (index,gender) in genderList.enumerated() {
            if gender == jenisKL {
                selectedGenderIndex = index
                break
            }
        }
        genderPickerView.selectRow(selectedGenderIndex, inComponent: 0, animated: false)
        
        if user.birthDate != "" {
            let birthday = user.birthDate.components(separatedBy: "-")
            let birthMonth = Int(birthday[birthday.count - 2])! - 1
            selectedMonthIndex = birthMonth
            
            let birthYear = Int(birthday[birthday.count - 1])
            for (index, year) in years.enumerated() {
                if year == birthYear {
                    selectedYearIndex = index
                    break
                }
            }
            
            self.birthdayField.text =  "\(DateFormatter().shortMonthSymbols[selectedMonthIndex].uppercased())/\(years[selectedYearIndex])"
        }
        birthdayPickerView.selectRow(selectedMonthIndex, inComponent: 0, animated: false)
        birthdayPickerView.selectRow(selectedYearIndex, inComponent: 1, animated: false)
        referralCodeLabel.text = user.qrCode.uppercased()
        
        if (!user.phone.isEmpty && !user.birthDate.isEmpty) {
            UserDefaults.standard.setIsSosmed(value: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let user: Consumer = Config.instance.loggedInUser!.copy() as! Consumer
        self.vehicleList = user.vehicleList
        self.vehicleTableView.reloadData()
    }
    
    func setupBarButton() {
        if isDataChanged {
            doneButton.tintColor = UIColor.white
            doneButton.isEnabled = true
        } else {
            doneButton.tintColor = UIColor.gray
            doneButton.isEnabled = false
        }
    }
    
    func drawShadow() {
        emailContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        emailContentWrapper.layer.shadowOpacity = 1
        emailContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        emailContentWrapper.layer.shadowRadius = 2
        
        firstNameContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        firstNameContentWrapper.layer.shadowOpacity = 1
        firstNameContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        firstNameContentWrapper.layer.shadowRadius = 2
        
        lastNameContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        lastNameContentWrapper.layer.shadowOpacity = 1
        lastNameContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        lastNameContentWrapper.layer.shadowRadius = 2
        
        phoneContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        phoneContentWrapper.layer.shadowOpacity = 1
        phoneContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        phoneContentWrapper.layer.shadowRadius = 2
        
        genderContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        genderContentWrapper.layer.shadowOpacity = 1
        genderContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        genderContentWrapper.layer.shadowRadius = 2
        
        birthdayContentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        birthdayContentWrapper.layer.shadowOpacity = 1
        birthdayContentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        birthdayContentWrapper.layer.shadowRadius = 2
    }
    
    func segmentChanged(sender: AnyObject) {
        if (menuSegment.selectedSegmentIndex == 0) {
            profileLeadingConstraint.constant = -16
            referralTrailingConstraint.constant = -999
            floaty.isHidden = false
        } else if (menuSegment.selectedSegmentIndex == 1) {
            profileLeadingConstraint.constant = -999
            referralTrailingConstraint.constant = -16
            floaty.isHidden = true
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField) {
        self.activeField = nil
    }
    
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        self.activeField = sender
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        setupBarButton()
    }
    
    @IBAction func segmentControlValueChanged(_ sender: CustomSegmentedControl) {
        sender.changeSelectedIndex(to: sender.selectedSegmentIndex)
    }
    
    func alertError(msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: "Oops!", message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func addButtonClicked() {
        editMode = false
        self.performSegue(withIdentifier: "showVehicleForm", sender: nil)
    }
    
    func changePasswordClicked() {
        self.performSegue(withIdentifier: "showPasswordForm", sender: nil)
    }
    let qrcode = "\(Config.instance.loggedInUser!.qrCode)"
   
    @IBAction func shareButtonClicked(_ sender: UIButton) {
        let message = "Selamat, kamu mendapatkan 1 (satu) kali parkir gratis pada mitra CariParkir! Gunakan kode referral \(qrcode.uppercased()) saat melakukan transaksi. Enjoy! \n CariParkir di Android : http://bit.ly/cariparkir-android \n CariParkir di iOS : http://bit.ly/cariparkir-ios"
        //Set the link to share.
        if let link = NSURL(string: "") {
            let objectsToShare = [message, link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func assign(picker: UIPickerView, to txtPickerTextField: UITextField){
        
        let pickerView = picker
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true
        
        let toolBar = UIToolbar()
        toolBar.barTintColor = UIColor.yellow_button
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Pilih", style: .done, target: self, action: #selector(self.pickerDoneButtonClicked))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Batal", style: .plain, target: self, action: #selector(self.pickerCancelButtonClicked))
        
        switch pickerView {
        case birthdayPickerView:
            doneButton.tag = 3
            cancelButton.tag = 3
        case genderPickerView:
            doneButton.tag = 4
            cancelButton.tag = 4
        default:
            return
        }
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtPickerTextField.inputView = pickerView
        txtPickerTextField.inputAccessoryView = toolBar
    }
    
    func pickerDoneButtonClicked(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 3:
            selectedMonthIndex = birthdayPickerView.selectedRow(inComponent: 0)
            selectedYearIndex = birthdayPickerView.selectedRow(inComponent: 1)
            let monthString = DateFormatter().shortMonthSymbols[selectedMonthIndex].uppercased()
            birthdayField.text = "\(monthString)/\(years[selectedYearIndex])"
        case 4:
            selectedGenderIndex = genderPickerView.selectedRow(inComponent: 0)
            genderField.text = genderList[selectedGenderIndex]
        default:
            break
        }
        
        self.view.endEditing(true)
        setupBarButton()
    }
    
    func pickerCancelButtonClicked(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 3:
            birthdayPickerView.selectRow(selectedMonthIndex, inComponent: 0, animated: false)
            birthdayPickerView.selectRow(selectedYearIndex, inComponent: 1, animated: false)
        case 4:
            genderPickerView.selectRow(selectedGenderIndex, inComponent: 0, animated: false)
        default:
            return
        }
        
        self.view.endEditing(true)
    }
    
    func deleteVehicle(_ vehicleToDelete: ConsumerVehicle) {
        
        vehicleDelete = vehicleToDelete
        
        let confirmDialog = UIAlertController(title: "Konfirmasi Hapus Kendaraan", message: "Apakah Anda yakin ingin menghapus kendaraan ini?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Batal", style: .cancel) { (action) in
            confirmDialog.dismiss(animated: true, completion: nil)
        }
        confirmDialog.addAction(cancelAction)
        
        let sureAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.destructive) {
            (action) in
            let consumer: Consumer = Config.instance.loggedInUser!.copy() as! Consumer
            guard let index = consumer.vehicleList.index(where: { vehicle -> Bool in
                return vehicleToDelete.regCode == vehicle.regCode
            }) else {
                self.vehicleList = consumer.vehicleList
                self.vehicleTableView.reloadData()
                return
            }
            consumer.vehicleList.remove(at: index)
            ProgressView.shared.showProgressView(self.view)
            NetworkService.sharedInstance.updateConsumerDetail(consumer: consumer, success: {data in
                ProgressView.shared.hideProgressView()
                if (data?.message == "OK") {
                    Config.instance.loggedInUser = data?.result
//                    Config.instance.showNotification(title: "Sukses", message: "Hapus kendaraan berhasil")
                    let alert = UIAlertController(title: "Sukses", message: "Hapus kendaraan berhasil", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.vehicleList.remove(at: index)
                    confirmDialog.dismiss(animated: true, completion: nil)
                    if let defaultVehicle = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id) {
                        if vehicleToDelete.regCode == defaultVehicle.regCode {
                            UserDefaults.standard.setDefaultVehicle(consumerId: Config.instance.loggedInUser!.id, vehicle: self.vehicleList[0])
                        }
                    }
                    self.vehicleTableView.reloadData()
                } else {
                    let message = data!.errorMessage
                    if message.lowercased().contains("version") {
                        self.refetchConsumerProfile {
                            self.deleteVehicle(vehicleToDelete)
                        }
                    } else {
                        self.alertError(msg: data!.errorMessage)
                    }
                }
            }, failure: {err in
                ProgressView.shared.hideProgressView()
                self.alertError(msg: Config.instance.getErrorMessage(error: err!))
            })
        }
        
        confirmDialog.addAction(sureAction)
        
        self.present(confirmDialog, animated: true, completion: nil)
        
        let storyBoard = UIStoryboard(name: "AlertConfirmDeleteVehicle", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertDeleteVehicleController") as! MyAlertDeleteVehicleController
        customAlert.content = MyAlertDelete(title: "Konfirmasi Hapus Kendaraan", message: "Apakah Anda yakin ingin menghapus kendaraan ini?", yes: "Confirm", no: "Batal")
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertDeleteDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonClicked(_ sender: UIButton) {
        let vehicleToDelete = self.vehicleList[sender.tag]
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.fetchActiveTicket(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                for parkingHistory in data!.result {
                    if (parkingHistory.checkOut == 0 && parkingHistory.regCode == vehicleToDelete.regCode) {
                        self.alertError(msg: "Kendaraan sedang digunakan")
                        return
                    }
                }
                self.deleteVehicle(vehicleToDelete)
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showVehicleForm") {
            let destVC = segue.destination as! VehicleFormController
            let vehicle: ConsumerVehicle? = sender as? ConsumerVehicle
            if vehicle != nil {
                destVC.vehicleToEdit = vehicle
            }
        }
    }
    
    func refetchConsumerProfile(completed: @escaping () -> Void) {
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.fetchConsumerDetail(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                completed()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {error in
            ProgressView.shared.hideProgressView()
            if Config.instance.loggedInUser != nil {
                self.getDataFromLocal()
            } else {
                self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            }
        })
    }
}

extension ProfileController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        vehicleTableHeightConstraint.constant = CGFloat(vehicleList.count) * vehicleTableView.rowHeight
        return vehicleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (vehicleList.count == 0) {
            return VehicleItemViewCell()
        }
        let cell = vehicleTableView.dequeueReusableCell(withIdentifier: kVehicleListItemCell) as! VehicleItemViewCell
        
        cell.bgView.backgroundColor = UIColor.color_primary
        let vehicle: ConsumerVehicle = vehicleList[indexPath.row]
        if let defaultVehicle = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id) {
            if vehicle.regCode == defaultVehicle.regCode {
                cell.bgView.backgroundColor = UIColor.fb_color_button
                defaultRow = indexPath.row
            }
        }
        switch vehicle.vehicleType {
        case .mobil: cell.vehicleIcon.image = #imageLiteral(resourceName: "car_white")
        case .motor: cell.vehicleIcon.image = #imageLiteral(resourceName: "motor_white")
        }
        cell.deleteButton.isHidden = vehicleList.count <= 1
        cell.deleteButton.tag = indexPath.row
        
        cell.vehicleInfo.text = "\(vehicle.brandName) \(vehicle.brandType) / \(vehicle.regCode.replacingOccurrences(of: " ", with: "")) / \(vehicle.vehicleYear)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vehicle = vehicleList[indexPath.row]
        
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.fetchActiveTicket(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                for parkingHistory in data!.result {
                    if (parkingHistory.checkOut == 0 && parkingHistory.regCode == vehicle.regCode) {
                        self.alertError(msg: "Kendaraan sedang digunakan")
                        return
                    }
                }
                self.performSegue(withIdentifier: "showVehicleForm", sender: vehicle)
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
}

extension ProfileController: UIPickerViewDelegate, UIPickerViewDataSource, MyAlertCustomControllerDelegate {
    
    func okButtonTapped() {
         self.profileValidator.validateAll(self)
        print("OK")
    }
    
    func cancelButtonTapped() {
         _ = self.navigationController?.popViewController(animated: true)
        print("cancel")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if (pickerView == birthdayPickerView) {
            return 2
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case birthdayPickerView:
            if (component == 0) {
                return months.count
            } else if (component == 1) {
                return years.count
            } else {
                return 0
            }
        case genderPickerView:
            return genderList.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView {
        case birthdayPickerView:
            switch component {
            case 0:
                return birthdayPickerView.getFontAttribute(months[row])
            case 1:
                return birthdayPickerView.getFontAttribute("\(years[row])")
            default:
                return nil
            }
        case genderPickerView:
            return genderPickerView.getFontAttribute(genderList[row])
        default:
            return nil
        }
    }
}

extension ProfileController : MyAlertSuccessDelegate{
    func finishButtonTapped() {
        if self.directSave {
            self.doneButton.tintColor = UIColor.soft_grey
            self.doneButton.isEnabled = false
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}

extension ProfileController : MyAlertDeleteDelegate{
    func confirmButtonTapped() {
        let consumer: Consumer = Config.instance.loggedInUser!.copy() as! Consumer
        guard let index = consumer.vehicleList.index(where: { vehicle -> Bool in
            return vehicleDelete.regCode == vehicle.regCode
        }) else {
            self.vehicleList = consumer.vehicleList
            self.vehicleTableView.reloadData()
            return
        }
        consumer.vehicleList.remove(at: index)
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.updateConsumerDetail(consumer: consumer, success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                //                    Config.instance.showNotification(title: "Sukses", message: "Hapus kendaraan berhasil")
                
                let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
                let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
                customAlert.content = MyAlertError(title: "Sukses", message: "Hapus kendaraan berhasil")
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.delegate = self as? MyAlertErrorDelegate
                self.present(customAlert, animated: true, completion: nil)
                
                self.vehicleList.remove(at: index)
             
                if let defaultVehicle = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id) {
                    if self.vehicleDelete.regCode == defaultVehicle.regCode {
                        UserDefaults.standard.setDefaultVehicle(consumerId: Config.instance.loggedInUser!.id, vehicle: self.vehicleList[0])
                    }
                }
                self.vehicleTableView.reloadData()
            } else {
                let message = data!.errorMessage
                if message.lowercased().contains("version") {
                    self.refetchConsumerProfile {
                        self.deleteVehicle(self.vehicleDelete)
                    }
                } else {
                    self.alertError(msg: data!.errorMessage)
                }
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    func noButtonTapped() {
        
    }
}
