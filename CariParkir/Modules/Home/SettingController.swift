//
//  SettingController.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class SettingController: UIViewController {
    let kCellIdentifier: String = "kSettingCell"
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var contentWrapper: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
    }
}

extension SettingController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0: print("not ready")
        case 1: performSegue(withIdentifier: "showHelp", sender: self)
        case 2: performSegue(withIdentifier: "showMyQRCode", sender: self)
        default: break
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) as! SettingItemViewCell
        switch indexPath.row {
        case 0:
            cell.menuLabel.text = "About"
            cell.menuIcon.image = #imageLiteral(resourceName: "ic_about")
        case 1:
            cell.menuLabel.text = "Help"
            cell.menuIcon.image = #imageLiteral(resourceName: "ic_help")
        case 2:
            cell.menuLabel.text = "My QR Code"
            cell.menuIcon.image = #imageLiteral(resourceName: "ic_qr_yellow")
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
}
