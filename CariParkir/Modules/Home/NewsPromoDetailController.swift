//
//  NewsPromoDetailControllerViewController.swift
//  CariParkir
//
//  Created by Muhammad Noor on 15/03/2018.
//  Copyright © 2018 PT NOSTRA. All rights reserved.
//

import UIKit

class NewsPromoDetailController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    var newsPromo = NewsPromo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        webView.delegate = self
        
        let url = URL(string: "\(Config.instance.addressPromoDetail)?id=\(newsPromo.id)")
        if let unwrappedURL = url {
            let request = URLRequest(url: unwrappedURL)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if error == nil {
                    self.webView.loadRequest(request)
                }else {
                    print("Error: \(error.debugDescription)")
                }
            }
            
            task.resume()
        }
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        ProgressView.shared.showProgressView(self.webView)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ProgressView.shared.hideProgressView()
    }
    
    @IBAction func backButton(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
}
