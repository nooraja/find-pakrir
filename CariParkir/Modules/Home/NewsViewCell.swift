//
//  NewsViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/23/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class NewsViewCell: UITableViewCell {
    @IBOutlet weak var newsImage: CustomImageView!
    @IBOutlet weak var descLabel: UILabel!
    
}
