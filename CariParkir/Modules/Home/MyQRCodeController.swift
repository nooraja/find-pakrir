//
//  MyQRCodeController.swift
//  CariParkir
//
//  Created by Indovti on 8/31/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import QRCode

class MyQRCodeController: UIViewController {

    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        qrCodeImage.image = {
            var qrCode = QRCode(Config.instance.loggedInUser!.qrCode)!
            qrCode.size = self.qrCodeImage.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
        nameLabel.text = Config.instance.loggedInUser!.firstName + " " + Config.instance.loggedInUser!.lastName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
