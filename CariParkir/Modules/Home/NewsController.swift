//
//  NewsController.swift
//  CariParkir
//
//  Created by Indovti on 9/11/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class NewsController: UIViewController {
    let kNewsCell: String = "kNewsCell"
    var newsList: [NewsPromo] = []
    
    @IBOutlet weak var newsTableView: UITableView!
    var isLastPage = false
    var page = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        newsList.removeAll()
        getDataFromAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getDataFromAPI() {
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.fetchNews(page: page, success: {data in
            if (data?.message == "OK") {
                self.newsList.append(contentsOf: data!.result)
                if data!.result.count == 0 {
                    self.isLastPage = true
                }
                self.newsTableView.reloadData()
            } else {
                self.alertError(msg: data?.errorMessage ?? "")
            }
            ProgressView.shared.hideProgressView()
        }, failure: {error in
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            ProgressView.shared.hideProgressView()
        })
    }
    
    func alertError(msg: String) {
        let alert = UIAlertController(title: "Oops!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NewsController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == self.newsTableView) {
            if (newsList.count > 0) {
                tableView.backgroundView = nil
            } else {
                let bg = generateTableBackground(tableView: tableView, title: "Oops!", text: "Tidak ada News untuk saat ini", image: #imageLiteral(resourceName: "no_news"), proporsion: 0.287, tint: UIColor.white)
                tableView.backgroundView = bg
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if (tableView == newsTableView) {
            let news: NewsPromo = newsList[indexPath.row]
            let cellNews = tableView.dequeueReusableCell(withIdentifier: kNewsCell) as! NewsViewCell
            cellNews.newsImage.tag = indexPath.row
            cellNews.newsImage.downloadedFrom(news.image, contentMode: .scaleAspectFill)
            cellNews.descLabel.text = news.title
            return cellNews
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == newsTableView) {
            return newsList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height: Float = 190
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (ProgressView.shared.isProgressViewAnimating()) {
            return
        }
        if (tableView == newsTableView) {
            let lastElement = newsList.count - 1
            if (indexPath.row == lastElement && !isLastPage) {
                ProgressView.shared.showProgressView(self.view)
                page = page + 1
                getDataFromAPI()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == newsTableView) {
            print("Test segue news mencari identifier")
            let news = newsList[indexPath.row]
            self.performSegue(withIdentifier: "showDetail", sender: news)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            print("Mendapatkan ")
            let vc = segue.destination as! NewsDetailController
            let news = sender as! NewsPromo
            
            vc.news = news
        }
    }
}
