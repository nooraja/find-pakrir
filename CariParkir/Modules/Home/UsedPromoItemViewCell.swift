//
//  UsedPromoItemViewCell.swift
//  CariParkir
//
//  Created by Indovti on 8/22/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit


class UsedPromoItemViewCell: UITableViewCell {
    
    
    @IBOutlet weak var usedButton: UIButton!
    @IBOutlet weak var contentWrapper: UIView!
    @IBOutlet weak var usedPromoImage: CustomImageView!
    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    var usedPromoList: [NewsPromo] = []
    var redeem: Redeem? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        contentWrapper.layer.shadowColor = UIColor.color_primary.cgColor
        contentWrapper.layer.shadowOpacity = 1
        contentWrapper.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentWrapper.layer.shadowRadius = 2
    }
    
    @IBAction func usePromoButtonClicked(_ sender: Any) {
        
        
    }
    
}
