//
//  ParkDetailHeaderCell.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 7/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import Kingfisher

class ParkDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var parkDetail: UILabel?
    @IBOutlet weak var parkName: UILabel?
    @IBOutlet weak var parkImage: UIImageView?
    @IBOutlet weak var parkOpenHour: UILabel?
    @IBOutlet weak var parkFacilities: UILabel?
    let numFormatter = NumberFormatter()
    
    func updateViews(park: Park) {
        numFormatter.groupingSeparator = "."
        numFormatter.numberStyle = .decimal
        if park.imageUrlList.isEmpty {
            parkImage?.image = UIImage(named: "no-preview")
        } else {
            if let url = URL(string: park.imageUrlList[0]) { // look at here
                parkImage?.kf.setImage(with: url, placeholder: UIImage(named: "no-preview"))
            } else {
                parkImage?.image = UIImage(named: "no-preview")
            }
        }
        
        
        
        parkName?.text = park.name
        parkFacilities?.text = park.facilities.joined(separator: ", ")
        
        let detailTextAtrr = NSMutableAttributedString()
        var detailIdx = 0

        for vehicle in park.parkingType {
            var boldFont: UIFont = UIFont.systemFont(ofSize: 13, weight: UIFontWeightBold)
            if let titiliumBoldFont = UIFont(name: "Titillium-Bold", size: 13) {
                boldFont = titiliumBoldFont
            }
        
//            var capacity = "--"
            var tarif    = "--"
            if (park.status == "true") {
//                capacity = "\(vehicle.capacity)"
                tarif    = "Rp \(numFormatter.string(from: NSNumber(value: vehicle.price))!) / 12jam"
            }
            
            if detailIdx < park.parkingType.count - 1 {
                tarif += "\n"
            }
            
//            let detailText: String = "\(vehicle.type), kapasitas \(capacity), tarif \(tarif)"
            let detailText: String = "\(vehicle.type), tarif \(tarif)"
            let tmpDetailTextAttr = NSMutableAttributedString.init(string: detailText)
            var index: Int = 0
            tmpDetailTextAttr.addAttribute(NSFontAttributeName, value: boldFont, range: NSRange(location: index,length: vehicle.type.count))
//            index += vehicle.type.count + 12
            index += vehicle.type.count + 2
//            tmpDetailTextAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: NSRange(location: index, length: "\(capacity)".count + 1))
//            tmpDetailTextAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: NSRange(location: index, length: "\(tarif)".count ))
//            index += "\(capacity)".count + 8
//            index += "\(tarif)".count + 8
            tmpDetailTextAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: NSRange(location: index,length: "\(tarif)".count))

            detailTextAtrr.append(tmpDetailTextAttr)
            
            detailIdx += 1
        }
        parkDetail?.attributedText = detailTextAtrr
        
        parkOpenHour?.text = ""
        if park.status == "true" {
            var openHourText: String!
            if(park.operationalDay.count > 2){
                openHourText = "Setiap Hari, \(park.open) - \(park.close)"
            }else {
                openHourText = "\(park.operationalDay.first!), \(park.open) - \(park.close)"
            }
            
            let openHourTextAttr = NSMutableAttributedString.init(string: openHourText)
            
//            openHourTextAttr.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: NSRange(location: "Setiap Hari".characters.count ,length: "\(park.open) - \(park.close)".characters.count))
            print("Masukkan error range")
            parkOpenHour?.attributedText = openHourTextAttr
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        parkImage?.layer.cornerRadius = 36
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func prepareOpeningHour(_ park: Park) -> String {
        let calendarsss: Calendar = Calendar.current
        let components: DateComponents  = (calendarsss as NSCalendar).components(.weekday, from:  Date())
        let currentday: Int  = components.weekday!
        let stringDay: String = CariParkirUtils.getDayString(currentday)
        
        if let day = park.operationalDay.first {
            return day
        }
        
        return stringDay
    }
    
    func prepareAvability(_ park: Park) -> String {
        guard let vehicle = park.parkingType.first else {
            return "Tidak tersedia"
        }
        
//        if park.counter < vehicle.capacity {
////            return "\(vehicle.type), kapasitas \(vehicle.capacity), tarif Rp \(vehicle.price) /jam"
//            return "\(vehicle.type), tarif Rp \(vehicle.price) /jam"
//        }
        
        return "Tidak tersedia"
    }
}
