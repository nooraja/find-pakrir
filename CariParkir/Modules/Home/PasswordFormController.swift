//
//  PasswordFormController.swift
//  CariParkir
//
//  Created by Indovti on 9/13/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class PasswordFormController: UIViewController, ValidationDelegate {
    @IBOutlet weak var oldPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var confirmPass: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var validator: Validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validator.registerField(oldPass, rules: [RequiredRule(message: "Password Lama wajib diisi")])
        validator.registerField(newPass, rules: [RequiredRule(message: "Password Baru wajib diisi"), PasswordRule(message: "Password minimal berisi 8 karakter dan ada huruf besarnya")])
        validator.registerField(confirmPass, rules: [RequiredRule(message: "Konfirmasi Password Wajib diisi"), ConfirmationRule(confirmField: newPass, message: "Konfirmasi password tidak sama")])
        
        self.hideKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func save(_ sender: UIButton) {
        validator.validateAll(self)
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    let closedEye = #imageLiteral(resourceName: "ic_eye_closed")
    let openEye = #imageLiteral(resourceName: "ic_eye_open")
    @IBAction func showHide(_ sender: UIButton) {
        switch sender.tag {
        case 0: oldPass.isSecureTextEntry = !oldPass.isSecureTextEntry
            sender.setImage(oldPass.isSecureTextEntry ? openEye : closedEye, for: .normal)
        case 1: newPass.isSecureTextEntry = !newPass.isSecureTextEntry
            sender.setImage(newPass.isSecureTextEntry ? openEye : closedEye, for: .normal)
        case 2: confirmPass.isSecureTextEntry = !confirmPass.isSecureTextEntry
            sender.setImage(confirmPass.isSecureTextEntry ? openEye : closedEye, for: .normal)
        default: break
        }
    }
    
    func validationSuccessful(_ validator: Validator) {
        let oldPass = self.oldPass.text!
        let newPass = self.newPass.text!
        
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.changePassword(newPass: newPass, oldPass: oldPass, success: { data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
        
                let storyBoard = UIStoryboard(name: "AlertSuccess", bundle: nil)
                let customAlert = storyBoard.instantiateViewController(withIdentifier: "alertsuccess") as! MyAlertSuccessController
                customAlert.content = MyAlertSuccess(title: "Sukses", message: "Change password berhasil. Harap melakukan login ulang.")
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.delegate = self as? MyAlertSuccessDelegate
                self.present(customAlert, animated: true, completion: nil)
            } else {
                self.alertError(msg: data!.result)
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
    
    func goToLogin() {
        let module = LoginModule()
        let controller = module.instantiateWithView()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func validationFailed(_ validator: Validator, _ errors:[(Validatable ,ValidationError)]) {
        if (errors.count > 0) {
            alertError(msg: errors[0].1.errorMessage)
        }
    }
    
    func alertError(msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: "Oops!", message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension PasswordFormController : MyAlertSuccessDelegate{
    func finishButtonTapped() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        ProgressView.shared.showProgressView(self.view)
        NetworkService.sharedInstance.logout(success: {data in
            ProgressView.shared.hideProgressView()
            if (data?.message == "OK") {
                UserDefaults.standard.setIsSosmed(value: false)
                UserDefaults.standard.setIsLoggedIn(value: false)
                UserDefaults.standard.setIsTermAndCondition(value: false)
                UserDefaults.standard.setUserName(value: "")
                UserDefaults.standard.setFullName(value: "")
                UserDefaults.standard.setToken(value: "")
                UserDefaults.standard.setRefreshToken(value: "")
                Config.instance.selectedVehicle = nil
                Config.instance.loggedInUser = nil
                self.goToLogin()
            } else {
                self.alertError(msg: data!.result)
            }
        }, failure: {err in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: err!))
        })
    }
}
