//
//  HomeModule.swift
//  CariParkir
//
//  Created by NOSTRA SOLUSI TEKNOLOGI on 6/9/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class HomeModule: NSObject {

    func instantiateWithNavigation() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let navigation = storyboard.instantiateInitialViewController() as! UINavigationController
        navigation.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigation.navigationBar.shadowImage = UIImage()
        let homeController = navigation.viewControllers.first as! HomeController
        let networkService = NetworkService.sharedInstance
        
        homeController.networkService = networkService
        
        return navigation
    }
}
