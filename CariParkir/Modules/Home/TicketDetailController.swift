//
//  TicketDetailController.swift
//  CariParkir
//
//  Created by Indovti on 8/31/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import QRCode

class TicketDetailController: UIViewController {
    @IBOutlet weak var qrCodeImageView: UIImageView!

    @IBOutlet weak var parkingDurationLabel: UILabel!
    @IBOutlet weak var checkInTimeLabel: UILabel!
    @IBOutlet weak var parkingAddress: UILabel!
    @IBOutlet weak var parkingName: UILabel!
    @IBOutlet weak var regNoLabel: UILabel!
    @IBOutlet weak var promoLabel: UILabel!
    
    var timer = Timer()
    var isTimerRunning = false
    var selectedTicket: ParkingHistory = ParkingHistory()
    var modalBooking: CustomModal?
    
    lazy var btnTutup: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("TUTUP", for: .normal)
        button.setTitleColor(.toolbar_grey, for: .normal)
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.toolbar_grey.cgColor
        button.titleLabel?.font = UIFont(name: "Titillium-Bold", size: 12)
        button.addTarget(self, action: #selector(handleBtnTutup), for: .touchUpInside)
        return button
    }()
    
    let titleLabel1: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.green
        label.font = UIFont(name: "Titillium-Bold", size: 18)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "SUKSES MELAKUKAN \nON THE SPOT BOOKING!"
        return label
    }()
    
    let titleLabel2: UILabel = {
        
        var myString: String = "Punya Kode (Promo / Referal / Redeem) ?"
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: myString, attributes: [NSFontAttributeName:UIFont(name: "Titillium-RegularUpright", size: 16.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: NSRange(location:6, length:31))
        
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = myMutableString
        return label
    }()
    
    let titleLabel3: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.font = UIFont(name: "Titillium-Regular", size: 16)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "Gunakan di sini! \nKosongkan jika tidak memiliki kode promo"
        return label
    }()
    
    let titleLabel4: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.orange
        label.font = UIFont(name: "Titillium-Regular", size: 16)
        label.text = "Kode"
        return label
    }()
    
    let textFieldKode: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.layer.backgroundColor = UIColor.white.cgColor
        
        textField.layer.masksToBounds = false
        textField.layer.shadowColor = UIColor.orange.cgColor
        textField.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        textField.layer.shadowRadius = 0.0
        return textField
    }()
    
    lazy var btnOK: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.yellow_button
        button.setTitle("Ya", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 3
        button.addTarget(self, action: #selector(handleBtnOK), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarFont()
        var vehicleType = ""
        for vehicle in Config.instance.loggedInUser!.vehicleList {
            if (vehicle.regCode == selectedTicket.regCode) {
                vehicleType = "Parkir \(vehicle.vehicleType.rawValue) "
            }
        }
        
        parkingName.text = "\(selectedTicket.parkingName)"
        parkingAddress.text = selectedTicket.address
        regNoLabel.text = "QR Code Transaksi - \(selectedTicket.regCode)"
        if selectedTicket.promoCode != "" {
            promoLabel.text = "Promo Code \(selectedTicket.promoCode.uppercased())"
        } else {
            promoLabel.text = "Tidak ada Promo"
            self.initRightButtonMenu()
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy - HH:mm"
        checkInTimeLabel.text = "\(formatter.string(from: Date(milliseconds: selectedTicket.checkIn)))"
        
        let checkOutTime: Int = selectedTicket.checkOut > 0 ? selectedTicket.checkOut : Date().millisecondsSince1970
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: (checkOutTime - selectedTicket.checkIn)/1000)
        parkingDurationLabel.text = String(format: "%02d : %02d : %02d", h, m, s)
        
        qrCodeImageView.image = {
            var qrCode = QRCode(selectedTicket.code)!
            qrCode.size = self.qrCodeImageView.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
        
        if !isTimerRunning {
            runTimer()
        }
        
        initializationModalBooking()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(TicketDetailController.keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TicketDetailController.keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        print("textFieldHeight: \(textFieldKode.frame.origin.y - textFieldKode.frame.size.height)")
        
        modalBooking?.parentView.setNeedsLayout()
        modalBooking?.parentView.layoutIfNeeded()
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = keyboardHeight - textFieldHeight
            modalBooking?.moveY(height: CGFloat(resultHeight + 20))
        }
        
        modalBooking?.parentView.setNeedsLayout()
        modalBooking?.parentView.layoutIfNeeded()
    }
    
    func keyboardWillHide(notification: NSNotification) {
        print("keyboardWillHide")
        print("textFieldHeight: \(textFieldKode.frame.origin.y - textFieldKode.frame.size.height)")
        
        modalBooking?.parentView.setNeedsLayout()
        modalBooking?.parentView.layoutIfNeeded()
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
            let resultHeight = textFieldHeight - keyboardHeight
            
            modalBooking?.moveY(height: CGFloat(resultHeight - 20))
        }
        
        modalBooking?.parentView.setNeedsLayout()
        modalBooking?.parentView.layoutIfNeeded()
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        let checkOutTime: Int = selectedTicket.checkOut > 0 ? selectedTicket.checkOut : Date().millisecondsSince1970
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: (checkOutTime - selectedTicket.checkIn)/1000)
        parkingDurationLabel.text = String(format: "%02d : %02d : %02d", h, m, s)
    }
    
    func initRightButtonMenu() {
        let optionBtn = UIButton(type: .custom)
        optionBtn.setImage(#imageLiteral(resourceName: "ico_promo_btn"), for: .normal)
        optionBtn.addTarget(self, action: #selector(optionBtnClicked), for: .touchUpInside)
        optionBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let rightBtn = UIBarButtonItem(customView: optionBtn)
        self.navigationItem.setRightBarButton(rightBtn, animated: false)
    }
    
    func optionBtnClicked() {
        if modalBooking != nil {
            modalBooking?.showModal()
        }
    }
    
    func initializationModalBooking() {
        modalBooking = CustomModal()
        modalBooking?.modal(self.view, title: "Konfirmasi Tiket", height: 370)
        modalBooking?.addContent(view: titleLabel1, space: -100, width: 180, height: 40)
        modalBooking?.addContent(view: titleLabel2, space: 10, width: 0, height: 16, referenceBottom: titleLabel1)
        modalBooking?.addContent(view: titleLabel3, space: 0, width: 0, height: 25, referenceBottom: titleLabel2)
        modalBooking?.addContent(view: titleLabel4, space: 20, width: 0, height: 16, referenceBottom: titleLabel3)
        modalBooking?.addContent(view: textFieldKode, space: 10, width: 0, height: 20, referenceBottom: titleLabel4)
        modalBooking?.addBtnBottom(btn1: btnTutup, btn2: btnOK, height: 33)
//        modalBooking = nil
    }
    
    func handleBtnTutup() {
        if (modalBooking != nil){
            modalBooking?.hideModal()
        }
       
        print("textFieldKode.frame.origin.y: \(textFieldKode.frame.origin.y)")
        print("textFieldKode.frame.size.height: \(textFieldKode.frame.size.height)")
        
        let textFieldHeight = textFieldKode.frame.origin.y - textFieldKode.frame.size.height
        print("textFieldHeight: \(textFieldHeight)")
    }
    
    func handleBtnOK() {
        
        if (textFieldKode.text?.isEmpty)! {
            self.alertTicket(title: "Oops!", msg: "Mohon di isi")
            return
        }
        
        if (modalBooking != nil){
            modalBooking?.hideModal()
            textFieldKode.text = ""
            
            ProgressView.shared.showProgressView(self.view)
            NetworkService.sharedInstance.claimPromo(redeem: textFieldKode.text!, ticketCode: selectedTicket.code, success: {data in
                ProgressView.shared.hideProgressView()
                if (data?.message == "OK") {
                    self.alertTicket(title: "Sukses", msg: data!.result)
                    Config.instance.showNotification(title: "Check In", message: "Selamat! Anda Mendapatkan Promo")
                    self.promoLabel.text = data!.result
                    self.navigationItem.setRightBarButton(nil, animated: false)
                } else {
                    self.alertTicket(title: "Oops!", msg: data!.result)
                }
            }, failure: {error in
                ProgressView.shared.hideProgressView()
                self.alertTicket(title: "Oops!", msg: Config.instance.getErrorMessage(error: error!))
            })
        } else if (modalBooking == nil ){
            
        }
        
    }
    
    func alertTicket(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ya", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
