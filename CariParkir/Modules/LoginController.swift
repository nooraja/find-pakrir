//
//  LoginController.swift
//  CariParkir
//
//  Created by vti on 8/18/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FacebookCore
import FacebookLogin
import GoogleSignIn

class LoginController: UIViewController , UITextFieldDelegate, ValidationDelegate {
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var lblSignup: UILabel!
    @IBOutlet weak var lblNoHPHilang: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var lblInfo: UILabel!
    
    let validator = Validator()
    var button: UIButton!
    var iconClick : Bool!
    var networkService: INetworkService!
    let transitionManager = TransitionManager()
    var dataToken: GetToken?
    var dataOTP: GetOTP?
    var fixTemp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(
            self,
            selector:  #selector(deviceDidRotate),
            name: .UIDeviceOrientationDidChange,
            object: nil
        )
       
        labelTitle.text = "Silahkan masukan" + "\n nomor HP Anda di bawah ini*"
        btnCustom()
        txtCustom()
        txtLoadUnderline()
        labelUnderline(label: lblSignup, filter: "SIGN UP")
        labelUnderline(label: lblNoHPHilang, filter: nil)
        
        self.hideKeyboard()
    }
    
    @IBAction func showHide(_ sender: Any) {
        if(iconClick == true) {
            iconClick = false
            button.setImage(UIImage(named: "ic_eye_open"), for: .normal)
        } else {
            iconClick = true
            button.setImage(UIImage(named: "ic_eye_closed"), for: .normal)
        }
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func txtLoadUnderline() {
        txtPhoneNumber.delegate = self
        txtPhoneNumber.underlined()
        txtPhoneNumber.backgroundColor = .clear
        txtPhoneNumber.layer.cornerRadius = 3
        txtPhoneNumber.layer.borderWidth = 2
        txtPhoneNumber.layer.borderColor = UIColor.yellow_button.cgColor
        txtPhoneNumber.setLeftPaddingPoints(10)
        txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "No. HP(08xxxxx)",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.gray_placeholder])
    }
    
    func txtCustom() {
        iconClick = true
        button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_eye_closed"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        button.frame = CGRect(x: 0, y: 5, width: CGFloat(20), height: CGFloat(20))
        button.addTarget(self, action: #selector(self.showHide), for: .touchUpInside)
        
        let gestureRecognizerSignUp = UITapGestureRecognizer(target: self, action: #selector(LoginController.goToSignup))
        lblSignup.isUserInteractionEnabled = true
        lblSignup.addGestureRecognizer(gestureRecognizerSignUp)
        
        let gestureRecognizerForgot = UITapGestureRecognizer(target: self, action: #selector(LoginController.goToForgotPasword))
        lblNoHPHilang.isUserInteractionEnabled = true
        lblNoHPHilang.addGestureRecognizer(gestureRecognizerForgot)
        
        
        
        
        txtPhoneNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    func btnCustom() {
        btnLogin.backgroundColor = .clear
        btnLogin.layer.cornerRadius = 3
        btnLogin.layer.borderWidth = 1
        btnLogin.layer.borderColor = UIColor.white.cgColor
        
        btnFb.layer.cornerRadius = 3
        btnGoogle.layer.cornerRadius = 3
    }
    
    func deviceDidRotate() {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            txtLoadUnderline()
        default:
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        }
    }
    
    @IBAction func closeLogin(_ sender: Any) {
        let module = HomeModule()
        let controller = module.instantiateWithNavigation()
        controller.modalPresentationStyle = .custom
        controller.modalTransitionStyle = .crossDissolve
        UIView.transition(with: UIApplication.shared.delegate!.window!!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            UIApplication.shared.delegate!.window!!.rootViewController = controller
        }, completion: nil)
    }
    
    func loadUserProfile() {
        NetworkService.sharedInstance.fetchConsumerDetail(success: {data in
            if (data?.message == "OK") {
                Config.instance.loggedInUser = data?.result
                let defaultVehicle: ConsumerVehicle? = UserDefaults.standard.getDefaultVehicle(consumerId: Config.instance.loggedInUser!.id)
                if (defaultVehicle == nil) {
                    let vehicle:ConsumerVehicle = Config.instance.loggedInUser!.vehicleList[0]
                    UserDefaults.standard.setDefaultVehicle(consumerId: Config.instance.loggedInUser!.id, vehicle: vehicle)
                }
                if(Config.instance.loggedInUser!.birthDate.isEmpty || Config.instance.loggedInUser!.phone.isEmpty){
                    UserDefaults.standard.setIsSosmed(value: true)
                }
                self.closeLogin(self)
            } else {
                ProgressView.shared.hideProgressView()
                self.alertError(title: "Error", msg: "Test "+(data?.errorMessage)! )
            }
        }, failure: {error in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: "TEst "+Config.instance.getErrorMessage(error: error!))
        })
    }
    
    func saveDeviceToken() {
        if let token = InstanceID.instanceID().token() {
            print("token : \(String(describing: token))")
            NetworkService.sharedInstance.registerDeviceToken(token: token, success: {data in
                print("data \(String(describing: data?.message))")
                if (data?.message == "OK") {
                    UserDefaults.standard.setIsLoggedIn(value: true)
                    self.loadUserProfile()
                } else {
                    self.alertError(title: "Error", msg: data?.errorMessage ?? "")
                }
            }, failure: {error in
                self.alertError(msg: "Test "+Config.instance.getErrorMessage(error: error!))
            })
            ProgressView.shared.hideProgressView()
        } else {
            ProgressView.shared.hideProgressView()
            self.alertError(msg: "Retrieving Token failed. Please restart your application")
        }
    }
    
    @IBAction func goToHome(_ sender: Any) {
        guard let temp = txtPhoneNumber.text else { return }
        if txtPhoneNumber.text != "" {
            let tempEndIndex = temp.endIndex
            if (txtPhoneNumber.text?.count == 13) && (txtPhoneNumber.text![txtPhoneNumber.text?.startIndex..<txtPhoneNumber.text?.index((txtPhoneNumber.text?.startIndex)!, offsetBy: 2)] == "62")  {
                let tmpStartIndex = temp.index(tempEndIndex, offsetBy: -11)
                let range = Range(uncheckedBounds: (lower: tmpStartIndex, upper: tempEndIndex))
                fixTemp = "0\(temp[range])"
            } else if txtPhoneNumber.text?.count == 12 {
                
//                validator.registerField(txtPhoneNumber, errorLabel: lblInfo, rules: [RequiredRule(), PhoneNumberRule(message: "Invalid Phone Number")])
                let tmpStartIndex = temp.index(tempEndIndex, offsetBy: -12)
                let range = Range(uncheckedBounds: (lower: tmpStartIndex, upper: tempEndIndex))
                fixTemp = "\(temp[range])"
            } else if (txtPhoneNumber.text?.count == 11) && (txtPhoneNumber.text![txtPhoneNumber.text?.startIndex..<txtPhoneNumber.text?.index((txtPhoneNumber.text?.startIndex)!, offsetBy: 1)] == "8") {
                let tmpStartIndex = temp.index(tempEndIndex, offsetBy: -11)
                let range = Range(uncheckedBounds: (lower: tmpStartIndex, upper: tempEndIndex))
                fixTemp = "0\(temp[range])"
            } else {
                print("out of index")
            }
        }
        
        print("index \(fixTemp)")
        
        validator.validateAll(self)
    }
    
    func validationSuccessful(_ validator: Validator) {
        ProgressView.shared.showProgressView(self.view)
        print("number fix : \(fixTemp)")
        networkService.fetchRequestLoginOTP(phone: fixTemp, success: { (data) in
            print("data \(String(describing: data?.message))")
            if (data?.message == "OK") {
                self.dataOTP = data?.result

                UserDefaults.standard.setPhoneNumber(value: self.fixTemp)
                if self.txtPhoneNumber.text != "" {
                    self.performSegue(withIdentifier: "toVerificationLogin", sender: self)
                }
            } else if (data?.message == "ERROR") {
                ProgressView.shared.hideProgressView()
                self.alertError(title: "Username not found", msg: "Silahkan daftarkan nomer anda")
                    self.performSegue(withIdentifier: "toSignup", sender: self)
            }
            ProgressView.shared.hideProgressView()
        }) { (error) in
            ProgressView.shared.hideProgressView()
            self.alertError(msg: Config.instance.getErrorMessage(error: error!))
        }
        
        let slece = txtPhoneNumber.text![txtPhoneNumber.text?.startIndex..<txtPhoneNumber.text?.index((txtPhoneNumber.text?.startIndex)!, offsetBy: 2)]
        print("number : \(slece)")
    }
    
    func validationFailed(_ validator: Validator, _ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.underlinedRed()
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
    
    func labelUnderline(label: UILabel, filter: String?) {
        let textRange: NSRange?
        let text = label.text
        let attributedText = NSMutableAttributedString(string: text!)
        
        if (filter != nil) {
            textRange = (text! as NSString).range(of: filter!)
            attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.orange, range: textRange!)
        }else {
            textRange = NSMakeRange(0, (text?.count)!)
            attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor.yellow_button, range: textRange!)
            attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange!)
        }
        
        label.attributedText = attributedText
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        validator.validateField(textField){ error in
            if error != nil {
                error?.errorLabel?.isHidden = false
                error?.errorLabel?.text = error?.errorMessage
            }
        }
    }
    
    func goToSignup(sender: UITapGestureRecognizer) {
        if UserDefaults.standard.isTermAndCondition() {
            self.performSegue(withIdentifier: "toSignup", sender: self)
        } else {
            self.performSegue(withIdentifier: "toTermAndCondition", sender: self)
        }
    }
    
    func goToForgotPasword(sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "toForgot", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toSignup"?:
            let dest = segue.destination as! SignupController
            dest.transitioningDelegate = self.transitionManager
            networkService = NetworkService.sharedInstance
            dest.networkService = networkService
            dest.flagWizard = false
        case "toForgot"?:
            let dest = segue.destination as! ForgotPasswordController
            dest.transitioningDelegate = self.transitionManager
            networkService = NetworkService.sharedInstance
            dest.networkService = networkService
        case "toTermAndCondition"?:
            let dest = segue.destination as! TermAndConditionController
            dest.transitioningDelegate = self.transitionManager
            dest.flagWizard = false
            dest.networkService = NetworkService.sharedInstance
        case "toVerificationLogin"?:
            if txtPhoneNumber.text != "" {
                let dest = segue.destination as! VerificationLoginViewController
                dest.transitioningDelegate = self.transitionManager
                dest.consumer.phone = txtPhoneNumber.text!
//                dest.consumer.phone = fixTemp
                dest.networkService = NetworkService.sharedInstance
            }else {
                print("Never Give up")
            }
        default:
            break
        }
    }
    
    func alertError(title: String = "Oops!", msg: String) {
        let storyBoard = UIStoryboard(name: "AlertError", bundle: nil)
        let customAlert = storyBoard.instantiateViewController(withIdentifier: "MyAlertErrorController") as! MyAlertErrorController
        customAlert.content = MyAlertError(title: title, message: msg)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self as? MyAlertErrorDelegate
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func fbBtnClicked(_ sender: UIButton) {
        facebookAuth()
    }
    
    @IBAction func gPlusBtnClicked(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }

    let loginFbManager: LoginManager = LoginManager()
    func facebookAuth() {
        loginFbManager.logIn([.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                self.firebaseLoginHandler(nil, error.localizedDescription, source: "FACEBOOK")
                print(error.localizedDescription)
            case .cancelled:
                self.firebaseLoginHandler(nil, "Login cancelled", source: "FACEBOOK")
                print("Login cancelled")
            case .success( _, _, let accessToken):
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                ProgressView.shared.showProgressView(self.view)
                Auth.auth().signIn(with: credential, completion: {
                    (user, error) in
                    self.firebaseLoginHandler(user, error?.localizedDescription, source: "FACEBOOK")
                })
            }
        }
    }
    
    func firebaseLoginHandler(_ user: User?, _ message: String?, source: String) {
        if message != nil {
            ProgressView.shared.hideProgressView()
            self.alertError(msg: message!)
        } else {
            networkService.signinSocmed(username: user!.email!, password: user!.uid, source: source, success: { (data) in
                if(data?.message == "OK"){
                    self.dataToken = data?.result
                    
                    UserDefaults.standard.setUserName(value: self.txtPhoneNumber.text!)
                    UserDefaults.standard.setFullName(value: self.dataToken!.fullName)
                    UserDefaults.standard.setToken(value: self.dataToken!.token)
                    UserDefaults.standard.setRefreshToken(value: self.dataToken!.refreshToken)
                    
                    
                    self.saveDeviceToken()
                }else{
                    ProgressView.shared.hideProgressView()
                    self.alertError(title: "Error login", msg: data!.errorMessage)
                }
            }) { (error) in
                ProgressView.shared.hideProgressView()
                self.alertError(msg: Config.instance.getErrorMessage(error: error!))
            }
        }
    }
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! >= 15) && (string.count > 0){
            lblInfo.text = "Hanya 15 angka yang diperbolehkan"
            return false
        }
        return true
    }
}

extension LoginController: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let idToken = user.authentication.idToken!
            let accessToken = user.authentication.accessToken!
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
            ProgressView.shared.showProgressView(self.view)
            Auth.auth().signIn(with: credential, completion: {
                (user, error) in
                self.firebaseLoginHandler(user, error?.localizedDescription, source: "GOOGLE")
            })
        } else {
            self.firebaseLoginHandler(nil, error?.localizedDescription, source: "GOOGLE")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        
    }
}
