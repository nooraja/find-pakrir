//
//  LoginModule.swift
//  CariParkir
//
//  Created by vti on 8/18/17.
//  Copyright © 2017 PT NOSTRA. All rights reserved.
//

import UIKit

class LoginModule: NSObject {
    
    func instantiateWithView() -> UIViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let viewLogin = storyboard.instantiateInitialViewController()!
        let loginController = viewLogin as! LoginController
        let networkService = NetworkService.sharedInstance
        loginController.networkService = networkService
        
        return viewLogin
    }
}
